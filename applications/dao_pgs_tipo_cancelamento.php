<?php

class dao_pgs_tipo_cancelamento {

    public static function pegar($codigo, $id = false) {
        $db = new mysqlsearch();
        $db->table("pgs_tipo_cancelamento");
        $db->column("*");
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->match("codigo", $codigo);
        }
        $dao = $db->go();
        if ($dao[0]) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("pgs_tipo_cancelamento");
        $db->column("*");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = $row;
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

}