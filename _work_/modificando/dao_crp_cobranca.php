<?php

class _dao_crp_cobranca {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("crp_cobranca");
        $db->column("*");
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            $crp_transacao = crp_transacao::listar($dao[0]["usuario_id"], $dao[0]["id"]);
            $crp_tipo_pagamento = dao_crp_tipo_pagamento::pegar($dao[0]["crp_tipo_pagamento_id"]);
            $crp_tipo_pagamento_status = dao_crp_tipo_pagamento_status::pegar($dao[0]["crp_tipo_pagamento_status_id"]);
            return self::hook($dao[0], $crp_transacao, $crp_tipo_pagamento, $crp_tipo_pagamento_status);
        }
        return false;
    }

    public static function listar($usuario_id, $lancamento = false, $faturamento = false, $vencimento = false, $creditado = false, $crp_tipo_pagamento_status_id = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("crp_cobranca");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        if ($lancamento) {
            if (is_array($lancamento)) {
                $db->between("DATE(lancamento)", $lancamento);
            } else {
                $db->match("DATE(lancamento)", $lancamento);
            }
        }
        if ($faturamento) {
            if (is_array($faturamento)) {
                $db->between("DATE(faturamento)", $faturamento);
            } else {
                $db->match("DATE(faturamento)", $faturamento);
            }
        }
        if ($vencimento) {
            if (is_array($vencimento)) {
                $db->between("DATE(vencimento)", $vencimento);
            } else {
                $db->match("DATE(vencimento)", $vencimento);
            }
        }
        if ($creditado) {
            if (is_array($creditado)) {
                $db->between("DATE(creditado)", $creditado);
            } else {
                $db->match("DATE(creditado)", $creditado);
            }
        }
        if ($crp_tipo_pagamento_status_id) {
            if (is_array($lancamento)) {
                $db->in("crp_tipo_pagamento_status_id", $crp_tipo_pagamento_status_id);
            } else {
                $db->match("crp_tipo_pagamento_status_id", $crp_tipo_pagamento_status_id);
            }
        }
        $dao = $db->go();
        if ($dao) {
            $dao_crp_tipo_pagamento = dao_crp_tipo_pagamento::listar();
            $dao_crp_tipo_pagamento_status = dao_crp_tipo_pagamento_status::listar();
            foreach ($dao as $row) {
                $crp_transacao = crp_transacao::listar($row["usuario_id"], $row["id"]);
                $crp_tipo_pagamento = (!empty($dao_crp_tipo_pagamento[$row["crp_tipo_pagamento_id"]]) ? $dao_crp_tipo_pagamento[$row["crp_tipo_pagamento_id"]] : false);
                $crp_tipo_pagamento_status = (!empty($dao_crp_tipo_pagamento_status[$row["crp_tipo_pagamento_status_id"]]) ? $dao_crp_tipo_pagamento_status[$row["crp_tipo_pagamento_status_id"]] : false);
                $array[$row["id"]] = self::hook($row, $crp_transacao, $crp_tipo_pagamento, $crp_tipo_pagamento_status);
            }
        }
        return $array;
    }

    public static function fatura_mes($usuario_id, $faturamento) {
        $db = new mysqlsearch();
        $db->table("crp_cobranca");
        $db->column("COUNT(id)", false, "total");
        $db->match("usuario_id", $usuario_id);
        $db->between("DATE(faturamento)", $faturamento);
        $dao = $db->go();
        if (!empty($dao[0]["total"])) {
            return $dao[0]["total"];
        }
        return 0;
    }

    private static function hook($row, $crp_transacao, $crp_tipo_pagamento, $crp_tipo_pagamento_status) {
        $row["lancamento_br"] = date("d/m/Y", strtotime($row["lancamento"]));
        $row["vencimento_br"] = date("d/m/Y", strtotime($row["vencimento"]));
        $row["valor"] = 0.00000;
        if ($crp_transacao) {
            foreach ($crp_transacao as $key => $value) {
                $row["valor"] += $value["valor"];
            }
        }
        $row["valor_br"] = number_format($row["valor"], 2, ',', '.');
        if ($crp_tipo_pagamento) {
            foreach ($crp_tipo_pagamento as $key => $value) {
                $row["crp_tipo_pagamento_{$key}"] = $value;
            }
        }
        if ($crp_tipo_pagamento_status) {
            foreach ($crp_tipo_pagamento_status as $key => $value) {
                $row["crp_tipo_pagamento_status_{$key}"] = $value;
            }
        }
        return $row;
    }

    public static function cadastrar($codigo, $referencia, $lancamento, $faturamento, $vencimento, $crp_tipo_pagamento_id, $crp_tipo_pagamento_status_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("crp_cobranca");
        $db->column("codigo", $codigo);
        $db->column("referencia", $referencia);
        $db->column("lancamento", $lancamento);
        $db->column("faturamento", $faturamento);
        $db->column("vencimento", $vencimento);
        $db->column("crp_tipo_pagamento_id", $crp_tipo_pagamento_id);
        $db->column("crp_tipo_pagamento_status_id", $crp_tipo_pagamento_status_id);
        $db->column("usuario_id", $usuario_id);
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function atualizar($id, $referencia, $creditado, $crp_tipo_pagamento_id, $crp_tipo_pagamento_status_id) {
        $db = new mysqlsave();
        $db->table("crp_cobranca");
        $db->column("referencia", $referencia);
        $db->column("creditado", $creditado);
        $db->column("crp_tipo_pagamento_id", $crp_tipo_pagamento_id);
        $db->column("crp_tipo_pagamento_status_id", $crp_tipo_pagamento_status_id);
        $db->match("id", $id);
        return $db->go();
    }

}