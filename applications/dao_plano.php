<?php

if (!class_exists("zpanel_plano", false)) {
    include path::applications("zpanel/plano.php");
}

class dao_plano {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("plano");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id, "AND");
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $plano_valor = dao_plano_valor::listar();
            return self::hook($dao[0], false, $plano_valor);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("plano");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("valor");
        $dao = $db->go();
        if ($dao) {
            $zpanel_dao = zpanel_plano::listar(dao_usuario::zpanel_id(dao_servidor::usuario_id(dao_usuario::servidor_id($usuario_id))));
            $plano_valor = dao_plano_valor::listar();
            foreach ($dao as $row) {
                $zpanel = (!empty($zpanel_dao[$row["zpanel_id"]]) ? $zpanel_dao[$row["zpanel_id"]] : false);
                $array[$row["id"]] = self::hook($row, $zpanel, $plano_valor);
            }
        }
        return $array;
    }

    private static function hook($row, $zpanel, $plano_valor) {
        $valor = 0.00;
        $reajuste = false;
        foreach ($plano_valor as $plv) {
            $tipo = $plv["tipo"];
            $row["plv_{$tipo}_valor"] = $plv["valor"];
            $row["plv_{$tipo}_valor_br"] = $plv["valor_br"];
            $row["plv_{$tipo}_total"] = ($plv["valor"] * (!empty($row["total_{$tipo}"]) ? $row["total_{$tipo}"] : 0.00));
            $row["plv_{$tipo}_total_br"] = number_format($row["plv_{$tipo}_total"], 2, ',', '.');
            $row["plv_{$tipo}_reajustado"] = (strtotime($plv["atualizado"]) > strtotime($row["atualizado"]));
            if ($row["plv_{$tipo}_reajustado"]) {
                $reajuste = true;
            }
            $valor += $row["plv_{$tipo}_total"];
        }
        $row["reajustado"] = $reajuste;
        $row["valor_br"] = number_format($row["valor"], 2, ',', '.');
        $row["valor"] = number_format($row["valor"], 2, '.', '');
        $row["valor_reajustado"] = number_format($valor, 2, '.', '');
        $row["valor_reajustado_br"] = number_format($valor, 2, ',', '.');
        $row["total_usuarios"] = (!empty($zpanel["clients"]) ? $zpanel["clients"] : 0);
        return $row;
    }

    public static function cadastrar($nome, $habilitar_php, $habilitar_cgi, $total_dominio, $total_subdominio, $total_email, $total_redirecionamento_email, $total_lista_email, $total_ftp, $total_mysql, $total_quota_disco, $total_quota_transferencia, $valor, $usuario_id) {
        $usuario_zpanel_id = dao_usuario::zpanel_id(dao_servidor::usuario_id(dao_usuario::servidor_id($usuario_id)));
        if ($usuario_zpanel_id) {
            if (!self::verificar($nome)) {
                $zpanel_id = zpanel_plano::cadastrar($nome, $habilitar_php, $habilitar_cgi, $total_dominio, $total_subdominio, $total_email, $total_redirecionamento_email, $total_lista_email, $total_ftp, $total_mysql, $total_quota_disco, $total_quota_transferencia, $usuario_zpanel_id);
                if ($zpanel_id) {
                    $db = new mysqlsave();
                    $db->table("plano");
                    $db->column("nome", $nome);
                    $db->column("habilitar_php", $habilitar_php);
                    $db->column("habilitar_cgi", $habilitar_cgi);
                    $db->column("total_dominio", $total_dominio);
                    $db->column("total_subdominio", $total_subdominio);
                    $db->column("total_email", $total_email);
                    $db->column("total_redirecionamento_email", $total_redirecionamento_email);
                    $db->column("total_lista_email", $total_lista_email);
                    $db->column("total_ftp", $total_ftp);
                    $db->column("total_mysql", $total_mysql);
                    $db->column("total_quota_disco", $total_quota_disco);
                    $db->column("total_quota_transferencia", $total_quota_transferencia);
                    $db->column("valor", $valor);
                    $db->column("usuario_id", $usuario_id);
                    $db->column("zpanel_id", $zpanel_id);
                    $db->column("cadastrado", date("Y-m-d H:i:s"));
                    $db->column("atualizado", date("Y-m-d H:i:s"));
                    if ($db->go()) {
                        return $db->id();
                    }
                }
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $habilitar_php, $habilitar_cgi, $total_dominio, $total_subdominio, $total_email, $total_redirecionamento_email, $total_lista_email, $total_ftp, $total_mysql, $total_quota_disco, $total_quota_transferencia, $valor, $usuario_id) {
        $zpanel_id = self::zpanel_id($id);
        $usuario_zpanel_id = dao_usuario::zpanel_id(dao_servidor::usuario_id(dao_usuario::servidor_id($usuario_id)));
        if ($zpanel_id && $usuario_zpanel_id) {
            if (zpanel_plano::atualizar($zpanel_id, $nome, $habilitar_php, $habilitar_cgi, $total_dominio, $total_subdominio, $total_email, $total_redirecionamento_email, $total_lista_email, $total_ftp, $total_mysql, $total_quota_disco, $total_quota_transferencia, $usuario_zpanel_id)) {
                $db = new mysqlsave();
                $db->table("plano");
                $db->column("nome", $nome);
                $db->column("habilitar_php", $habilitar_php);
                $db->column("habilitar_cgi", $habilitar_cgi);
                $db->column("total_dominio", $total_dominio);
                $db->column("total_subdominio", $total_subdominio);
                $db->column("total_email", $total_email);
                $db->column("total_redirecionamento_email", $total_redirecionamento_email);
                $db->column("total_lista_email", $total_lista_email);
                $db->column("total_ftp", $total_ftp);
                $db->column("total_mysql", $total_mysql);
                $db->column("total_quota_disco", $total_quota_disco);
                $db->column("total_quota_transferencia", $total_quota_transferencia);
                $db->column("valor", $valor);
                $db->column("atualizado", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function remover($id, $plano_id) {
        $zpanel_id = self::zpanel_id($id);
        $plano_zpanel_id = self::zpanel_id($plano_id);
        if ($zpanel_id && $plano_zpanel_id) {
            if (zpanel_plano::remover($zpanel_id, $plano_zpanel_id)) {
                $db = new mysqlsave();
                $db->table("plano");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    return self::mover_relacionados($id, $plano_id);
                }
            }
        }
        return false;
    }

    private static function mover_relacionados($id, $plano_id) {
        // move todos os usuarios
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("plano_id", $plano_id);
        $db->column("atualizado", date("Y-m-d H:i:s"));
        $db->match("plano_id", $id);
        return $db->go();
    }

    public static function verificar($nome) {
        $db = new mysqlsearch();
        $db->table("plano");
        $db->column("id");
        $db->match("nome", $nome);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}