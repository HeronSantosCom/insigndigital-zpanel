<?php

class layout_dns extends main {

    public function __construct() {
        if (isset($_GET["parent_id"])) {
            $this->extract($_GET);
            $dominio = dao_dominio::pegar($this->parent_id, logon::meu_id());
            if ($dominio) {
                $this->extract($dominio, "dominio");
                if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
                    $this->formulario = true;
                    if (!empty($_GET["id"])) {
                        if (!$this->abrir($_GET["id"])) {
                            define("app_layout_error", true);
                            return false;
                        }
                    }
                    if (isset($_GET["remover"])) {
                        $this->remover = true;
                    }
                    if (isset($_POST["id"])) {
                        $this->salvar($_POST["id"]);
                    }
                }
                $this->zona_estado = "danger";
                $this->zona_titulo = "Erro";
                $this->zona_mensagem = "Os erros detectados tem impedido que sua zona de DNS seja carregada. Por favor, corrija os erros listados abaixo. Até que estes erros sejam corrigidos, o DNS não irá funcionar.";
                $status = dao_dns::status($this->dominio_id, false);
                if ($status) {
                    $this->zona_checkout = $status[1];
                    switch ($status[0]) {
                        case "1":
                            $this->zona_estado = "success";
                            $this->zona_titulo = "OK";
                            $this->zona_mensagem = "Sua zona de DNS foi carregada sem erros.";
                            break;
                        case "2":
                            $this->zona_estado = "warning";
                            $this->zona_titulo = "Problemas";
                            $this->zona_mensagem = "Sua zona de DNS foi carregada, mas com erros. Alguns recursos podem não funcionar até corrigido.";
                            break;
                    }
                }
                $this->dns = dao_dns::listar(logon::meu_id(), $this->dominio_id, false, false);
                if ($this->dns) {
                    $entradas = false;
                    foreach ($this->dns as $row) {
                        $row["remover"] = ($row["subdominio_id"] ? false : true);
                        $entradas[$row["tipo"]][] = $row;
                    }
                    $this->a = (!empty($entradas["A"]) ? $entradas["A"] : false);
                    $this->aaaa = (!empty($entradas["AAAA"]) ? $entradas["AAAA"] : false);
                    $this->cname = (!empty($entradas["CNAME"]) ? $entradas["CNAME"] : false);
                    $this->mx = (!empty($entradas["MX"]) ? $entradas["MX"] : false);
                    $this->txt = (!empty($entradas["TXT"]) ? $entradas["TXT"] : false);
                    $this->srv = (!empty($entradas["SRV"]) ? $entradas["SRV"] : false);
                    $this->spf = (!empty($entradas["SPF"]) ? $entradas["SPF"] : false);
                    $this->ns = (!empty($entradas["NS"]) ? $entradas["NS"] : false);
                }
                return true;
            }
        }
        define("app_layout_error", true);
    }

    private function abrir($id) {
        $dao = (dao_dns::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o registro de DNS do domínio, verifique os dados e tente novamente!");
            $action = dao_dns::cadastrar($this->tipo, $this->valor, $this->ttl, $this->alvo, $this->prioridade, $this->peso, $this->porta, $this->dominio_id, false, logon::meu_id());
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Registro de DNS do domínio salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o registro de DNS do domínio, tente novamente!");
        if ($id) {
            $action = dao_dns::remover($id);
            if ($action) {
                $this->msgbox("Registro de DNS do domínio removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}