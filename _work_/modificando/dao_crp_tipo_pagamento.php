<?php

class _dao_crp_tipo_pagamento {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("crp_tipo_pagamento");
        $db->column("*");
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            return $dao[0];
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("crp_tipo_pagamento");
        $db->column("*");
        $db->is("desativado", false);
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = $row;
            }
        }
        return $array;
    }

}