<?php

if (!class_exists("pagseguro", false)) {
    include path::plugins("pagseguro.php");
}

class layout_saldo extends main {

    public function __construct() {
        $this->sem_cadastro = true;
        if (isset($_GET["cadastrar"])) {
            $this->formulario = "inserir";
            if (isset($_POST["tipo_pagamento_id"])) {
                $this->salvar();
            }
            $this->valores = dao_crp_tipo_valor::listar();
            $this->pagamentos = dao_crp_tipo_pagamento::listar();
        } else {
            $cadastrado = array(date("Y-m-d", strtotime("-30 days")), date("Y-m-d"));
            $this->periodo_inicial = date("d/m/Y", strtotime("-30 days"));
            $this->periodo_final = date("d/m/Y");
            $this->transacoes = dao_crp_transacao::listar(logon::meu_id(), false, array(1, 2, 3, 4, 5, 8), $cadastrado); // 6,7
            $this->debitos = dao_crp_transacao::listar(logon::meu_id(), false, array(6, 7), $cadastrado, true); // 1,2,3,4,5,8
        }
        $saldo = dao_usuario::saldo(logon::meu_id());
        $this->saldo_br = $saldo["saldo_br"];
        $this->valor_diario_br = number_format((logon::meu_plano_valor() / cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"))), 2, ',', '.');
    }

    private function salvar() {
        $this->extract($_POST);
        $this->msgbox("Não foi possível efetuar o pagamento, tente novamente!");
        $tipo_pagamento = dao_crp_tipo_pagamento::pegar($this->tipo_pagamento_id);
        $tipo_valor = dao_crp_tipo_valor::pegar($this->valor_id);
        if ($tipo_pagamento && $tipo_valor) {
            $codigo = dao_crp_transacao::gera_codigo(logon::meu_id());
            $action = dao_crp_transacao::cadastrar(getmypid(), $codigo, "Crédito em Conta via {$tipo_pagamento["nome"]}", $tipo_valor["valor"], date("Y-m-d H:i:s"), false, 1, logon::meu_id());
            if ($action) {
                $_SESSION["saldo"]["id"] = $action;
                $pg = new pagseguro($codigo, logon::meu_nome(), logon::meu_email());
                $pg->item("Crédito para Conta Insign Digital - REF. {$codigo}", number_format($tipo_valor["valor"], 2, '.', ''));
                if (!$pg->go()) {
                    unset($_SESSION["saldo"]["id"]);
                    dao_crp_transacao::cancelar($_SESSION["saldo"]["id"]);
                    $this->session_msgbox();
                    $this->msgbox("Não foi possível efetuar o pagamento, tente novamente!");
                }
            }
        }
    }

}