<?php

class dao_crp_transacao {

    public static function pegar($id, $codigo = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("crp_transacao");
        $db->column("*");
        if ($id) {
            $db->match("id", $id);
        }
        if ($codigo) {
            $db->match("codigo", $codigo);
        }
        $dao = $db->go();
        if ($dao[0]) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $efetivado = false, $crp_tipo_transacao_id = false, $cadastrado = false, $agrupado = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("crp_transacao");
        $db->column("*");

        $db->match("usuario_id", $usuario_id);

        if ($efetivado) {
            if (is_array($cadastrado)) {
                $db->between("DATE(efetivado)", $efetivado);
            } else {
                $db->match("DATE(efetivado)", $efetivado);
            }
        }

        if ($crp_tipo_transacao_id) {
            if (is_array($crp_tipo_transacao_id)) {
                $db->in("crp_tipo_transacao_id", $crp_tipo_transacao_id);
            } else {
                $db->match("crp_tipo_transacao_id", $crp_tipo_transacao_id);
            }
        }

        if ($cadastrado) {
            if (is_array($cadastrado)) {
                $db->between("DATE(cadastrado)", $cadastrado);
            } else {
                $db->match("DATE(cadastrado)", $cadastrado);
            }
        }

        if ($agrupado) {
            $db->column("MONTH(cadastrado)", false, "mes");
            $db->column("YEAR(cadastrado)", false, "ano");
            $db->column("SUM(valor)", false, "total");
            $db->column("COUNT(id)", false, "qtd");

            $db->group("mes");
            $db->group("ano");
            $db->group("crp_tipo_transacao_id");
            $db->group("descricao");
            $db->group("valor");

            $db->order("ano");
            $db->order("mes");
            $db->order("crp_tipo_transacao_id");
        } else {
            $db->order("cadastrado");
        }

        $db->is("cancelado", false);
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["valor_br"] = number_format($row["valor"], 5, ',', '.');
        $row["valor_br2"] = number_format($row["valor"], 2, ',', '.');
        $row["cadastrado_br"] = date("d/m/Y", strtotime($row["cadastrado"]));
        $row["cadastrado_br2"] = date("d/m", strtotime($row["cadastrado"]));
        $row["efetivado_br"] = ($row["efetivado"] ? date("d/m/Y", strtotime($row["efetivado"])) : false);
        $row["efetivado"] = ($row["efetivado"] ? '1' : '0');
        if (!empty($row["total"])) {
            $row["total_br"] = number_format($row["total"], 5, ',', '.');
            $row["total_br2"] = number_format($row["total"], 2, ',', '.');
        }
        return $row;
    }

    public static function cadastrar($pid, $codigo, $descricao, $valor, $cadastrado, $efetivado, $crp_tipo_transacao_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("pid", $pid);
        $db->column("codigo", $codigo);
        $db->column("descricao", $descricao);
        $db->column("valor", $valor);
        $db->column("cadastrado", $cadastrado);
        $db->column("efetivado", $efetivado);
        $db->column("crp_tipo_transacao_id", $crp_tipo_transacao_id);
        $db->column("usuario_id", $usuario_id);
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function efetivar($id, $codigo = false) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("efetivado", date("Y-m-d H:i:s"));
        if ($id) {
            $db->match("id", $id);
        }
        if ($codigo) {
            $db->match("codigo", $codigo);
        }
        return $db->go();
    }

    public static function cancelar($id, $codigo = false) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("cancelado", date("Y-m-d H:i:s"));
        if ($id) {
            $db->match("id", $id);
        }
        if ($codigo) {
            $db->match("codigo", $codigo);
        }
        return $db->go();
    }

    public static function gera_codigo($usuario_id) {
//        return substr(sha1(uniqid(date("r") . $usuario_id . mt_rand(), true)), 0, 8);
        return knife::generate_uid(date("r") . $usuario_id, 8);
    }

}