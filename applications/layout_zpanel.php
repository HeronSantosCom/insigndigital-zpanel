<?php

class layout_zpanel extends main {

    public function __construct() {
        if (logon::meu_zpanel_id() == "1") {
            $this->host = logon::meu_servidor_hostname();
            $this->usuario = logon::meu_zpanel_usuario();
            $this->senha = logon::meu_zpanel_senha();
        }
    }

}