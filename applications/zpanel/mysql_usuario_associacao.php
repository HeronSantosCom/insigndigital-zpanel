<?php

class zpanel_mysql_usuario_associacao extends api_zpanel {

    static public function listar($mysql_usuario_id) {
        $arr = self::PostXMWS("mysql_users", "GetDatabasesUsers", $mysql_usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["mmid"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($mysql_usuario_id, $mysql_id, $usuario_id) {
        $arr = self::PostXMWS("mysql_users", "AddDBUsers", "<uid>{$usuario_id}</uid><myuserid>{$mysql_usuario_id}</myuserid><dbid>{$mysql_id}</dbid>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function remover($id, $mysql_usuario_id) {
        $arr = self::PostXMWS("mysql_users", "RemoveDBUsers", "<myuserid>{$mysql_usuario_id}</myuserid><mapid>{$id}</mapid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
