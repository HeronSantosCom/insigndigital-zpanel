<?php

class zpanel_apelido_email extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("aliases", "GetAliases", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($endereco, $endereco_destino, $usuario_id) {
        $endereco = explode("@", $endereco);
        if (!empty($endereco[1])) {
            $arr = self::PostXMWS("aliases", "CreateAlias", "<uid>{$usuario_id}</uid><address>{$endereco[0]}</address><domain>{$endereco[1]}</domain><destination>{$endereco_destino}</destination>");
            if (isset($arr['id']) and $arr['id'] != "false") {
                return $arr['id'];
            }
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("aliases", "DeleteAlias", "<aliasid>{$id}</aliasid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
