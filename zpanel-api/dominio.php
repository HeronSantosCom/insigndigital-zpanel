<?php

class zpanel_dominio extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("domains", "GetDomainsForUser", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($dominio, $diretorio, $www, $redirecionar, $estacionado, $usuario_id) {
        $autohome = '1';
        if ($diretorio) {
            $autohome = '0';
            if (substr($diretorio, 0, 1) == "/") {
                $diretorio = substr($diretorio, 1);
            }
        }
        $arr = self::PostXMWS("domains", "CreateDomain", "<uid>{$usuario_id}</uid><domain>{$dominio}</domain><destination>{$diretorio}</destination><autohome>{$autohome}</autohome><redirect>{$redirecionar}</redirect><wwwtype>{$www}</wwwtype><parked>{$estacionado}</parked>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function atualizar($id, $dominio, $diretorio, $www, $redirecionar, $estacionado, $usuario_id) {
        $autohome = '1';
        if ($diretorio) {
            $autohome = '0';
            if (substr($diretorio, 0, 1) == "/") {
                $diretorio = substr($diretorio, 1);
            }
        }
        $arr = self::PostXMWS("domains", "UpdateDomain", "<uid>{$usuario_id}</uid><domainid>{$id}</domainid><domain>{$dominio}</domain><destination>{$diretorio}</destination><autohome>{$autohome}</autohome><redirect>{$redirecionar}</redirect><wwwtype>{$www}</wwwtype><parked>{$estacionado}</parked>");
        if (isset($arr['updated']) and $arr['updated'] == "true") {
            return $arr['updated'];
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("domains", "DeleteDomain", "<domainid>{$id}</domainid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
