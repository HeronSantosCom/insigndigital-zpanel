<?php

class api_google {

    public static function endereco_by_cep($cep) {
        $retorno = array(
            "cep" => ""
            , "bairro" => ""
            , "cidade" => ""
            , "estado" => ""
            , "uf" => ""
            , "pais" => ""
        );
        $out = json_decode(knife::open("http://maps.googleapis.com/maps/api/geocode/json?address={$cep}&sensor=false"));
        if (!empty($out->status) && $out->status == "OK") {
            if (!empty($out->results[0]->address_components)) {
                $out = $out->results[0]->address_components;
                foreach ($out as $row) {
                    switch (array_shift($row->types)) {
                        case "postal_code":
                            $retorno["cep"] = $row->long_name;
                            break;
                        case "sublocality":
                            $retorno["bairro"] = $row->long_name;
                            break;
                        case "locality":
                            $retorno["cidade"] = $row->long_name;
                            break;
                        case "administrative_area_level_1":
                            $retorno["estado"] = $row->long_name;
                            $retorno["uf"] = $row->short_name;
                            break;
                        case "country":
                            $retorno["pais"] = $row->long_name;
                            break;
                    }
                }
            }
        }
        return $retorno;
    }

}
