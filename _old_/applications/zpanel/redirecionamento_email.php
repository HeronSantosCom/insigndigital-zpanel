<?php

class zpanel_redirecionamento_email extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("forwarders", "GetForwarders", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($endereco, $endereco_destino, $original, $usuario_id) {
        $endereco_destino = explode("@", $endereco_destino);
        if (!empty($endereco_destino[1])) {
            $arr = self::PostXMWS("forwarders", "CreateForwarder", "<uid>{$usuario_id}</uid><address>{$endereco}</address><dname>{$endereco_destino[0]}</dname><ddomain>{$endereco_destino[1]}</ddomain><keepmessage>{$original}</keepmessage>");
            if (isset($arr['id']) and $arr['id'] != "false") {
                return $arr['id'];
            }
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("forwarders", "DeleteForwarder", "<forwarderid>{$id}</forwarderid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
