<?php

class dao_endereco {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("endereco");
        $db->column("*");
        $db->match("id", $id);
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $tipo_endereco = dao_tipo_endereco::pegar($dao[0]["tipo_endereco_id"]);
            return self::hook($dao[0], $tipo_endereco);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("endereco");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("local");
        $dao = $db->go();
        if ($dao) {
            $dao_tipo_endereco = dao_tipo_endereco::listar();
            foreach ($dao as $row) {
                $tipo_endereco = (!empty($dao_tipo_endereco[$row["tipo_endereco_id"]]) ? $dao_tipo_endereco[$row["tipo_endereco_id"]] : false);
                $array[$row["id"]] = self::hook($row, $tipo_endereco);
            }
        }
        return $array;
    }

    private static function hook($row, $tipo_endereco) {
        $row["tipo_endereco_nome"] = (!empty($tipo_endereco["nome"]) ? $tipo_endereco["nome"] : false);
        return $row;
    }

    public static function cadastrar($nome, $local, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $pais, $referencia, $tipo_endereco_id, $usuario_id) {
        if (!self::verificar($local)) {
            $db = new mysqlsave();
            $db->table("endereco");
            $db->column("nome", $nome);
            $db->column("local", $local);
            $db->column("cep", $cep);
            $db->column("logradouro", $logradouro);
            $db->column("numero", $numero);
            $db->column("complemento", $complemento);
            $db->column("bairro", $bairro);
            $db->column("cidade", $cidade);
            $db->column("estado", $estado);
            $db->column("pais", $pais);
            $db->column("referencia", $referencia);
            $db->column("tipo_endereco_id", $tipo_endereco_id);
            $db->column("usuario_id", $usuario_id);
            $db->column("cadastrado", date("Y-m-d H:i:s"));
            $db->column("atualizado", date("Y-m-d H:i:s"));
            if ($db->go()) {
                return $db->id();
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $local, $cep, $logradouro, $numero, $complemento, $bairro, $cidade, $estado, $pais, $referencia, $tipo_endereco_id) {
        $db = new mysqlsave();
        $db->table("endereco");
        $db->column("nome", $nome);
        $db->column("local", $local);
        $db->column("cep", $cep);
        $db->column("logradouro", $logradouro);
        $db->column("numero", $numero);
        $db->column("complemento", $complemento);
        $db->column("bairro", $bairro);
        $db->column("cidade", $cidade);
        $db->column("estado", $estado);
        $db->column("estado", $estado);
        $db->column("pais", $pais);
        $db->column("referencia", $referencia);
        $db->column("tipo_endereco_id", $tipo_endereco_id);
        $db->column("atualizado", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        return $db->go();
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("endereco");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        if ($db->go()) {
            self::remover_relacionados($id);
            return true;
        }
        return false;
    }

    private static function remover_relacionados($id) {
        // remove apelido de email
        $db = new mysqlsave();
        $db->table("apelido_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
    }

    public static function verificar($local) {
        $db = new mysqlsearch();
        $db->table("endereco");
        $db->column("id");
        $db->match("local", $local);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function descobrir($cep) {
        $retorno = array(
            "cep" => ""
            , "bairro" => ""
            , "cidade" => ""
            , "estado" => ""
            , "uf" => ""
            , "pais" => ""
        );
        $endereco = json_decode(knife::open("http://maps.googleapis.com/maps/api/geocode/json?address={$cep}&sensor=false"));
        if (!empty($endereco->status) && $endereco->status == "OK") {
            if (!empty($endereco->results[0]->address_components)) {
                $endereco = $endereco->results[0]->address_components;
                foreach ($endereco as $row) {
                    switch (array_shift($row->types)) {
                        case "postal_code":
                            $retorno["cep"] = $row->long_name;
                            break;
                        case "sublocality":
                            $retorno["bairro"] = $row->long_name;
                            break;
                        case "locality":
                            $retorno["cidade"] = $row->long_name;
                            break;
                        case "administrative_area_level_1":
                            $retorno["estado"] = $row->long_name;
                            $retorno["uf"] = $row->short_name;
                            break;
                        case "country":
                            $retorno["pais"] = $row->long_name;
                            break;
                    }
                }
            }
        }
        return $retorno;
    }

}