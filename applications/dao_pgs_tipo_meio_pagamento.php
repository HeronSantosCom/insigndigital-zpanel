<?php

class dao_pgs_tipo_meio_pagamento {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("pgs_tipo_meio_pagamento");
        $db->column("*");
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("pgs_tipo_meio_pagamento");
        $db->column("*");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = $row;
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

}