<?php

class dao_spool_imap {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("spool_imap");
        $db->column("*");
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($cadastrado = false, $lido = false, $de = false, $para = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("spool_imap");
        $db->column("*");
        if ($cadastrado) {
            if (is_array($cadastrado)) {
                $db->between("cadastrado", $cadastrado);
            } else {
                $db->match("cadastrado", $cadastrado);
            }
        }
        if ($lido) {
            if (is_array($lido)) {
                $db->between("lido", $lido);
            } else {
                $db->match("lido", $lido);
            }
        }
        if ($de) {
            $db->match("de", $de);
        }
        if ($para) {
            $db->match("para", $para);
        }
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["lido_br"] = date("d/m/Y H:i", strtotime($row["lido"]));
        $row["cadastrado_br"] = date("d/m/Y", strtotime($row["cadastrado"]));
        return $row;
    }

    public static function cadastrar($de, $para, $assunto, $mensagem) {
        $db = new mysqlsave();
        $db->table("spool_imap");
        $db->column("de", $de);
        $db->column("para", $para);
        $db->column("assunto", $assunto);
        $db->column("mensagem", $mensagem);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function marcar_lido($id) {
        $db = new mysqlsave();
        $db->table("spool_imap");
        $db->column("lido", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        if ($db->go()) {
            return true;
        }
        return false;
    }

}