<?php

class dao_cloud_regiao {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("cloud_regiao");
        $db->column("*");
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            return $dao[0];
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("cloud_regiao");
        $db->column("*");
        $db->order("nome");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = $row;
            }
        }
        return $array;
    }

    public static function digitalocean_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["digitalocean_id"]) ? $dao["digitalocean_id"] : false);
    }

}