<?php

class zpanel_plano extends api_zpanel {

    static public function pegar($id) {
        $arr = self::PostXMWS("packages", "GetContentPackages", $id);
        if (isset($arr["list"])) {
            return $arr["list"];
        }
        return false;
    }

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("packages", "GetPackages", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["packageid"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($nome, $php_habilitado, $cgi_habilitado, $total_dominio, $total_subdominio, $total_email, $total_redirecionamento_email, $total_lista_email, $total_ftp, $total_mysql, $total_espaco, $total_transferencia, $usuario_id) {
//        $nome = substr(sha1(uniqid($nome . mt_rand(), true)), 0, 8);
        $nome = knife::generate_uid($nome, 8);
        $php_habilitado = ($php_habilitado ? '1' : '0');
        $cgi_habilitado = ($cgi_habilitado ? '1' : '0');
        $arr = self::PostXMWS("packages", "CreatePackage", "<uid>{$usuario_id}</uid><packagename>{$nome}</packagename><EnablePHP>{$php_habilitado}</EnablePHP><EnableCGI>{$cgi_habilitado}</EnableCGI><Domains>{$total_dominio}</Domains><SubDomains>{$total_subdominio}</SubDomains><ParkedDomains>{$total_dominio}</ParkedDomains><Mailboxes>{$total_email}</Mailboxes><Fowarders>{$total_redirecionamento_email}</Fowarders><DistLists>{$total_lista_email}</DistLists><FTPAccounts>{$total_ftp}</FTPAccounts><MySQL>{$total_mysql}</MySQL><DiskQuota>{$total_espaco}</DiskQuota><BandQuota>{$total_transferencia}</BandQuota>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function atualizar($id, $nome, $php_habilitado, $cgi_habilitado, $total_dominio, $total_subdominio, $total_email, $total_redirecionamento_email, $total_lista_email, $total_ftp, $total_mysql, $total_espaco, $total_transferencia, $usuario_id) {
//        $nome = substr(sha1(uniqid($nome . mt_rand(), true)), 0, 8);
        $nome = knife::generate_uid($nome, 8);
        $php_habilitado = ($php_habilitado ? '1' : '0');
        $cgi_habilitado = ($cgi_habilitado ? '1' : '0');
        $arr = self::PostXMWS("packages", "UpdatePackage", "<uid>{$usuario_id}</uid><pid>{$id}</pid><packagename>{$nome}</packagename><EnablePHP>{$php_habilitado}</EnablePHP><EnableCGI>{$cgi_habilitado}</EnableCGI><Domains>{$total_dominio}</Domains><SubDomains>{$total_subdominio}</SubDomains><ParkedDomains>{$total_dominio}</ParkedDomains><Mailboxes>{$total_email}</Mailboxes><Fowarders>{$total_redirecionamento_email}</Fowarders><DistLists>{$total_lista_email}</DistLists><FTPAccounts>{$total_ftp}</FTPAccounts><MySQL>{$total_mysql}</MySQL><DiskQuota>{$total_espaco}</DiskQuota><BandQuota>{$total_transferencia}</BandQuota>");
        if (isset($arr['updated']) and $arr['updated'] == "true") {
            return $arr['updated'];
        }
        return false;
    }

    static public function remover($id, $plano_id) {
        $arr = self::PostXMWS("packages", "DeletePackage", "<package_id>{$id}</package_id><move_package_id>{$plano_id}</move_package_id>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
