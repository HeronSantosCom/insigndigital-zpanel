<?php

if (!class_exists("digitalocean_imagem", false)) {
    include path::applications("digitalocean/imagem.php");
}

class dao_cloud_imagem {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("cloud_imagem");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $cloud_tipo_imagem_id = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("cloud_imagem");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        if ($cloud_tipo_imagem_id) {
            $db->match("cloud_tipo_imagem_id", $cloud_tipo_imagem_id);
        }
        $db->is("removido", false);
        $db->order("cloud_tipo_imagem_id");
        $db->order("nome");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

    public static function cadastrar($nome, $cloud_servidor_id, $cloud_tipo_imagem_id, $usuario_id) {
        if (!self::verificar($nome)) {
            return self::cadastrar_imagem($nome, $digitalocean_id, $cloud_servidor_id, $cloud_tipo_imagem_id, $usuario_id);
        }
        return false;
    }

    public static function cadastrar_imagem($nome, $digitalocean_id, $cloud_servidor_id, $cloud_tipo_imagem_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("cloud_imagem");
        $db->column("nome", $nome);
        $db->column("cloud_servidor_id", $cloud_servidor_id);
        $db->column("cloud_tipo_imagem_id", $cloud_tipo_imagem_id);
        $db->column("usuario_id", $usuario_id);
        $db->column("digitalocean_id", $digitalocean_id);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        $db->column("atualizado", date("Y-m-d H:i:s"));
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("cloud_imagem");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function verificar($nome, $cloud_servidor_id, $usuario_id) {
        $db = new mysqlsearch();
        $db->table("cloud_imagem");
        $db->column("id");
        $db->match("nome", $nome);
        $db->match("cloud_servidor_id", $cloud_servidor_id);
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function digitalocean_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["digitalocean_id"]) ? $dao["digitalocean_id"] : false);
    }

}