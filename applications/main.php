<?php

class main extends logon {

    public function __construct() {
        // carrega logon
        parent::__construct();

        // verifica se está chamando o módulo de pagamento
        $subdominio = substr(domain, 0, strpos(domain, "."));
        switch ($subdominio) {
            case "pagseguro":
            case "moip":
                new crp($subdominio);
                die();
                break;
        }

        // redefine o titulo
        $this->titulo(false);
        $this->session_msgbox();

        //verifica se não está logado, abre as determinadas páginas
        if (!parent::meu_id()) {
            switch ($_SERVER["REDIRECT_URL"]) {
                case "/entrar.html":
                    return true;
                    break;
                case "/cadastrar.html":
                    return $this->cadastrar();
                    break;
                case "/redefinir.html":
                    return $this->redefinir_senha();
                    break;
                default:
                    knife::redirect("entrar.html");
                    break;
            }
        }

        // verifica se está chamando o módulo beta
        if (isset($_GET["beta"])) {
            new beta();
            die();
        }

        // verifica se está chamando o módulo beta
        if (isset($_GET["saldo"])) {
            unset($_SESSION["saldo"]["id"]);
            $this->session_msgbox("Pagamento em Processamento!");
            knife::redirect("/index.html?m=saldo");
        }

        if (!empty($_SESSION["saldo"]["id"])) {
            dao_crp_transacao::cancelar($_SESSION["saldo"]["id"]);
            unset($_SESSION["saldo"]["id"]);
        }

        // se não for ajax nem index, retorna ao index senão carrega
        switch ($_SERVER["REDIRECT_URL"]) {
            case "/index.html":
            case "/xhr.html":
                return $this->layout();
                break;
            default:
                knife::redirect("/index.html");
                break;
        }
    }

    public static function encode($string) {
        $md5 = new md5();
        return $md5->set($string);
    }

    public static function decode($string) {
        $md5 = new md5();
        return $md5->get($string);
    }

    private static function is_xhr() {
        return ($_SERVER["REDIRECT_URL"] == "/xhr.html");
    }

    protected function msgbox($msgbox) {
        $this->msgbox = $msgbox;
    }

    protected function session_msgbox($msgbox = false) {
        if ($msgbox) {
            $_SESSION["msgbox"] = $msgbox;
        } else {
            if (!empty($_SESSION["msgbox"])) {
                $this->msgbox($_SESSION["msgbox"]);
                unset($_SESSION["msgbox"]);
            }
        }
    }

    protected function aplicacao($aplicacao) {
        $aplicacao = "layout_{$aplicacao}";
        if (!class_exists($aplicacao, false) and file_exists(knife::application("{$aplicacao}.php"))) {
            new $aplicacao();
            if (defined("app_layout_error")) {
                return false;
            }
        }
        return true;
    }

    protected function titulo($titulo) {
        $this->page_titulo = name;
        if ($titulo) {
            $this->layout_titulo = $titulo;
            $this->page_titulo .= " // " . $this->layout_titulo;
        }
    }

    protected function nome($nome) {
        $this->layout_nome = $nome;
    }

    protected function icone($icone) {
        $this->layout_icone = $icone;
    }

    protected function descricao($descricao) {
        $this->layout_descricao = $descricao;
    }

    protected function faq($faq) {
        $this->layout_faq = $faq;
    }

    protected function css($css) {
        if (file_exists(knife::source($css))) {
            $this->layout_css = $css;
        }
    }

    protected function modulo($modulo) {
        $this->layout_modulo = knife::html($modulo);
    }

    protected function navbar($modulos) {
        $assistente = logon::meu_assistente();
        if ($assistente) {
            $arr_navbar = array();
            $arr_subnavbar = array();
            foreach ($modulos as $key => $row) {
                if ($row["exibe_navbar"]) {
                    $arr_navbar[] = $row;
                }
                if ($row["exibe_subnavbar"]) {
                    $arr_subnavbar[] = $row;
                }
            }
            $this->layout_navbar = $arr_navbar;
            $this->layout_subnavbar = $arr_subnavbar;
        }
    }

    private function layout() {
        $layout = (!empty($_GET["m"]) ? $_GET["m"] : "index");
        $modulo = dao_modulo::pegar($layout, logon::meu_grupo_id());
        if ($modulo) {
            $this->navbar(dao_modulo::listar(logon::meu_grupo_id()));
            if ($layout != "index") {
                $this->titulo(($modulo["titulo"] ? $modulo["titulo"] : $modulo["nome"]));
                $this->nome(($modulo["nome"] ? $modulo["nome"] : $modulo["titulo"]));
            }
            $this->icone($modulo["icone"]);
            $this->descricao(($modulo["descricao"] ? $modulo["descricao"] : $modulo["descricao"]));
            $this->faq(($modulo["faq"] ? $modulo["faq"] : $this->layout_titulo));
            if (self::is_xhr()) {
                return $this->aplicacao($layout);
            } else {
                if (file_exists(knife::source("layout/main/{$layout}.html"))) {
                    if ($this->aplicacao($layout)) {
                        $this->css("css/pages/{$layout}.css");
                        return $this->modulo("layout/main/{$layout}.html");
                    }
                }
                return $this->modulo("layout/main/erro.html");
            }
        }
        knife::redirect("error.html");
    }

}