<?php

class api_digitalocean {

    const api_id = '';
    const api_key = '';

//    const api_id = '';
//    const api_key = '';

    static private function connect($action, $data = false) {
        $call = "https://api.digitalocean.com/{$action}/?client_id=" . self::api_id . "&api_key=" . self::api_key . ($data ? "&{$data}" : false);
        $response = knife::open($call);
        if ($response) {
            knife::dump("DigitalOcean Connect(api.digitalocean.com): Ok!");
            $response = @json_decode($response);
            if (!empty($response->status)) {
                knife::dump("DigitalOcean Response({$action}): {$response->status}!");
                if ($response->status == "OK") {
                    return $response;
                }
            }
        }
        knife::dump("DigitalOcean Connect(api.digitalocean.com): Erro!");
        return false;
    }

    static public function post($action, $data) {
        if (is_array($data)) {
            foreach ($data as $field => $value) {
                $data[$field] = urlencode($field) . "=" . urlencode($value);
            }
        }
        return self::connect($action, join("&", $data));
    }

    static public function get($action) {
        return self::connect($action);
    }

//    static private function all_regions() {
//        return self::connect("regions");
//    }
//
//    static private function all_images() {
//        return self::connect("images");
//    }
//
//    static private function all_sizes() {
//        return self::connect("sizes");
//    }
}
