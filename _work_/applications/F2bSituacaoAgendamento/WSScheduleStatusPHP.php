<?php
require_once("WSScheduleStatus.php");
// Inicia a classe WSScheduleStatus
$WSScheduleStatus = new WSScheduleStatus();

// Cria o cabe�alho SOAP
$xmlObj = $WSScheduleStatus->add_node("","soap-env:Envelope");
$WSScheduleStatus->add_attributes($xmlObj, array("xmlns:soap-env" => "http://schemas.xmlsoap.org/soap/envelope/") );
$xmlObj = $WSScheduleStatus->add_node($xmlObj,"soap-env:Body");

// Cria  o elemento m:F2bAgendamento
$xmlObjF2bAgendamento = $WSScheduleStatus->add_node($xmlObj,"m:F2bSituacaoAgendamento");
$WSScheduleStatus->add_attributes($xmlObjF2bAgendamento, array("xmlns:m" => "http://www.f2b.com.br/soap/wsschedulestatus.xsd") );

// Cria o elemento mensagem
$xmlObj = $WSScheduleStatus->add_node($xmlObjF2bAgendamento,"mensagem");
$WSScheduleStatus->add_attributes($xmlObj, array("data" => date("Y-m-d"),
                                          "numero" => date("His")));

// Cria o elemento cliente
$xmlObj = $WSScheduleStatus->add_node($xmlObjF2bAgendamento,"cliente");
$WSScheduleStatus->add_attributes($xmlObj, array("conta" => "9023010001230123",
                                          "senha" => "123456"));

// Cria o elemento agendamento
$xmlObjAgendamento = $WSScheduleStatus->add_node($xmlObjF2bAgendamento,"agendamento");

$WSScheduleStatus->add_attributes($xmlObjAgendamento, array("txt_email" => "teste@email.com"));

//echo $WSScheduleStatus->getXML();

// envia dados
$WSScheduleStatus->send($WSScheduleStatus->getXML());
$resposta = $WSScheduleStatus->resposta;
if(strlen($resposta) > 0){
	// Reinicia a classe WSScheduleStatus, agora com uma string XML
	$WSScheduleStatus = new WSScheduleStatus($resposta);

	// LOG 
	$log = $WSScheduleStatus->pegaLog();
	echo "<html><head><title>WSScheduleStatus</title></head><body>";
	if($log["texto"] == "OK"){
		// CLIENTE
		$cliente = $WSScheduleStatus->pegaCliente();
		// AGENDAMENTOS
		$agendamento = $WSScheduleStatus->pegaAgendamento();

		echo "<table border=1><tr><td colspan='2' bgcolor='gray'><b>Log</b></td></tr>";
		foreach($log as $key => $value){
			echo '<tr><td>$log["'.$key.'"]</td><td>'.$value.'</td></tr>';
		}
		echo "<tr><td colspan='2' bgcolor='gray'><b>Cliente</b></td></tr>";
		foreach($cliente as $key => $value){
			foreach($cliente[$key] as $key2 => $value2){
				echo '<tr><td>$cliente['.$key.']["'.$key2.'"]</td><td>'.$value2.'</td></tr>';
			}
		}
		echo "<tr><td colspan='2' bgcolor='gray'><b>Agendamentos</b></td></tr>";
		foreach($agendamento as $key => $value){
			foreach($agendamento[$key] as $key2 => $value2){
				echo '<tr><td>$agendamento['.$key.']["'.$key2.'"]</td><td>'.$value2.'</td></tr>';
			}
		}

		echo "</table>";
	} else {
		echo "<table border=1><tr><td colspan='2' bgcolor='gray'><b>Log</b></td></tr>";
		foreach($log as $key => $value){
			echo '<tr><td>$log["'.$key.'"]</td><td><font color="red">'.$value.'</font></td></tr>';
		}
		echo "</table>";
	}
	echo "</body></html>";
} else {
	echo '<font color="red">Sem resposta</font>';
}

?>
