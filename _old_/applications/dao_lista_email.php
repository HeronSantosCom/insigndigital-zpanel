<?php

if (!class_exists("zpanel_lista_email", false)) {
    include path::applications("zpanel/lista_email.php");
}

class dao_lista_email {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("lista_email");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("lista_email");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("endereco");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

    public static function cadastrar($endereco, $dominio_id, $usuario_id) {
        if (dao_modulo::zpanel_disponivel("lista_email", $usuario_id)) {
            $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
            $dominio_hostname = dao_dominio::hostname($dominio_id);
            if ($usuario_zpanel_id && $dominio_hostname) {
                $endereco = "{$endereco}@{$dominio_hostname}";
                if (!self::verificar($endereco)) {
                    $zpanel_id = zpanel_lista_email::cadastrar($endereco, $usuario_zpanel_id);
                    if ($zpanel_id) {
                        $db = new mysqlsave();
                        $db->table("lista_email");
                        $db->column("endereco", $endereco);
                        $db->column("dominio_id", $dominio_id);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            return $db->id();
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_lista_email::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("lista_email");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    self::remover_relacionados($id);
                    return true;
                }
            }
        }
        return false;
    }

    private static function remover_relacionados($id) {
        // remove membros
        $db = new mysqlsave();
        $db->table("lista_email_membro");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("lista_email_id", $id);
        $db->go();
    }

    public static function verificar($endereco) {
        $db = new mysqlsearch();
        $db->table("lista_email");
        $db->column("id");
        $db->match("endereco", $endereco);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function endereco($id) {
        $dao = self::pegar($id);
        return (!empty($dao["endereco"]) ? $dao["endereco"] : false);
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}