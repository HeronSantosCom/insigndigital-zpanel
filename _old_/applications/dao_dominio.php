<?php

if (!class_exists("zpanel_dominio", false)) {
    include path::applications("zpanel/dominio.php");
}

class dao_dominio {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("dominio");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0], false);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("dominio");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("dominio");
        $dao = $db->go();
        if ($dao) {
            $zpanel_dao = zpanel_dominio::listar(dao_usuario::zpanel_id($usuario_id));
            foreach ($dao as $row) {
                $zpanel = (!empty($zpanel_dao[$row["zpanel_id"]]) ? $zpanel_dao[$row["zpanel_id"]] : false);
                $array[$row["id"]] = self::hook($row, $zpanel);
            }
        }
        return $array;
    }

    private static function hook($row, $zpanel) {
        $row["online"] = (!empty($zpanel["active"]) ? ($zpanel["active"] ? true : false) : false);
        $row["redirect"] = ($row["redirecionar"] ? true : false);
        $row["mirror"] = ($row["diretorio"] != ("/" . str_replace(".", "_", $row["dominio"])));
        return $row;
    }

    public static function cadastrar($dominio, $diretorio, $www, $redirecionar, $estacionado, $usuario_id) {
        if (dao_modulo::zpanel_disponivel("dominio", $usuario_id)) {
            $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
            if ($usuario_zpanel_id) {
                if (!self::verificar($dominio)) {
                    $zpanel_id = zpanel_dominio::cadastrar($dominio, $diretorio, $www, $redirecionar, $estacionado, $usuario_zpanel_id);
                    if ($zpanel_id) {
                        $diretorio = ($diretorio ? $diretorio : ("/" . str_replace(".", "_", $dominio)));
                        $db = new mysqlsave();
                        $db->table("dominio");
                        $db->column("dominio", $dominio);
                        $db->column("diretorio", $diretorio);
                        $db->column("www", $www);
                        $db->column("redirecionar", $redirecionar);
                        $db->column("estacionado", $estacionado);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            $id = $db->id();
                            if (dao_dns::criar($id, $usuario_id)) {
                                return $id;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function atualizar($id, $diretorio, $www, $redirecionar, $estacionado, $usuario_id) {
        $dominio = self::hostname($id);
        $zpanel_id = self::zpanel_id($id);
        $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
        if ($dominio && $zpanel_id && $usuario_zpanel_id) {
            if (zpanel_dominio::atualizar($zpanel_id, $dominio, $diretorio, $www, $redirecionar, $estacionado, $usuario_id)) {
                $diretorio = ($diretorio ? $diretorio : ("/" . str_replace(".", "_", $dominio)));
                $db = new mysqlsave();
                $db->table("dominio");
                $db->column("diretorio", $diretorio);
                $db->column("www", $www);
                $db->column("redirecionar", $redirecionar);
                $db->column("estacionado", $estacionado);
                $db->column("atualizado", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_dominio::remover($zpanel_id)) {
                if (dao_dns::destruir($id, false)) {
                    $db = new mysqlsave();
                    $db->table("dominio");
                    $db->column("removido", date("Y-m-d H:i:s"));
                    $db->match("id", $id);
                    if ($db->go()) {
                        self::remover_relacionados($id);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static function remover_relacionados($id) {
        // remove apelidos de email
        $db = new mysqlsave();
        $db->table("apelido_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("dominio_id", $id);
        $db->go();
        // remove lista de emails
        $db = new mysqlsave();
        $db->table("lista_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("dominio_id", $id);
        $db->go();
        // remove emails
        $db = new mysqlsave();
        $db->table("email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("dominio_id", $id);
        $db->go();
        // remove subdominios
        dao_subdominio::remover(false, $id);
    }

    public static function verificar($dominio) {
        $db = new mysqlsearch();
        $db->table("dominio");
        $db->column("id");
        $db->match("dominio", $dominio);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function hostname($id) {
        $dao = self::pegar($id);
        return (!empty($dao["dominio"]) ? $dao["dominio"] : false);
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}