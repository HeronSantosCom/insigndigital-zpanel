<?php
require_once("WSBillingSegundaVia.php");
// Inicia a classe WSBillingSegundaVia
$WSBillingSegundaVia = new WSBillingSegundaVia();

// Cria o cabe�alho SOAP
$xmlObj = $WSBillingSegundaVia->add_node("","soap-env:Envelope");
$WSBillingSegundaVia->add_attributes($xmlObj, array("xmlns:soap-env" => "http://schemas.xmlsoap.org/soap/envelope/") );
$xmlObj = $WSBillingSegundaVia->add_node($xmlObj,"soap-env:Body");

// Cria  o elemento m:F2bCobranca
$xmlObjF2bCobranca = $WSBillingSegundaVia->add_node($xmlObj,"m:F2bSegundaVia");
$WSBillingSegundaVia->add_attributes($xmlObjF2bCobranca, array("xmlns:m" => "http://www.f2b.com.br/soap/wsbillingsegundavia.xsd") );

// Cria o elemento cliente
$xmlObj = $WSBillingSegundaVia->add_node($xmlObjF2bCobranca,"cliente");
$WSBillingSegundaVia->add_attributes($xmlObj, array("conta" => "9023010071230123","senha" => "senha123"));

// Cria o elemento sacado
$xmlObjCobranca = $WSBillingSegundaVia->add_node($xmlObjF2bCobranca,"sacado");
$WSBillingSegundaVia->add_attributes($xmlObjCobranca, array("txt_email" => "teste@email.com.br", "num_cpf" => "12345678910", "somente_registradas" => "1")); //somente_registradas:1=exibe cobran�as registradas;2=exibe cobran�as registradas e pagas

// envia dados
$WSBillingSegundaVia->send($WSBillingSegundaVia->getXML());
$resposta = $WSBillingSegundaVia->resposta;
if(strlen($resposta) > 0){
	// Reinicia a classe WSBillingSegundaVia, agora com uma string XML
	$WSBillingSegundaVia = new WSBillingSegundaVia($resposta);

	// LOG 
	$log = $WSBillingSegundaVia->pegaLog();
	echo "<html><head><title>WSBillingSegundaVia</title></head><body>";
	if($log["texto"] == "OK"){
		// Sacado
		$sacado = $WSBillingSegundaVia->pegaSacado();
		// COBRANCAS
		$cobranca = $WSBillingSegundaVia->pegaCobranca();

		echo "<table border=1><tr><td colspan='2' bgcolor='gray'><b>Log</b></td></tr>";
		foreach($log as $key => $value){
			echo '<tr><td>$log["'.$key.'"]</td><td>'.$value.'</td></tr>';
		}
		echo "<tr><td colspan='2' bgcolor='gray'><b>Sacado</b></td></tr>";
		foreach($sacado as $key => $value){
			foreach($sacado[$key] as $key2 => $value2){
				echo '<tr><td>$cliente['.$key.']["'.$key2.'"]</td><td>'.$value2.'</td></tr>';
			}
		}
		echo "<tr><td colspan='2' bgcolor='gray'><b>Cobran�as</b></td></tr>";
		foreach($cobranca as $key => $value){
			foreach($cobranca[$key] as $key2 => $value2){
				echo '<tr><td>$cobranca['.$key.']["'.$key2.'"]</td><td>'.$value2.'</td></tr>';
			}
			echo "<tr><td colspan='2' height='10'></td></tr>";
		}

		echo "</table>";
	} else {
		echo "<table border=1><tr><td colspan='2' bgcolor='gray'><b>Log</b></td></tr>";
		foreach($log as $key => $value){
			echo '<tr><td>$log["'.$key.'"]</td><td><font color="red">'.$value.'</font></td></tr>';
		}
		echo "</table>";
	}
	echo "</body></html>";
} else {
	echo '<font color="red">Sem resposta</font>';
}
?>
