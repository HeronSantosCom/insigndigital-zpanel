<?php

class dao_tipo_endereco {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("tipo_endereco");
        $db->column("*");
        $db->match("id", $id);
        $dao = $db->go();
        if ($dao) {
            return $dao[0];
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("tipo_endereco");
        $db->column("*");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = $row;
            }
        }
        return $array;
    }

}