<?php

/**
 * @package zpanelx
 * @subpackage modules
 * @author Bobby Allen (ballen@zpanelcp.com)
 * @copyright ZPanel Project (http://www.zpanelcp.com/)
 * @link http://www.zpanelcp.com/
 * @license GPL (http://www.gnu.org/licenses/gpl.html)
 */
class webservice extends ws_xmws {

    /**
     * Get the full list of currently active domains on the server.
     * @global type $zdbh
     * @return type
     */
    public function GetAllDomains() {
        $xml = "\n";
        $array = module_controller::ListDomainsAndParked();
        if (!fs_director::CheckForEmptyValue($array)) {
            foreach ($array as $row) {
                $xml .= ws_xmws::NewXMLContentSection('list', $row);
            }
        }
        $dbo = new runtime_dataobject();
        $dbo->addItemValue('response', '');
        $dbo->addItemValue('content', $xml);
        return $dbo->getDataObject();
    }

    /**
     * Gets a list of all the domains that a user has configured on their hosting account (the user id needs to be sent in the <content> tag).
     * @global type $zdbh
     * @return type
     */
    public function GetDomainsForUser() {
        $xml = "\n";
        $wsdata = $this->RawXMWSToArray($this->wsdata);
        $array = module_controller::ListDomainsAndParked($wsdata['content']);
        if (!fs_director::CheckForEmptyValue($array)) {
            foreach ($array as $row) {
                $xml .= ws_xmws::NewXMLContentSection('list', $row);
            }
        }
        $dbo = new runtime_dataobject();
        $dbo->addItemValue('response', '');
        $dbo->addItemValue('content', $xml);
        return $dbo->getDataObject();
    }

    /**
     * Enables an authenticated user to create a domain on their hosting account.
     * @return type
     */
    public function CreateDomain() {
        $wsdata = $this->RawXMWSToArray($this->wsdata);
        $dbo = new runtime_dataobject();
        $dbo->addItemValue('response', '');
        $id = module_controller::ExecuteAddDomain(
                        ws_generic::GetTagValue('uid', $wsdata['content']), ws_generic::GetTagValue('domain', $wsdata['content']), ws_generic::GetTagValue('destination', $wsdata['content']), ws_generic::GetTagValue('autohome', $wsdata['content']), ws_generic::GetTagValue('redirect', $wsdata['content']), ws_generic::GetTagValue('wwwtype', $wsdata['content']), ws_generic::GetTagValue('parked', $wsdata['content'])
        );
        if ($id) {
            $dbo->addItemValue('content', ws_xmws::NewXMLTag('id', $id));
        } else {
            $dbo->addItemValue('content', ws_xmws::NewXMLTag('id', 'false'));
        }
        return $dbo->getDataObject();
    }

    /**
     * update a domain on their hosting account.
     * @return type
     */
    public function UpdateDomain() {
        $wsdata = $this->RawXMWSToArray($this->wsdata);
        $dbo = new runtime_dataobject();
        $dbo->addItemValue('response', '');
        if (module_controller::ExecuteUpdateDomain(
                        ws_generic::GetTagValue('uid', $wsdata['content']), ws_generic::GetTagValue('domainid', $wsdata['content']), ws_generic::GetTagValue('domain', $wsdata['content']), ws_generic::GetTagValue('destination', $wsdata['content']), ws_generic::GetTagValue('autohome', $wsdata['content']), ws_generic::GetTagValue('redirect', $wsdata['content']), ws_generic::GetTagValue('wwwtype', $wsdata['content']), ws_generic::GetTagValue('parked', $wsdata['content'])
        )) {
            $dbo->addItemValue('content', ws_xmws::NewXMLTag('updated', 'true'));
        } else {
            $dbo->addItemValue('content', ws_xmws::NewXMLTag('updated', 'false'));
        }
        return $dbo->getDataObject();
    }

    /**
     * Delete a specified domain using the content <domainid> tag to pass the domain DB ID through.
     * @return type
     */
    public function DeleteDomain() {
        $wsdata = $this->RawXMWSToArray($this->wsdata);
        $contenttags = $this->XMLDataToArray($wsdata['content']);
        $dbo = new runtime_dataobject();
        $dbo->addItemValue('response', '');
        if (module_controller::ExecuteDeleteDomain($contenttags['domainid'])) {
            $dbo->addItemValue('content', ws_xmws::NewXMLTag('deleted', 'true'));
        } else {
            $dbo->addItemValue('content', ws_xmws::NewXMLTag('deleted', 'false'));
        }
        return $dbo->getDataObject();
    }

}

?>
