<?php

class digitalocean_servidor extends api_digitalocean {

    static public function listar() {
        // https://api.digitalocean.com/droplets/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","droplets":[{"backups_active":null,"id":100823,"image_id":420,"name":"test222","region_id":1,"size_id":33,"status":"active"}]}
        $arr = self::get("droplets");
        if ($arr) {
            foreach ($arr->droplets as $row) {
                $retorno[$row->id] = get_object_vars($row);
            }
            return $retorno;
        }
        return false;
    }

    static public function pegar($id) { 
        // https://api.digitalocean.com/droplets/[DROPLET_ID]?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","droplet":{"backups_active":null,"id":100823,"image_id":420,"name":"test222","region_id":1,"size_id":33,"status":"active"}}
        $arr = self::get("droplets/{$id}");
        if ($arr) {
            return get_object_vars($arr->droplet);
        }
        return false;
    }

    static public function cadastrar($hostname, $plano_id, $imagem_id, $regiao_id) {
        // https://api.digitalocean.com/droplets/new?name=[DROPLET_NAME]&size_id=[SIZE_ID]&image_id=[IMAGE_ID]&region_id=[REGION_ID]&client_id=[YOUR_CLIENT_ID]&ssh_key_ids=[SSH_KEY_ID1],[SSH_KEY_ID2]
        // {"status":"OK","droplet":{"id":100824,"name":"test","image_id":419,"size_id":32,"event_id":7499}}
        $arr = self::post("droplets/new", array(
                    "name" => $hostname,
                    "size_id" => $plano_id,
                    "image_id" => $imagem_id,
                    "region_id" => $regiao_id
                ));
        if ($arr) {
            return $arr->droplet;
        }
        return false;
    }

    static public function remover($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/destroy/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7504}
        $arr = self::get("droplets/{$id}/destroy");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function redefinir_senha($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/password_reset/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7501}
        $arr = self::get("droplets/{$id}/password_reset");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function redefinir_plano($id, $plano_id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/resize/?size_id=[SIZE_ID]&client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7501}
        $arr = self::post("droplets/{$id}/resize", array(
                    "size_id" => $plano_id
                ));
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function habilitar_backup($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/enable_backups/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7504}
        $arr = self::get("droplets/{$id}/enable_backups");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function desabilitar_backup($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/disable_backups/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7504}
        $arr = self::get("droplets/{$id}/disable_backups");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function restaurar($id, $imagem_id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/restore/?image_id=[IMAGE_ID]&client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7504}
        $arr = self::post("droplets/{$id}/restore", array(
                    "image_id" => $imagem_id
                ));
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function reconstruir($id, $imagem_id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/rebuild/?image_id=[IMAGE_ID]&client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7504}
        $arr = self::post("droplets/{$id}/rebuild", array(
                    "image_id" => $imagem_id
                ));
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function ligar($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/power_on/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7501}
        $arr = self::get("droplets/{$id}/power_on");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function reiniciar($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/power_cycle/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7501}
        $arr = self::get("droplets/{$id}/power_cycle");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function reiniciar_forcado($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/reboot/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7501}
        $arr = self::get("droplets/{$id}/reboot");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function desligar($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/shutdown/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7501}
        $arr = self::get("droplets/{$id}/shutdown");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function desligar_forcado($id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/power_off/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7501}
        $arr = self::get("droplets/{$id}/power_off");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

}
