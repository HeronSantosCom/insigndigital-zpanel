<?php

class layout_subdominios extends main {

    public function __construct() {
        if (isset($_GET["parent_id"])) {
            $this->extract($_GET);
            $dominio = dao_dominio::pegar($this->parent_id, logon::meu_id());
            if ($dominio) {
                $this->extract($dominio, "dominio");
                if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
                    $this->formulario = true;
                    if (!empty($_GET["id"])) {
                        if (!$this->abrir($_GET["id"])) {
                            define("app_layout_error", true);
                            return false;
                        }
                    }
                    if (isset($_GET["remover"])) {
                        $this->remover = true;
                    }
                    if (isset($_POST["id"])) {
                        $this->salvar($_POST["id"]);
                    }
                }
                $this->extract(dao_modulo::layout_uso("subdominio", logon::meu_id()), "uso");
                $this->subdominios = dao_subdominio::listar(logon::meu_id(), $this->dominio_id);
                $this->dominios = dao_dominio::listar(logon::meu_id());
                return true;
            }
        }
        define("app_layout_error", true);
    }

    private function abrir($id) {
        $dao = (dao_subdominio::pegar($id, false, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o subdomínio, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_subdominio::atualizar($id, ($this->mirror ? $this->diretorio : false), ($this->redirect ? $this->redirecionar : false), logon::meu_id());
            } else {
                $action = dao_subdominio::cadastrar($this->subdominio, ($this->mirror ? $this->diretorio : false), ($this->redirect ? $this->redirecionar : false), $this->dominio_id, logon::meu_id());
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Subdomínio salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o subdomínio, tente novamente!");
        if ($id) {
            $action = dao_subdominio::remover($id);
            if ($action) {
                $this->msgbox("Subdomínio removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}