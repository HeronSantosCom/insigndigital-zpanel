<?php

class layout_index extends main {

    public function __construct() {
        $this->plano_id = logon::meu_plano_id();
        if ($this->plano_id) {
            $assistente = logon::meu_assistente();
            if (!$assistente) {
                return knife::redirect("/index.html?m=assistente");
            }
            $this->uso = $this->zpanel_uso();
//            $crp_cobranca = dao_crp_cobranca::listar(logon::meu_id(), false, false, false, false, 1);
//            $this->crp_cobranca_aberto = ($crp_cobranca ? true : false);
            $saldo = dao_usuario::saldo(logon::meu_id());
            $this->saldo_cor = "found-small";
            $porcentagem_saldo = ceil(($saldo["saldo"] * 100) / logon::meu_plano_valor());
            if ($porcentagem_saldo > 35) {
                $this->saldo_cor = "found-medium";
                if ($porcentagem_saldo > 70) {
                    $this->saldo_cor = "found-full";
                }
            }
            $this->saldo_br = $saldo["saldo_br"];
            $noticia = dao_noticia::pegar(false);
            if ($noticia) {
                $this->extract($noticia, "noticia");
            }
        } else {
            $produto = dao_produtos::listar_planos(1);
            if ($produto) {
                $produto = array_shift($produto);
                $this->extract($produto);
            }
        }
    }

    private function zpanel_status() {
        $valores = dao_servidor::atividade();
        if ($valores) {
            foreach ($valores as $key => $row) {
                $estilo = "danger";
                $titulo = "Falhou!";
                if ($row["status"]) {
                    $estilo = "success";
                    $titulo = "Ok!";
                }
                $resumo[] = array("nome" => $row["nome"], "estilo" => $estilo, "titulo" => $titulo);
            }
            return $resumo;
        }
    }

    private function zpanel_uso() {
        $valores = dao_servidor::uso(logon::meu_id());
        if ($valores) {
            foreach ($valores as $key => $row) {
                $estilo = "bar-danger";
                $porcentagem = 100;
                if ($row["total"] > $row["usado"]) {
                    $porcentagem = ceil(($row["usado"] * 100) / $row["total"]);
                    if ($porcentagem < 80) {
                        $estilo = "bar-warning";
                        if ($porcentagem < 40) {
                            $estilo = "bar-success";
                        }
                    }
                }
                $resumo[] = array("nome" => $row["nome"], "ilimitado" => (isset($row["ilimitado"]) ? $row["ilimitado"] : false), "porcentagem" => $porcentagem, "estilo" => $estilo, "usado" => (isset($row["size"]) ? knife::human_size_disk($row["usado"]) : $row["usado"]), "total" => (isset($row["size"]) ? knife::human_size_disk($row["total"]) : $row["total"]));
            }
            return $resumo;
        }
        return false;
    }

}