<?php

class layout_minha_conta extends main {

    public function __construct() {
        if (isset($_POST["email"])) {
            $this->salvar();
        }
        $this->extract(self::session());
        $saldo = dao_usuario::saldo(logon::meu_id());
        $this->saldo_br = $saldo["saldo_br"];
//        $this->planos = dao_plano::listar(logon::meu_usuario_id());
    }

    private function salvar() {
        $this->extract($_POST);
        $this->msgbox("Não foi possível alterar sua conta, verifique os dados e tente novamente.");
        $action = dao_usuario::atualizar(logon::meu_id(), $this->nome, logon::meu_email(), $this->senha, $this->telefone, logon::meu_motivo(), logon::meu_bloqueado(), logon::meu_bloqueado_saldo(), 1, 1, logon::meu_plano_id(), logon::meu_grupo_id());
        if ($action) {
            self::reboot(logon::meu_id(), false, false, (self::cookie() ? '1' : '0'));
            $this->msgbox("Sua conta foi alterada com sucesso.");
        }
    }

}