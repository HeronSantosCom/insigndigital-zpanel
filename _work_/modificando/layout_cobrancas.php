<?php

if (!class_exists("pagseguro", false)) {
    include path::plugins("pagseguro.php");
}

class _layout_cobrancas extends main {

    public function __construct() {
        $this->sem_cadastro = true;
        if (!empty($_GET["pagseguro"])) {
            $pagseguro = $_GET["pagseguro"];
            $crp_cobranca = $_SESSION["pagseguro"]["crp_cobranca"];
            if (!empty($crp_cobranca["id"])) {
                dao_crp_cobranca::atualizar($crp_cobranca["id"], $pagseguro, false, 2, 2);
                knife::redirect("/index.html?m=cobrancas&editar&id={$crp_cobranca["id"]}");
            } else {
                knife::redirect("/index.html?m=cobrancas");
            }
        } else {
            if (isset($_GET["editar"])) {
                $this->formulario = true;
                if (!empty($_GET["id"])) {
                    if (!$this->abrir($_GET["id"])) {
                        define("app_layout_error", true);
                        return false;
                    }
                }
                if (isset($_POST["id"])) {
                    $this->salvar($_POST["id"]);
                }
                $this->crp_transacoes = crp_transacao::listar(logon::meu_id(), $this->id);
                switch ($this->crp_tipo_pagamento_id) {
                    case 2:
                        $this->pgs_transacoes = dao_pgs_transacao::listar($this->id);
                        break;
                }
            }
            $this->historico = crp_transacao::historico(logon::meu_id());
        }
        $this->crp_cobrancas = dao_crp_cobranca::listar(logon::meu_id());
    }

    private function abrir($id) {
        $dao = dao_crp_cobranca::pegar($id);
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $crp_cobranca = dao_crp_cobranca::pegar($id);
        if ($crp_cobranca && $crp_cobranca["crp_tipo_pagamento_status_id"] < 2) {
            $_SESSION["pagseguro"]["crp_cobranca"] = $crp_cobranca;
            $pg = new pagseguro($crp_cobranca["codigo"], logon::meu_nome(), logon::meu_email());
            $pg->item("Serviços Diversos", number_format($crp_cobranca["valor"], 2, '.', ''));
            if (!$pg->go()) {
                $this->msgbox("Não foi possível efetuar o pagamento, tente novamente!");
            }
        } else {
            $this->msgbox("Pagamento já processado!");
        }
    }

}