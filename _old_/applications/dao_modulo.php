<?php

class dao_modulo {

    public static function pegar($layout, $grupo_id) {
        $db = new mysqlsearch();
        $db->table("modulo");
        $db->column("*");
        $db->match("layout", $layout);
        $db->morethan("grupo_id", $grupo_id);
        $db->match("ativo", "1");
        $dao = $db->go();
        if ($dao) {
            return $dao[0];
        }
        return false;
    }

    public static function listar($grupo_id) {
        $db = new mysqlsearch();
        $db->table("modulo");
        $db->column("*");
        $db->order("prioridade");
        $db->morethan("grupo_id", $grupo_id);
        $db->match("ativo", "1");
        $dao = $db->go();
        if ($dao) {
            return self::getOrganizarModulos($dao, $grupo_id);
        }
        return false;
    }

    private static function getOrganizarModulos($array, $grupo_id) {
        if (is_array($array)) {
            $menu = false;
            foreach ($array as $row) {
                $show = true;
                if ($row["bloqueado"]) {
                    $show = false;
                    if ($grupo_id == $row["grupo_id"]) {
                        $show = true;
                    }
                }
                if ($show) {
                    $row["sub"] = false;
                    if (!$row["href"]) {
                        $row["href"] = "javascript:;";
                        if ($row["layout"]) {
                            $row["href"] = "?m={$row["layout"]}";
                        }
                    }
                    $menu[$row["id"]] = $row;
                }
            }
            if ($menu) {
                foreach ($menu as $row) {
                    if ($row["modulo_id"]) {
                        $menu[$row["modulo_id"]]["sub"][$row["id"]] = true;
                    }
                }
                $_ENV["menu"] = $menu;
                $menu = false;
                foreach ($_ENV["menu"] as $idx => $rows) {
                    if (empty($rows["modulo_id"])) {
                        if (!empty($rows["id"])) {
                            $menu[$idx] = self::getSubsModulos($rows["id"]);
                        }
                    }
                }
                $_ENV["menu"] = $menu;
            }
        }
        return $_ENV["menu"];
    }

    private static function getSubsModulos($idx) {
        $array = false;
        if (!empty($_ENV["menu"][$idx])) {
            $array = $_ENV["menu"][$idx];
            $subs = (!empty($array["sub"]) ? $array["sub"] : false);
            if ($subs) {
                foreach ($subs as $subs_idx => $value) {
                    $array["sub"][$subs_idx] = self::getSubsModulos($subs_idx);
                }
            }
        }
        return $array;
    }

    public static function zpanel_uso($modulo, $usuario_id) {
        $dao = dao_servidor::uso($usuario_id);
        return (!empty($dao[$modulo]) ? $dao[$modulo] : false);
    }

    public static function zpanel_disponivel($modulo, $usuario_id) {
        $dao = self::zpanel_uso($modulo, $usuario_id);
        if ($dao) {
            if ($dao["disponivel"] > 0) {
                return true;
            }
        }
        return false;
    }

    public static function layout_uso($modulo, $usuario_id) {
        $dao = dao_modulo::zpanel_uso($modulo, $usuario_id);
        if ($dao) {
            $estilo = "bar-danger";
            $porcentagem = 100;
            if ($dao["total"] > $dao["usado"]) {
                $porcentagem = ceil(($dao["usado"] * 100) / $dao["total"]);
                if ($porcentagem < 80) {
                    $estilo = "bar-warning";
                    if ($porcentagem < 40) {
                        $estilo = "bar-success";
                    }
                }
            }
            return array("nome" => $dao["nome"], "porcentagem" => $porcentagem, "estilo" => $estilo, "usado" => (isset($dao["size"]) ? knife::human_size_disk($dao["usado"]) : $dao["usado"]), "total" => (isset($dao["size"]) ? knife::human_size_disk($dao["total"]) : $dao["total"]));
        }
        return false;
    }

}