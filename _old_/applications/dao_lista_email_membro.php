<?php

if (!class_exists("zpanel_lista_email_membro", false)) {
    include path::applications("zpanel/lista_email_membro.php");
}

class dao_lista_email_membro {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("lista_email_membro");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $lista_email = dao_lista_email::pegar($dao[0]["lista_email_id"]);
            return self::hook($dao[0], $lista_email);
        }
        return false;
    }

    public static function listar($usuario_id, $lista_email_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("lista_email_membro");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->match("lista_email_id", $lista_email_id);
        $db->is("removido", false);
        $db->order("endereco");
        $dao = $db->go();
        if ($dao) {
            $dao_lista_email = dao_lista_email::listar($usuario_id);
            foreach ($dao as $row) {
                $lista_email = (!empty($dao_lista_email[$row["lista_email_id"]]) ? $dao_lista_email[$row["lista_email_id"]] : false);
                $array[$row["id"]] = self::hook($row, $lista_email);
            }
        }
        return $array;
    }

    private static function hook($row, $lista_email) {
        $row["lista_email_endereco"] = (!empty($lista_email["endereco"]) ? $lista_email["endereco"] : false);
        return $row;
    }

    public static function cadastrar($endereco, $lista_email_id, $usuario_id) {
        $lista_email_zpanel_id = dao_lista_email::zpanel_id($lista_email_id);
        if ($lista_email_zpanel_id) {
            if (!self::verificar($endereco, $lista_email_id)) {
                $zpanel_id = zpanel_lista_email_membro::cadastrar($endereco, $lista_email_zpanel_id);
                if ($zpanel_id) {
                    $db = new mysqlsave();
                    $db->table("lista_email_membro");
                    $db->column("endereco", $endereco);
                    $db->column("lista_email_id", $lista_email_id);
                    $db->column("usuario_id", $usuario_id);
                    $db->column("zpanel_id", $zpanel_id);
                    $db->column("cadastrado", date("Y-m-d H:i:s"));
                    $db->column("atualizado", date("Y-m-d H:i:s"));
                    if ($db->go()) {
                        return $db->id();
                    }
                }
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_lista_email_membro::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("lista_email_membro");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function verificar($endereco, $lista_email_id) {
        $db = new mysqlsearch();
        $db->table("lista_email_membro");
        $db->column("id");
        $db->match("endereco", $endereco);
        $db->match("lista_email_id", $lista_email_id);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}