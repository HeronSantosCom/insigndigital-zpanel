<?php

class beta extends api_zpanel {

	private function comunicar($funcao, $data) {
		try {
			$return = self::PostXMWS("kengoo", $funcao, json_encode($data));
			if ($return) {
				return json_decode($return);
			}
			return array( 'status' => false, 'response' => "No response!", 'data' => false, 'id' => false );
		} catch (Exception $e) {
			return array( 'status' => false, 'response' => $e->getMessage(), 'data' => false, 'id' => false );
		}
	}

    public function __construct() {
        $arrau = array(
			'uid' => 111111,
			'verdadeiro' => true,
			'false' => false,
			'zero' => 0,
			'algumacoisa' => 'alguma coisa',
			'array' => array(
				'uid' => 111111,
				'verdadeiro' => true,
				'false' => false,
				'zero' => 0,
				'algumacoisa' => 'alguma coisa',
				'array' => 'alguma coisa'
			)
		);
		print "<pre>";
		var_dump($this->comunicar("dominio_listar_todos", $arrau));
		print "</pre>";
    }

}