<?php

if (!class_exists("zpanel_redirecionamento_email", false)) {
    include path::applications("zpanel/redirecionamento_email.php");
}

class dao_redirecionamento_email {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("redirecionamento_email");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $email = dao_email::pegar($dao[0]["email_id"]);
            return self::hook($dao[0], $email);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("redirecionamento_email");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $dao_email = dao_email::listar($usuario_id);
            foreach ($dao as $row) {
                $email = (!empty($dao_email[$row["email_id"]]) ? $dao_email[$row["email_id"]] : false);
                $array[$row["id"]] = self::hook($row, $email);
            }
        }
        return $array;
    }

    private static function hook($row, $email) {
        $row["email_endereco"] = (!empty($email["endereco"]) ? $email["endereco"] : false);
        return $row;
    }

    public static function cadastrar($endereco, $original, $email_id, $usuario_id) {
        if (dao_modulo::zpanel_disponivel("redirecionamento_email", $usuario_id)) {
            $email_endereco = dao_email::endereco($email_id);
            $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
            if ($usuario_zpanel_id && $email_endereco) {
                if (!self::verificar($endereco, $email_id)) {
                    $zpanel_id = zpanel_redirecionamento_email::cadastrar($email_endereco, $endereco, ($original ? '1' : '0'), $usuario_zpanel_id);
                    if ($zpanel_id) {
                        $db = new mysqlsave();
                        $db->table("redirecionamento_email");
                        $db->column("endereco", $endereco);
                        $db->column("original", ($original ? '1' : '0'));
                        $db->column("email_id", $email_id);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            return $db->id();
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_redirecionamento_email::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("redirecionamento_email");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function verificar($endereco, $email_id) {
        $db = new mysqlsearch();
        $db->table("redirecionamento_email");
        $db->column("id");
        $db->match("endereco", $endereco);
        $db->match("email_id", $email_id);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}