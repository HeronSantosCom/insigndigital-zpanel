<?php

class digitalocean_ssh extends api_digitalocean {

    static public function listar() {
        // https://api.digitalocean.com/ssh_keys/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","ssh_keys":[{"id":10,"name":"office-imac"},{"id":11,"name":"macbook-air"}]}
        $arr = self::get("ssh_keys");
        if ($arr) {
            foreach ($arr->ssh_keys as $row) {
                $retorno[$row->id] = get_object_vars($row);
            }
            return $retorno;
        }
        return false;
    }

    static public function pegar($id) {
        // https://api.digitalocean.com/ssh_keys/[SSH_KEY_ID]/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","ssh_key":{"id":10,"name":"office-imac","ssh_pub_key":"ssh-dss AHJASDBVY6723bgBVhusadkih238723kjLKFnbkjGFklaslkhfgBAFFHGBJbju8)H3hnNGjASGFkjgZn86ZCqk02NX3BTcMV4YI2I4/sebg8VnuebDn0XUbbmVrAq4YqGiobn86ZCqk02NX3BTcMp4QGmyL4/sebg8Vnusytv93cA2PsXOxvbU0CdebDn0XUbbmVrAq4YqGiob48KzCT/NT6L6VoD5n+jSZvQAAAIAspspAelh4bW5ncO5+CedFZPZn86ZCqk02NX3BTcMV4YIaSCO43Y+ghI2of4+E1TDJ1R9Znk9XJsald/U0u0uXwtyHXP2sommNWuAGtzp4QGmyL4/sebg8Vnusytv93cA2PsXOxvbU0CdebDn0XUbbmVrAq4YqGiob48KzCT/NT6L6VoD5n+jSZflFD684gdLsW1+gjVoFBk0MZWuGSXEQyIwlBRq/8jAAAAFQDrxI/h35BewJUmVjid8Qk1NprMvQAAAIAspspAelh4bW5ncO5+CedFZPZn86ZCqk02NX3BTcMV4YI2IEzb6R2vzZkjCTuZVy6dcH3ag6JlEfju67euWT5yMnT1I0Ow== me@office-imac"}}
        $arr = self::get("ssh_keys/{$id}");
        if ($arr) {
            return get_object_vars($arr->droplet);
        }
        return false;
    }

    static public function cadastrar($nome, $chave) {
        // https://api.digitalocean.com/ssh_keys/new/?name=[SSH_KEY_NAME]&ssh_pub_key=[SSH_PUBLIC_KEY]client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","ssh_key":{"id":47,"name":"my_key","ssh_pub_key":"ssh-dss AAAAB3NzaC1kc3MAAACBAK5uLwicCrFEpaVKBzkWxC7RQn+smg5ZQb5keh9RQKo8AszFTol5npgUAr0JWmqKIHv7nof0HndO86x9iIqNjq3vrz9CIVcFfZM7poKBJZ27Hv3v0fmSKfAc6eGdx8eM9UkZe1gzcLXK8UP2HaeY1Y4LlaHXS5tPi/dXooFVgiA7AAAAFQCQl6LZo/VYB9VgPEZzOmsmQevnswAAAIBCNKGsVP5eZ+IJklXheUyzyuL75i04OOtEGW6MO5TymKMwTZlU9r4ukuwxty+T9Ot2LqlNRnLSPQUjb0vplasZ8Ix45JOpRbuSvPovryn7rvS7//klu9hIkFAAQ/AZfGTw+696EjFBg4F5tN6MGMA6KrTQVLXeuYcZeRXwE5t5lwAAAIEAl2xYh098bozJUANQ82DiZznjHc5FW76Xm1apEqsZtVRFuh3V9nc7QNcBekhmHp5Z0sHthXCm1XqnFbkRCdFlX02NpgtNs7OcKpaJP47N8C+C/Yrf8qK/Wt3fExrL2ZLX5XD2tiotugSkwZJMW5Bv0mtjrNt0Q7P45rZjNNTag2c= user@host"}}
        $arr = self::post("ssh_keys/new", array(
                    "name" => $nome,
                    "ssh_pub_key" => $chave
                ));
        if ($arr) {
            return $arr->ssh_key->id;
        }
        return false;
    }

    static public function remover($id) {
        // https://api.digitalocean.com/ssh_keys/[SSH_KEY_ID]/destroy/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK"}
        $arr = self::get("ssh_keys/{$id}/destroy");
        if ($arr) {
            return true;
        }
        return false;
    }

}
