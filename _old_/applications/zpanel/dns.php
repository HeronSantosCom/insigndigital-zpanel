<?php

class zpanel_dns extends api_zpanel {

    static public function pegar($dominio) {
        $arr = self::PostXMWS("dns_manager", "GetRecordsbyDomainName", $dominio);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function listar($dominio_id) {
        $arr = self::PostXMWS("dns_manager", "GetRecordsbyDomainID", $dominio_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function criar($dominio_id, $usuario_id) {
        $arr = self::PostXMWS("dns_manager", "CreateDefaultRecords", "<userid>{$usuario_id}</userid><domainid>{$dominio_id}</domainid>");
        if (isset($arr["created"]) and $arr['created'] == "true") {
            return $arr['created'];
        }
        return false;
    }

    static public function cadastrar($tipo, $valor, $ttl, $alvo, $prioridade, $peso, $porta, $dominio_id, $usuario_id) {
        $arr = self::PostXMWS("dns_manager", "CreateRecord", "<userid>{$usuario_id}</userid><domainid>{$dominio_id}</domainid><type>{$tipo}</type><value>{$valor}</value><ttl>{$ttl}</ttl><target>{$alvo}</target><priority>{$prioridade}</priority><weight>{$peso}</weight><port>{$porta}</port>");
        if (isset($arr["id"]) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function atualizar($id, $tipo, $valor, $ttl, $alvo, $prioridade, $peso, $porta) {
        $arr = self::PostXMWS("dns_manager", "UpdateRecord", "<recordid>{$id}</recordid><type>{$tipo}</type><value>{$valor}</value><ttl>{$ttl}</ttl><target>{$alvo}</target><priority>{$prioridade}</priority><weight>{$peso}</weight><port>{$porta}</port>");
        if (isset($arr["updated"]) and $arr['updated'] == "true") {
            return $arr['updated'];
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("dns_manager", "DeleteRecord", "<recordid>{$id}</recordid>");
        if (isset($arr["deleted"]) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

    static public function status($dominio_id) {
        $arr = self::PostXMWS("dns_manager", "CheckZoneStatus", "<domainid>{$dominio_id}</domainid>");
        if (isset($arr["status"]) and $arr['status'] != "false") {
            return unserialize($arr['status']);
        }
        return false;
    }

}
