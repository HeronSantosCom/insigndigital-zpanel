<?php

if (!class_exists("digitalocean_servidor", false)) {
    include path::applications("digitalocean/servidor.php");
}

class dao_cloud_servidor {

    public static function pegar($id, $hostname = false, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("cloud_servidor");
        $db->column("*");
        if ($id) {
            $db->match("id", $id);
        }
        if ($hostname) {
            $db->match("hostname", $hostname);
        }
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $digitalocean_dao = digitalocean_servidor::pegar($dao[0]["digitalocean_id"]);
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("cloud_servidor");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("hostname");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $digitalocean_dao = digitalocean_servidor::pegar($row["digitalocean_id"]);
                $array[$row["id"]] = self::hook($row, $digitalocean_dao);
            }
        }
        return $array;
    }

    private static function hook($row, $digitalocean) {
        if ($digitalocean) {
            foreach ($digitalocean as $key => $value) {
                $row["digitalocean_{$key}"] = $value;
            }
        }
        return $row;
    }

    public static function cadastrar($hostname, $dominio_id, $cloud_imagem_id, $cloud_regiao_id, $cloud_plano_id, $usuario_id) {
        $dominio_hostname = dao_dominio::hostname($dominio_id);
        if ($dominio_hostname) {
            $hostname = str_replace(array("-", "__", "___"), "_", knife::remove_deadkeys(strtolower($hostname), true)) . ".{$dominio_hostname}";
            if (!self::verificar($hostname, $usuario_id)) {
//                $gid = substr(sha1(uniqid($hostname . mt_rand(), true)), 0, 6);
                $gid = knife::generate_uid($hostname, 6);
                $plano_digitalocean_id = dao_cloud_plano::digitalocean_id($cloud_plano_id);
                $imagem_digitalocean_id = dao_cloud_imagem::digitalocean_id($cloud_imagem_id);
                $regiao_digitalocean_id = dao_cloud_regiao::digitalocean_id($cloud_regiao_id);
                if ($plano_digitalocean_id && $imagem_digitalocean_id && $regiao_digitalocean_id) {
                    $digitalocean = digitalocean_servidor::cadastrar($hostname, $plano_digitalocean_id, $imagem_digitalocean_id, $regiao_digitalocean_id);
                    if ($digitalocean) {
                        $digitalocean_id = $digitalocean->id;
                        $cloud_evento_digitalocean_id = $digitalocean->event_id;
                        $db = new mysqlsave();
                        $db->table("cloud_servidor");
                        $db->column("gid", $gid);
                        $db->column("hostname", $hostname);
                        $db->column("backup", '0');
                        $db->column("dominio_id", $dominio_id);
                        $db->column("cloud_imagem_id", $cloud_imagem_id);
                        $db->column("cloud_regiao_id", $cloud_regiao_id);
                        $db->column("cloud_plano_id", $cloud_plano_id);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("digitalocean_id", $digitalocean_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            $id = $db->id();
                            dao_cloud_evento::cadastrar("Criou novo Servidor Cloud", $id, $usuario_id, $cloud_evento_digitalocean_id);
                            return $id;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function habilitar_backup($id, $usuario_id) {
        $digitalocean_id = self::digitalocean_id($id);
        if ($digitalocean_id) {
            $digitalocean_evento_id = digitalocean_servidor::habilitar_backup($digitalocean_id);
            if ($digitalocean_evento_id) {
                $db = new mysqlsave();
                $db->table("cloud_servidor");
                $db->column("backup", '1');
                $db->match("id", $id);
                if ($db->go()) {
                    dao_cloud_evento::cadastrar("Habilitou o Backup Automático do Servidor Cloud", $id, $usuario_id, $digitalocean_evento_id);
                    return true;
                }
                return false;
            }
        }
    }

    public static function desabilitar_backup($id, $usuario_id) {
        $digitalocean_id = self::digitalocean_id($id);
        if ($digitalocean_id) {
            $digitalocean_evento_id = digitalocean_servidor::desabilitar_backup($digitalocean_id);
            if ($digitalocean_evento_id) {
                $db = new mysqlsave();
                $db->table("cloud_servidor");
                $db->column("backup", '0');
                $db->match("id", $id);
                if ($db->go()) {
                    dao_cloud_evento::cadastrar("Desabilitou o Backup Automático do Servidor Cloud", $id, $usuario_id, $digitalocean_evento_id);
                    return true;
                }
                return false;
            }
        }
    }

    public static function redefinir_senha($id, $usuario_id) {
        $digitalocean_id = self::digitalocean_id($id);
        if ($digitalocean_id) {
            $digitalocean_evento_id = digitalocean_servidor::redefinir_senha($digitalocean_id);
            if ($digitalocean_evento_id) {
                $db = new mysqlsave();
                $db->table("cloud_servidor");
                $db->column("aguardando_senha", '1');
                $db->match("id", $id);
                if ($db->go()) {
                    dao_cloud_evento::cadastrar("Redefiniu a Senha do Servidor Cloud", $id, $usuario_id, $digitalocean_evento_id);
                    return true;
                }
                return false;
            }
        }
    }

    public static function redefinir_plano($id, $cloud_plano_id, $usuario_id) {
        $digitalocean_id = self::digitalocean_id($id);
        $plano_digitalocean_id = dao_cloud_plano::digitalocean_id($cloud_plano_id);
        if ($digitalocean_id && $plano_digitalocean_id) {
            $digitalocean_evento_id = digitalocean_servidor::redefinir_plano($id, $plano_digitalocean_id);
            if ($digitalocean_evento_id) {
                $db = new mysqlsave();
                $db->table("cloud_servidor");
                $db->column("cloud_plano_id", $cloud_plano_id);
                $db->match("id", $id);
                if ($db->go()) {
                    dao_cloud_evento::cadastrar("Alterou o Plano do Servidor Cloud", $id, $usuario_id, $digitalocean_evento_id);
                    return true;
                }
                return false;
            }
        }
    }

    public static function marcar_configurado($id, $ip, $senha) {
        $db = new mysqlsave();
        $db->table("cloud_servidor");
        $db->column("ip", $ip);
        $db->column("senha", $senha);
        $db->column("configurado", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function remover($id, $usuario_id) {
        $digitalocean_id = self::digitalocean_id($id);
        if ($digitalocean_id) {
            $digitalocean_evento_id = digitalocean_servidor::remover($digitalocean_id);
            if ($digitalocean_evento_id) {
                $db = new mysqlsave();
                $db->table("cloud_servidor");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    dao_cloud_evento::cadastrar("Removeu Servidor Cloud", $id, $usuario_id, $digitalocean_evento_id);
                    return true;
                }
                return false;
            }
        }
    }

    public static function verificar($hostname, $usuario_id) {
        $db = new mysqlsearch();
        $db->table("cloud_servidor");
        $db->column("id");
        $db->match("hostname", $hostname);
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function digitalocean_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["digitalocean_id"]) ? $dao["digitalocean_id"] : false);
    }

}