<?php

class layout_listas_email extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->extract(dao_modulo::layout_uso("lista_email", logon::meu_id()), "uso");
        $this->listas_email = dao_lista_email::listar(logon::meu_id());
        $this->dominios = dao_dominio::listar(logon::meu_id());
    }

    private function abrir($id) {
        $dao = (dao_lista_email::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o grupo de e-mail, verifique os dados e tente novamente!");
            $action = dao_lista_email::cadastrar($this->endereco, $this->dominio_id, logon::meu_id());
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Grupo de e-mail salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o grupo de e-mail, tente novamente!");
        if ($id) {
            $action = dao_lista_email::remover($id);
            if ($action) {
                $this->msgbox("Grupo de e-mail removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}