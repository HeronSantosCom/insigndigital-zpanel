<?php

if (!class_exists("zpanel_mysql_usuario_associacao", false)) {
    include path::applications("zpanel/mysql_usuario_associacao.php");
}

class dao_mysql_usuario_associacao {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("mysql_usuario_associacao");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $mysql = dao_mysql::pegar($dao[0]["mysql_id"]);
            $mysql_usuario = dao_mysql_usuario::pegar($dao[0]["mysql_usuario_id"]);
            return self::hook($dao[0], $mysql, $mysql_usuario);
        }
        return false;
    }

    public static function listar($usuario_id, $mysql_usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("mysql_usuario_associacao");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->match("mysql_usuario_id", $mysql_usuario_id);
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $dao_mysql = dao_mysql::listar($usuario_id);
            $dao_mysql_usuario = dao_mysql_usuario::listar($usuario_id);
            foreach ($dao as $row) {
                $mysql = (!empty($dao_mysql[$row["mysql_id"]]) ? $dao_mysql[$row["mysql_id"]] : false);
                $mysql_usuario = (!empty($dao_mysql_usuario[$row["mysql_usuario_id"]]) ? $dao_mysql_usuario[$row["mysql_usuario_id"]] : false);
                $array[$row["id"]] = self::hook($row, $mysql, $mysql_usuario);
            }
        }
        return $array;
    }

    private static function hook($row, $mysql, $mysql_usuario) {
        $row["mysql_nome"] = (!empty($mysql["nome"]) ? $mysql["nome"] : false);
        $row["mysql_usuario_usuario"] = (!empty($mysql_usuario["usuario"]) ? $mysql_usuario["usuario"] : false);
        return $row;
    }

    public static function cadastrar($mysql_id, $mysql_usuario_id, $usuario_id) {
        $mysql_zpanel_id = dao_mysql::zpanel_id($mysql_id);
        $mysql_usuario_zpanel_id = dao_mysql_usuario::zpanel_id($mysql_usuario_id);
        $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
        if ($mysql_zpanel_id && $mysql_usuario_zpanel_id && $usuario_zpanel_id) {
            $zpanel_id = zpanel_mysql_usuario_associacao::cadastrar($mysql_usuario_zpanel_id, $mysql_zpanel_id, $usuario_zpanel_id);
            if ($zpanel_id) {
                $db = new mysqlsave();
                $db->table("mysql_usuario_associacao");
                $db->column("mysql_id", $mysql_id);
                $db->column("mysql_usuario_id", $mysql_usuario_id);
                $db->column("usuario_id", $usuario_id);
                $db->column("zpanel_id", $zpanel_id);
                $db->column("cadastrado", date("Y-m-d H:i:s"));
                $db->column("atualizado", date("Y-m-d H:i:s"));
                if ($db->go()) {
                    return $db->id();
                }
            }
        }
        return false;
    }

    public static function remover($id, $mysql_usuario_id) {
        $zpanel_id = self::zpanel_id($id);
        $mysql_usuario_zpanel_id = dao_mysql_usuario::zpanel_id($mysql_usuario_id);
        if ($zpanel_id && $mysql_usuario_zpanel_id) {
            if (zpanel_mysql_usuario_associacao::remover($zpanel_id, $mysql_usuario_zpanel_id)) {
                $db = new mysqlsave();
                $db->table("mysql_usuario_associacao");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}