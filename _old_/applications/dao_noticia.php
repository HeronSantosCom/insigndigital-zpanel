<?php

class dao_noticia {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("noticia");
        $db->join("usuario", array("usuario_id", "=", "id"), "LEFT");
        $db->column("*");
        $db->column("nome", 2, "usuario_nome");
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->limit(1);
            $db->order("cadastrado", "DESC");
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("noticia");
        $db->join("usuario", array("usuario_id", "=", "id"), "LEFT");
        $db->column("*");
        $db->column("nome", 2, "usuario_nome");
        $db->is("removido", false);
        $db->limit(20);
        $db->order("cadastrado", "DESC");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["atualizado_br"] = date("d/m/Y", strtotime($row["atualizado"]));
        $row["mensagem"] = nl2br($row["mensagem"]);
        return $row;
    }

}