<?php

header("Content-type: text/html; charset=utf8");
define("ip", "169.254.2.1");

function SMS() {
    $pid = getmypid();
    if (mysql_query("UPDATE spool_sms SET status = '2', pid = '{$pid}' WHERE status = '1' ORDER BY id ASC LIMIT 20;", $_ENV["mysql"])) {
        $sql = mysql_query("SELECT * FROM spool_sms WHERE status = '2' and pid = '{$pid}' ORDER BY id ASC LIMIT 20;", $_ENV["mysql"]);
        if ($sql) {
            if (mysql_num_rows($sql) > 0) {
                Msg(__LINE__, "Iniciando envio das mensagens em spool...");
                print "\n";
                while ($row = mysql_fetch_array($sql, MYSQL_ASSOC)) {
                    print ">> {$row["numero"]}: {$row["mensagem"]}";
                    $status = (EnviaSMS($row["numero"], $row["mensagem"]) ? '3' : '4');
                    print " - " . ($status == '3' ? "Ok" : "Erro") . "!\n";
                    mysql_query("UPDATE spool_sms SET status = '{$status}', atualizacao = '" . date("Y-m-d H:i:s") . "' WHERE id = '{$row["id"]}'", $_ENV["mysql"]);
                }
                print "\n";
                Msg(__LINE__, "Processamento concluido!");
            }
            Aguardar(__LINE__, "Reiniciando", 300);
            return WRANull();
        } else
            Error(__LINE__, "Problemas ao selecionar as mensagens no spool ({$pid}).");
    } else
        Error(__LINE__, "Não foi possível criar spool ({$pid}) de sms para ser enviado.");

    return WRANull(true);
}

function EnviaSMS($numero, $mensagem, $steps = 0) {
    if ($steps < 3) {
        curl_setopt($_ENV["ch"], CURLOPT_URL, 'http://' . ip . '/messaging/?page=newmessage');
        curl_setopt($_ENV["ch"], CURLOPT_POST, 1);
        curl_setopt($_ENV["ch"], CURLOPT_POSTFIELDS, "recipient={$numero}&recipientName=&smsbody={$mensagem}");
        return (preg_match("/success/", curl_exec($_ENV["ch"])) ? true : EnviaSMS($numero, $mensagem, ++$steps));
    }
    return false;
}

function ConectaMySQL() {
    $_ENV["mysql"] = mysql_connect("insigndigital.com.br", "insigndigital", "ty6ygatav");
    if ($_ENV["mysql"]) {
        if (mysql_select_db('zadmin_insigndigital', $_ENV["mysql"])) {
            if (mysql_client_encoding($_ENV["mysql"]) != "utf8") {
                if (!mysql_set_charset("utf8", $_ENV["mysql"])) {
                    mysql_query("SET NAMES utf8", $_ENV["mysql"]);
                }
            }
            mysql_query("SET SESSION time_zone = 'America/Sao_Paulo'", $_ENV["mysql"]);
            return true;
        } else
            Error(__LINE__, "Não foi possível não foi possível selecionar a base de dados 'zadmin_insigndigital' no insigndigital.com.br (insigndigital@{$_SERVER["SERVER_ADDR"]}).");
    } else
        Error(__LINE__, "Não foi possível estabelecer uma conexão MySQL com insigndigital.com.br (insigndigital@{$_SERVER["SERVER_ADDR"]}).");
    return false;
}

function DesconectaMySQL() {
    return mysql_close($_ENV["mysql"]);
}

function ConectaWRA() {
    $_ENV["ch"] = curl_init();
    curl_setopt($_ENV["ch"], CURLOPT_URL, 'http://' . ip . '/authent/?returnurl=%2Ffs%2F');
    curl_setopt($_ENV["ch"], CURLOPT_POST, 1);
    curl_setopt($_ENV["ch"], CURLOPT_POSTFIELDS, 'user=Admin&pwd=1234');
    curl_setopt($_ENV["ch"], CURLOPT_COOKIEJAR, 'cookie.txt');
    curl_setopt($_ENV["ch"], CURLOPT_RETURNTRANSFER, 1);
    if (curl_exec($_ENV["ch"]) === false) {
        Error(__LINE__, "Não foi possível se conectar ao dispositivo gsm (" . ip . ").");
        return false;
    }
    return true;
}

function DesconectaWRA() {
    return curl_close($_ENV["ch"]);
}

function WRANull($stop = false) {
    if (!$stop) {
        curl_setopt($_ENV["ch"], CURLOPT_URL, 'http://' . ip . '/fs/');
        curl_exec($_ENV["ch"]);
        return SMS();
    }
    return false;
}

function Aguardar($linha, $mensagem, $tempo) {
    Msg($linha, "{$mensagem} em {$tempo} segundos.");
    sleep($tempo);
}

function Error($linha, $mensagem) {
    Msg($linha, "Error: {$mensagem}.");
}

function Msg($linha, $mensagem) {
    print "[" . date("d/m/Y H:i:s") . "] [{$linha}] {$mensagem}\n";
}

Aguardar(__LINE__, "Iniciando", 3);
$x = true;
while ($x) {
    if (ConectaWRA()) {
        if (ConectaMySQL()) {
            SMS();
        } else
            Error(__LINE__, "Sem conexão com a base de dados.");
    } else
        Error(__LINE__, "Sem conexão com dispositivo gsm.");
    DesconectaMySQL();
    DesconectaWRA();
    Aguardar(__LINE__, "Reiniciando SMS Sender Service", 60);
}