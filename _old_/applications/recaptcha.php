<?php

if (!function_exists("_recaptcha_qsencode")) {
    include path::plugins("recaptchalib.php");
}

class recaptcha extends app {

    public static function form() {
        ob_start();
        $publickey = "6LffVtESAAAAAD8PDZxjfCt8RBiT-GKAuu03iq6d"; // you got this from the signup page
        echo recaptcha_get_html($publickey);
        return ob_get_clean();
    }

    public static function validacao() {
        $privatekey = "6LffVtESAAAAAL9f4nAR7Jj3EnN3BqJoHrM8hxd4";
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        return $resp->is_valid;
    }

}

?>