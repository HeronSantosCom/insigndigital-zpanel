<?php

class layout_dominios extends main {

    public function __construct() {
        $this->www = "1";
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->extract(dao_modulo::layout_uso("dominio", logon::meu_id()), "uso");
        $this->dominios = dao_dominio::listar(logon::meu_id());
        $this->subdominios = dao_subdominio::listar(logon::meu_id());
    }

    private function abrir($id) {
        $dao = (dao_dominio::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o domínio, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_dominio::atualizar($id, ($this->mirror ? $this->diretorio : false), $this->www, ($this->redirect ? $this->redirecionar : false), $this->estacionado, logon::meu_id());
            } else {
                $action = dao_dominio::cadastrar($this->dominio, ($this->mirror ? $this->diretorio : false), $this->www, ($this->redirect ? $this->redirecionar : false), $this->estacionado, logon::meu_id());
                if ($action) {
                    snr::uac(logon::meu_nome(), logon::meu_email(), "Novo Domínio: {$this->dominio}");
                }
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Domínio salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o domínio, tente novamente!");
        if ($id) {
            $action = dao_dominio::remover($id);
            if ($action) {
                snr::uac(logon::meu_nome(), logon::meu_email(), "Domínio Removido: {$this->dominio}");
                $this->msgbox("Domínio removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}