<?php

class digitalocean_imagem extends api_digitalocean {

    static public function listar() {
        // https://api.digitalocean.com/images/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","images":[{"id":429,"name":"Real Backup 10242011","distribution":"Ubuntu"},{"id":430,"name":"test233","distribution":"Ubuntu"},{"id":431,"name":"test888","distribution":"Ubuntu"},{"id":442,"name":"tesah22","distribution":"Ubuntu"},{"id":443,"name":"testah33","distribution":"Ubuntu"},{"id":444,"name":"testah44","distribution":"Ubuntu"},{"id":447,"name":"ahtest55","distribution":"Ubuntu"},{"id":448,"name":"ahtest66","distribution":"Ubuntu"},{"id":449,"name":"ahtest77","distribution":"Ubuntu"},{"id":458,"name":"Rails3-1Ruby1-9-2","distribution":"Ubuntu"},{"id":466,"name":"NYTD Backup 1-18-2012","distribution":"Ubuntu"},{"id":478,"name":"NLP Final","distribution":"Ubuntu"},{"id":540,"name":"API - Final","distribution":"Ubuntu"},{"id":577,"name":"test1-1","distribution":"Ubuntu"},{"id":578,"name":"alec snapshot1","distribution":"Ubuntu"}]}
        /*
         * filter=my_images
          filter= global
         */
        $arr = self::get("images");
        if ($arr) {
            foreach ($arr->images as $row) {
                $retorno[$row->id] = get_object_vars($row);
            }
            return $retorno;
        }
        return false;
    }

    static public function pegar($id) {
        // https://api.digitalocean.com/images/[IMAGE_ID]/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","image":{"id":429,"name":"Real Backup 10242011","distribution":"Ubuntu"}}
        $arr = self::get("images/{$id}");
        if ($arr) {
            return get_object_vars($arr->image->$arr);
        }
        return false;
    }

    static public function cadastrar($nome, $servidor_id) {
        // https://api.digitalocean.com/droplets/[DROPLET_ID]/snapshot/?name=[SNAPSHOT_NAME]&client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK","event_id":7504}
        $arr = self::post("droplets/{$servidor_id}/snapshot", array(
                    "name" => $nome
                ));
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

    static public function remover($id) {
        // https://api.digitalocean.com/images/[IMAGE_ID]/destroy/?client_id=[YOUR_CLIENT_ID]&api_key=[YOUR_API_KEY]
        // {"status":"OK"}
        $arr = self::get("images/{$id}/destroy");
        if ($arr) {
            return $arr->event_id;
        }
        return false;
    }

}
