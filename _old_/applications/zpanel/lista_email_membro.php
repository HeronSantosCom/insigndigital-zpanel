<?php

class zpanel_lista_email_membro extends api_zpanel {

    static public function listar($lista_email_id) {
        $arr = self::PostXMWS("distlists", "GetListDistUsers", $lista_email_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($endereco, $lista_email_id) {
        $endereco = explode("@", $endereco);
        if (!empty($endereco[1])) {
            $arr = self::PostXMWS("distlists", "AddDistListUser", "<distlistid>{$lista_email_id}</distlistid><address>{$endereco[0]}</address><domain>{$endereco[1]}</domain><dladdress></dladdress>");
            if (isset($arr['id']) and $arr['id'] != "false") {
                return $arr['id'];
            }
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("distlists", "DeleteDistListUser", "<distlistusersid>{$id}</distlistusersid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
