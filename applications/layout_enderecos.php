<?php

class layout_enderecos extends main {

    public function __construct() {
        if (!empty($_GET["cep"])) {
            $this->xhr_cep($_GET["cep"]);
        } else {
            if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
                $this->formulario = true;
                if (!empty($_GET["id"])) {
                    if (!$this->abrir($_GET["id"])) {
                        define("app_layout_error", true);
                        return false;
                    }
                }
                if (isset($_GET["remover"])) {
                    $this->remover = true;
                }
                if (isset($_POST["id"])) {
                    $this->salvar($_POST["id"]);
                }
            }
            $this->enderecos = dao_endereco::listar(logon::meu_id());
            $this->tipo_endereco = dao_tipo_endereco::listar();
        }
    }

    private function xhr_cep($cep) {
        $endereco = api_google::endereco_by_cep($cep);
        print json_encode(array('retorno' => $endereco));
    }

    private function abrir($id) {
        $dao = (dao_endereco::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o endereço, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_endereco::atualizar($id, logon::meu_nome(), $this->local, $this->cep, $this->logradouro, $this->numero, $this->complemento, $this->bairro, $this->cidade, $this->estado, $this->pais, $this->referencia, $this->tipo_endereco_id);
            } else {
                $action = dao_endereco::cadastrar(logon::meu_nome(), $this->local, $this->cep, $this->logradouro, $this->numero, $this->complemento, $this->bairro, $this->cidade, $this->estado, $this->pais, $this->referencia, $this->tipo_endereco_id, logon::meu_id());
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Endereço salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o endereço, tente novamente!");
        if ($id) {
            $action = dao_endereco::remover($id);
            if ($action) {
                $this->msgbox("Endereço removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}