<?php
require_once("WSBillingAction.php");
// Inicia a classe WSBillingStatus
$WSBillingAction = new WSBillingAction();

// Cria o cabe�alho SOAP
$xmlObj = $WSBillingAction->add_node("","soap-env:Envelope");
$WSBillingAction->add_attributes($xmlObj, array("xmlns:soap-env" => "http://schemas.xmlsoap.org/soap/envelope/") );
$xmlObj = $WSBillingAction->add_node($xmlObj,"soap-env:Body");

// Cria  o elemento m:F2bCobranca
$xmlObjF2bCobranca = $WSBillingAction->add_node($xmlObj,"m:F2bAcaoCobranca");
$WSBillingAction->add_attributes($xmlObjF2bCobranca, array("xmlns:m" => "http://www.f2b.com.br/soap/wsbillingaction.xsd") );

// Cria o elemento mensagem
$xmlObj = $WSBillingAction->add_node($xmlObjF2bCobranca,"mensagem");
$WSBillingAction->add_attributes($xmlObj, array("data" => date("Y-m-d"),
                                          "numero" => date("His")));

// Cria o elemento cliente
$xmlObj = $WSBillingAction->add_node($xmlObjF2bCobranca,"cliente");
$WSBillingAction->add_attributes($xmlObj, array("conta" => "9023010001230123",
                                          "senha" => "senha123"));

// Cria o elemento cobranca
$xmlObjCobranca = $WSBillingAction->add_node($xmlObjF2bCobranca,"acao_cobranca");

// Deve ser enviado

$WSBillingAction->add_attributes($xmlObjCobranca, array(
// ********************** N�mero da cobran�a ************************************
							"numero" => "153079",
							"cancelar_cobranca" => "0",
// ***********************************************************************************

// ou ---------------

// ********************** A��o (Registrar Pagamento) *******************************
//							"registrar_pagamento" => "0",
//							"registrar_pagamento_valor" => "",
//							"dt_registrar_pagamento" => "",
// ***********************************************************************************

// ou ---------------

// ********************** A��o (Cancelar Multa) *******************************
//							"cancelar_multa" => "1",
// ***********************************************************************************

// e/ou ---------------(para a a��o cancelar multa) ------------------ou (para as demais a��es)

// ********************** A��o (Registrar Pagamento) *******************************
//							"permitir_pagamento" => "1",
//							"dt_permitir_pagamento" => "2005-05-20",
// ***********************************************************************************

// e/ou ---------------(para a a��o reenviar email) ------------------

// ********************** A��o (Reenviar Email) *******************************
//							"reenviar_email" => "0",
//							"email_tosend" => "",
// ***********************************************************************************
));
// envia dados
$WSBillingAction->send($WSBillingAction->getXML());
$resposta = $WSBillingAction->resposta;
if(strlen($resposta) > 0){
	// Reinicia a classe WSBillingAction, agora com uma string XML
	$WSBillingAction = new WSBillingAction($resposta);

	// LOG 
	$log = $WSBillingAction->pegaLog();
	echo "<html><head><title>WSBillingAction</title></head><body>";
	if($log["texto"] == "OK"){
		// CLIENTE
		$cliente = $WSBillingAction->pegaCliente();
		// COBRANCAS
		$cobranca = $WSBillingAction->pegaCobranca();

		echo "<table border=1><tr><td colspan='2' bgcolor='gray'><b>Log</b></td></tr>";
		foreach($log as $key => $value){
			echo '<tr><td>$log["'.$key.'"]</td><td>'.$value.'</td></tr>';
		}
		echo "<tr><td colspan='2' bgcolor='gray'><b>Cliente</b></td></tr>";
		foreach($cliente as $key => $value){
			foreach($cliente[$key] as $key2 => $value2){
				echo '<tr><td>$cliente['.$key.']["'.$key2.'"]</td><td>'.$value2.'</td></tr>';
			}
		}
		echo "<tr><td colspan='2' bgcolor='gray'><b>A��o Cobran�a</b></td></tr>";
		foreach($cobranca as $key => $value){
			foreach($cobranca[$key] as $key2 => $value2){
				echo '<tr><td>$cobranca['.$key.']["'.$key2.'"]</td><td>'.$value2.'</td></tr>';
			}
		}

		echo "</table>";
	} else {
		echo "<table border=1><tr><td colspan='2' bgcolor='gray'><b>Log</b></td></tr>";
		foreach($log as $key => $value){
			echo '<tr><td>$log["'.$key.'"]</td><td><font color="red">'.$value.'</font></td></tr>';
		}
		echo "</table>";
	}
	echo "</body></html>";
} else {
	echo '<font color="red">Sem resposta</font>';
}
?>
