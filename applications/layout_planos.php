<?php

class layout_planos extends main {

    public function __construct() {

        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            } else {
                $this->plano_valor();
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->planos = dao_plano::listar(logon::meu_id());
    }

    private function plano_valor() {
        $dao = dao_plano_valor::listar();
        if ($dao) {
            foreach ($dao as $plv) {
                $tipo = $plv["tipo"];
                $row["plv_{$tipo}_valor"] = $plv["valor"];
                $row["plv_{$tipo}_valor_br"] = $plv["valor_br"];
                $row["plv_{$tipo}_total"] = 0.00000;
                $row["plv_{$tipo}_total_br"] = "0,00";
                $row["plv_{$tipo}_reajustado"] = false;
            }
            $this->extract($row);
            return true;
        }
        return false;
    }

    private function calcula_plano_valor() {
        $valor = 0.00;
        $dao = dao_plano_valor::listar();
        if ($dao) {
            foreach ($dao as $plv) {
                $tipo = $plv["tipo"];
                if (isset($this->{"total_{$tipo}"})) {
                    $valor += ($plv["valor"] * $this->{"total_{$tipo}"});
                }
            }
        }
        return $valor;
    }

    private function abrir($id) {
        $dao = dao_plano::pegar($id, logon::meu_id());
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            //$this->msgbox("O valor total está inferior ao limite determinado (R$ 7,00), verifique sua combinação e tente novamente!");
            $valor = $this->calcula_plano_valor();
            //if ($valor > 7.00) {
                $valor = number_format($valor, 2, '.', '');
                $this->msgbox("Não foi possível salvar o plano, verifique os dados e tente novamente!");
                if ($id) {
                    $action = dao_plano::atualizar($id, $this->nome, $this->habilitar_php, $this->habilitar_cgi, $this->total_dominio, $this->total_subdominio, $this->total_email, $this->total_redirecionamento_email, $this->total_lista_email, $this->total_ftp, $this->total_mysql, $this->total_quota_disco, $this->total_quota_transferencia, $valor, logon::meu_id());
                } else {
                    $action = dao_plano::cadastrar($this->nome, $this->habilitar_php, $this->habilitar_cgi, $this->total_dominio, $this->total_subdominio, $this->total_email, $this->total_redirecionamento_email, $this->total_lista_email, $this->total_ftp, $this->total_mysql, $this->total_quota_disco, $this->total_quota_transferencia, $valor, logon::meu_id());
                    if ($action) {
                        snr::uac(logon::meu_nome(), logon::meu_email(), "Novo Plano: {$this->nome}");
                    }
                }
                if ($action) {
                    unset($this->formulario);
                    $this->msgbox("Plano salvo com sucesso!");
                }
            //}
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o plano, tente novamente!");
        if ($id and $this->plano_id) {
            $plano = dao_plano::pegar($this->plano_id, logon::meu_id());
            if ($plano) {
                $action = dao_plano::remover($id, $this->plano_id);
                if ($action) {
                    snr::uac(logon::meu_nome(), logon::meu_email(), "Plano Removido: {$this->nome} >> {$plano["nome"]}");
                    $this->msgbox("Plano removido com sucesso, os clientes atribuidos a este plano foram movidos para o plano {$plano["nome"]}!");
                    unset($this->formulario);
                }
            }
        }
    }

}