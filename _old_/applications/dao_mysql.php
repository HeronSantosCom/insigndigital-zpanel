<?php

if (!class_exists("zpanel_mysql", false)) {
    include path::applications("zpanel/mysql.php");
}

class dao_mysql {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("mysql");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $zpanel = zpanel_mysql::pegar($dao[0]["zpanel_id"]);
            return self::hook($dao[0], $zpanel);
        }
        return false;
    }

    public static function listar($usuario_id, $mysql_usuario = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("mysql");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        if ($mysql_usuario) {
            $db->in("id", "(SELECT mysql_id FROM mysql_usuario_associacao WHERE mysql_usuario_id = {$mysql_usuario} AND removido IS NULL)", false, true);
        }
        $db->is("removido", false);
        $db->order("nome");
        $dao = $db->go();
        if ($dao) {
            $zpanel_dao = zpanel_mysql::listar(dao_usuario::zpanel_id($usuario_id));
            foreach ($dao as $row) {
                $zpanel = (!empty($zpanel_dao[$row["zpanel_id"]]) ? $zpanel_dao[$row["zpanel_id"]] : false);
                $array[$row["id"]] = self::hook($row, $zpanel);
            }
        }
        return $array;
    }

    private static function hook($row, $zpanel) {
        $row["total_usuarios"] = (!empty($zpanel["totaldb"]) ? ($zpanel["totaldb"] ? $zpanel["totaldb"] : '0') : '0');
        $row["tamanho"] = (!empty($zpanel["mysqlsize"]) ? ($zpanel["mysqlsize"] ? $zpanel["mysqlsize"] : '0') : '0');
        $row["tamanho_legivel"] = knife::human_size_disk($row["tamanho"]);
        return $row;
    }

    public static function cadastrar($nome, $usuario_id) {
        if (dao_modulo::zpanel_disponivel("mysql", $usuario_id)) {
            $usuario_zpanel_usuario = dao_usuario::zpanel_usuario($usuario_id);
            $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
            if ($usuario_zpanel_usuario && $usuario_zpanel_id) {
                $nome_completo = "{$usuario_zpanel_usuario}_{$nome}";
                if (!self::verificar($nome_completo)) {
                    $zpanel_id = zpanel_mysql::cadastrar($nome, $usuario_zpanel_id);
                    if ($zpanel_id) {
                        $db = new mysqlsave();
                        $db->table("mysql");
                        $db->column("nome", $nome_completo);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            return $db->id();
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_mysql::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("mysql");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    self::remover_relacionados($id);
                    return true;
                }
            }
        }
        return false;
    }

    private static function remover_relacionados($id) {
        // remove associacao com usuarios do banco de dados
        $db = new mysqlsave();
        $db->table("mysql_usuario_associacao");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("mysql_id", $id);
        $db->go();
    }

    public static function verificar($nome_completo) {
        $db = new mysqlsearch();
        $db->table("mysql");
        $db->column("id");
        $db->match("nome", $nome_completo);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}