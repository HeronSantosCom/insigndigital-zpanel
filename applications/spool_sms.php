<?php

/**
 * Spool de envio de SMS
 */
class spool_sms {

    /**
     * Envia para o Cliente
     * @param string $celular
     * @param string $mensagem
     * @return boolean
     */
    public static function epc($celular, $mensagem) {
        $celular = trim(str_replace(array("(", ")", "-", "+", "_", " "), "", $celular));
        if ($celular) {
            $mensagem = "[Insign Digital] {$mensagem} " . date("d/m \à\s H:i");
            $db = new mysqlsave();
            $db->table("spool_sms");
            $db->column("numero", $celular);
            $db->column("mensagem", $mensagem);
            return $db->go();
        }
        return true;
    }

    /**
     * Notifica Cadastrado de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function uc($telefone) {
        return self::epc($telefone, "Seja bem-vindo. Sua credencial de acesso foram enviadas para o seu e-mail.");
    }

    /**
     * Notifica Atualização de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function ua($telefone) {
        return self::epc($telefone, "Suas conta foi atualizada, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * Notifica Administração de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function au($telefone) {
        return self::epc($telefone, "Sua conta está sendo administrada pelo seu gestor, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * CRP: Notificação de Crédito de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function crpuc($telefone) {
        return self::epc($telefone, "Seu pagamento foi registrado e creditado em sua conta, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * CRP: Notificação de Crédito de Cliente de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function crpcc($telefone) {
        return self::epc($telefone, "Sua comissão foi registrado e creditado em sua conta, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * Notifica Remoção de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function ur($telefone) {
        return self::epc($telefone, "Suas conta foi removida, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * Notifica Redefição de Senha do Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function usr($telefone) {
        return self::epc($telefone, "Sua senha foi redefinida e enviada para o seu e-mail.");
    }

    /**
     * Notifica Assinatura de Plano do Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function uap($telefone) {
        return self::epc($telefone, "Sua assinatura foi efetuada com sucesso, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * Notificação de Bloqueio de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function ub($telefone) {
        return self::epc($telefone, "Suas conta foi bloqueada, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * Notificação de Desbloqueio de Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function ud($telefone) {
        return self::epc($telefone, "Suas conta foi desbloqueada, maiores informações foram enviadas para o seu e-mail.");
    }

    /**
     * Notificação de Novo Atendimento de Suporte do Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function sc($telefone) {
        return self::epc($telefone, "Seu pedido atendimento foi registrado com sucesso, maiores informações acesse o painel do cliente.");
    }

    /**
     * Notificação de Nova Resposta de Atendimento do Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function scr($telefone) {
        return self::epc($telefone, "Sua resposta foi registrada com sucesso, maiores informações acesse o painel do cliente.");
    }

    /**
     * Notificação de Encerramento de Atendimento do Usuário
     * @param string $telefone
     * @return boolean
     */
    public static function sr($telefone) {
        return self::epc($telefone, "Seu atendimento foi encerrado, maiores informações acesse o painel do cliente.");
    }

    /**
     * Notificação de Nova Resposta de Atendimento do Gestor
     * @param string $telefone
     * @return boolean
     */
    public static function scrg($telefone) {
        return self::epc($telefone, "Seu atendimento foi respondido, maiores informações acesse o painel do cliente.");
    }

    /**
     * Notificação de Encerramento de Atendimento do Gestor
     * @param string $telefone
     * @return boolean
     */
    public static function srg($telefone) {
        return self::epc($telefone, "Seu atendimento foi solucionado, maiores informações acesse o painel do cliente.");
    }

    /**
     * Notificação de Cadastro de Servidor Cloud
     * @param type $telefone
     * @return type
     */
//    public static function scc($telefone) {
//        return self::epc($telefone, "Seu servidor cloud já está pronto, maiores informações foram enviadas para o seu e-mail.");
//    }

}