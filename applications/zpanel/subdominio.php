<?php

class zpanel_subdominio extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("sub_domains", "GetSubDomainsForUser", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["subid"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($dominio, $diretorio, $redirecionar, $usuario_id) {
        $autohome = '1';
        if ($diretorio) {
            $autohome = '0';
            if (substr($diretorio, 0, 1) == "/") {
                $diretorio = substr($diretorio, 1);
            }
        }
        $arr = self::PostXMWS("sub_domains", "CreateSubDomain", "<uid>{$usuario_id}</uid><domain>{$dominio}</domain><destination>{$diretorio}</destination><autohome>{$autohome}</autohome><redirect>{$redirecionar}</redirect>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function atualizar($id, $dominio, $diretorio, $redirecionar, $usuario_id) {
        $autohome = '1';
        if ($diretorio) {
            $autohome = '0';
            if (substr($diretorio, 0, 1) == "/") {
                $diretorio = substr($diretorio, 1);
            }
        }
        $arr = self::PostXMWS("sub_domains", "UpdateSubDomain", "<uid>{$usuario_id}</uid><domainid>{$id}</domainid><domain>{$dominio}</domain><destination>{$diretorio}</destination><autohome>{$autohome}</autohome><redirect>{$redirecionar}</redirect>");
        if (isset($arr['updated']) and $arr['updated'] == "true") {
            return $arr['updated'];
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("sub_domains", "DeleteSubDomain", "<domainid>{$id}</domainid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}