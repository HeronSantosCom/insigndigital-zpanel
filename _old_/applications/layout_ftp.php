<?php

class layout_ftp extends main {

    public function __construct() {
        $this->autohome = "2";
        $this->tipo_acesso_id = "3";
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->extract(dao_modulo::layout_uso("ftp", logon::meu_id()), "uso");
        $this->ftps = dao_ftp::listar(logon::meu_id());
        $this->dominios = dao_dominio::listar(logon::meu_id());
        $this->subdominios = dao_subdominio::listar(logon::meu_id());
        $this->tipo_acesso = dao_tipo_acesso::listar();
        $this->diretorios = dao_ftp::diretorios(logon::meu_id());
        $this->zpanel_usuario = logon::meu_zpanel_usuario();
    }

    private function abrir($id) {
        $dao = (dao_ftp::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar a conta FTP, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_ftp::atualizar($id, $this->senha);
            } else {
                $action = dao_ftp::cadastrar($this->usuario, $this->senha, ($this->autohome == "2" ? '/' : ($this->autohome == "3" ? $this->diretorio : false)), $this->tipo_acesso_id, logon::meu_id());
            }
            if ($action) {
                snr::uac(logon::meu_nome(), logon::meu_email(), "Nova Conta FTP: {$this->usuario}");
                unset($this->formulario);
                $this->msgbox("Conta FTP salva com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover a conta FTP, tente novamente!");
        if ($id) {
            $action = dao_ftp::remover($id);
            if ($action) {
                snr::uac(logon::meu_nome(), logon::meu_email(), "Conta FTP Removida: {$this->usuario}");
                $this->msgbox("Conta FTP removida com sucesso!");
                unset($this->formulario);
            }
        }
    }

}