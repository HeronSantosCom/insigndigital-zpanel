<?php

if (!class_exists("IMAPMAIL", false)) {
//    knife::dump(path::plugins("imap.php"));
    include path::plugins("imap.php");
}

if (!class_exists("MIMEDECODE", false)) {
//    knife::dump(path::plugins("mimedecode.php"));
    include path::plugins("mimedecode.php");
}

class spool_imap {

    public function __construct() {
        $imap = new IMAPMAIL;
        if ($imap->open("mail.insigndigital.com.br", "143")) {
            $imap->login("", "");
            $imap->open_mailbox("INBOX");
            $total = $imap->get_msglist();
            for ($msg_id = 1; $msg_id <= $total; $msg_id++) {
                $from = false;
                $to = false;
                $subject = false;
                $cabecalho = $imap->get_message_header($msg_id);
                if ($cabecalho) {
                    $cabecalho = explode("\n", $cabecalho);
                    if ($cabecalho) {
                        foreach ($cabecalho as $row) {
                            $row = trim($row);
                            $x = explode(": ", $row, 2);
                            if (!empty($x[1])) {
                                $key = strtolower(trim($x[0]));
                                $$key = trim($x[1]);
                            } else {
                                $x = explode("=", $row, 2);
                                if (!empty($x[1])) {
                                    $key = strtolower(trim($x[0]));
                                    $$key = trim($x[1]);
                                }
                            }
                        }
                    }
                }
                if (!empty($from) && !empty($to) && !empty($subject)) {
                    if (preg_match("/<(.*)>/", $from, $matches)) {
                        if (!empty($matches[1])) {
                            $from = $matches[1];
                        }
                    }
                    if (preg_match("/<(.*)>/", $to, $matches)) {
                        if (!empty($matches[1])) {
                            $to = $matches[1];
                        }
                    }
                    $action = dao_spool_imap::cadastrar($from, $to, $subject, htmlentities($imap->get_message_body($msg_id)));
                    if ($action) {
                        $this->logger(__LINE__, $imap->delete_message($msg_id));
                    }
                }
            }
            $this->logger(__LINE__, $imap->get_error());
            $imap->close();
        } else {
            $this->logger(__LINE__, $imap->get_error());
        }
    }

    private function logger($linha, $mensagem) {
        $mensagem = trim($mensagem);
        if (strlen($mensagem) > 0) {
            print $linha . " - " . $mensagem . "\n";
        }
    }

}