<?php

class zpanel_mysql_usuario extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("mysql_users", "GetDatabasesUsers", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["mysqlid"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($usuario, $senha, $acesso, $mysql_id, $usuario_id) {
        $arr = self::PostXMWS("mysql_users", "CreateMySQLUsers", "<uid>{$usuario_id}</uid><username>{$usuario}</username><database>{$mysql_id}</database><access>{$acesso}</access><password>{$senha}</password>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return unserialize($arr['id']);
        }
        return false;
    }

    static public function atualizar($id, $senha) {
        if ($senha) {
            $arr = self::PostXMWS("mysql_users", "ResetPasswordUsers", "<myuserid>{$id}</myuserid><password>{$senha}</password>");
            if (isset($arr['updated']) and $arr['updated'] == "true") {
                return $arr['updated'];
            }
            return false;
        }
        return true;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("mysql_users", "DeleteMySQLUsers", "<myuserid>{$id}</myuserid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
