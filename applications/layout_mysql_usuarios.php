<?php

class layout_mysql_usuarios extends main {

    public function __construct() {
        $this->acesso_tipo = "1";
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->mysql = dao_mysql::listar(logon::meu_id());
        $this->mysql_usuarios = dao_mysql_usuario::listar(logon::meu_id());
        $this->zpanel_usuario = logon::meu_zpanel_usuario();
    }

    private function abrir($id) {
        $dao = (dao_mysql_usuario::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o usuário do banco de dados, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_mysql_usuario::atualizar($id, $this->senha);
            } else {
                $action = dao_mysql_usuario::cadastrar($this->usuario, $this->senha, ($this->acesso_tipo == "1" ? "%" : $this->acesso), $this->mysql_id, logon::meu_id());
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Usuário do banco de dados salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o usuário do banco de dados, tente novamente!");
        if ($id) {
            $action = dao_mysql_usuario::remover($id);
            if ($action) {
                $this->msgbox("Usuário do banco de dados removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}