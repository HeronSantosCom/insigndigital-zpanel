<?php

class zpanel_email extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("mailboxes", "GetMailboxs", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($endereco, $senha, $usuario_id) {
        $endereco = explode("@", $endereco);
        if (!empty($endereco[1])) {
            $arr = self::PostXMWS("mailboxes", "CreateMailbox", "<uid>{$usuario_id}</uid><address>{$endereco[0]}</address><domain>{$endereco[1]}</domain><password>{$senha}</password>");
            if (isset($arr['id']) and $arr['id'] != "false") {
                return $arr['id'];
            }
        }
        return false;
    }

    static public function atualizar($id, $senha, $habilitado) {
        $arr = self::PostXMWS("mailboxes", "UpdateMailbox", "<mailclientid>{$id}</mailclientid><password>{$senha}</password><enabled>{$habilitado}</enabled>");
        if (isset($arr['updated']) and $arr['updated'] == "true") {
            return $arr['updated'];
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("mailboxes", "DeleteMailbox", "<mailclientid>{$id}</mailclientid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
