<?php

class layout_mysql extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->extract(dao_modulo::layout_uso("mysql", logon::meu_id()), "uso");
        $this->mysql = dao_mysql::listar(logon::meu_id());
        $this->zpanel_usuario = logon::meu_zpanel_usuario();
    }

    private function abrir($id) {
        $dao = (dao_mysql::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o banco de dados, verifique os dados e tente novamente!");
            $action = dao_mysql::cadastrar($this->nome, logon::meu_id());
            if ($action) {
                snr::uac(logon::meu_nome(), logon::meu_email(), "Novo Banco MySQL: {$this->nome}");
                unset($this->formulario);
                $this->msgbox("Banco de dados salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o banco de dados, tente novamente!");
        if ($id) {
            $action = dao_mysql::remover($id);
            if ($action) {
                snr::uac(logon::meu_nome(), logon::meu_email(), "Banco MySQL Removido: {$this->nome}");
                $this->msgbox("Banco de dados removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}