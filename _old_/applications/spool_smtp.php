<?php

/**
 * Spool de envio de email
 */
class spool_smtp {

    /**
     * Envia para o Cliente
     * @param string $email
     * @param string $titulo
     * @param array $mensagem
     * @return boolean
     */
    public static function epc($email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = "Lembre-se, em caso de dúvidas ou sugestões entre em contato conosco através dos nossos canais de atendimento!";
        $mensagem[] = "";
        $mensagem[] = "Insign Digital";
        $mensagem[] = "http://www.insigndigital.com.br/";
        $mensagem[] = "";
        $mensagem[] = "---";
        $mensagem[] = "Notificação gerada e enviada automaticamente.";
        $mensagem[] = date("r") . " / " . getmypid() . (!empty($_SERVER["REMOTE_ADDR"]) ? " / " . $_SERVER["REMOTE_ADDR"] : false );
        $headers = "From: Insign Digital <contato@insigndigital.com.br>\n";
        $headers .= "Reply-To: Insign Digital <contato@insigndigital.com.br>\n";
        return knife::mail_utf8($email, '[Insign Digital] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

    /**
     * Envia para o Gestor
     * @param string $nome
     * @param string $email
     * @param string $titulo
     * @param array $mensagem
     * @return boolean
     */
    public static function epg($nome, $email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = "Insign Digital";
        $mensagem[] = "http://www.insigndigital.com.br/";
        $mensagem[] = "";
        $mensagem[] = "---";
        $mensagem[] = "Notificação gerada e enviada automaticamente.";
        $mensagem[] = date("r") . " / " . getmypid() . (!empty($_SERVER["REMOTE_ADDR"]) ? " / " . $_SERVER["REMOTE_ADDR"] : false );
        $headers = "From: " . $nome . " <" . $email . ">\n";
        $headers .= "Reply-To: " . $nome . " <" . $email . ">\n";
        return knife::mail_utf8("contato@insigndigital.com.br", '[Insign Digital] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

    /**
     * Ação do Usuário
     * @param string $nome
     * @param string $email
     * @param string $acao
     * @return boolean
     */
    public static function uac($nome, $email, $acao) {
        $corpo_gestor[] = "Nome: {$nome}";
        $corpo_gestor[] = "Usuário: {$email}";
        $corpo_gestor[] = "";
        $corpo_gestor[] = $acao;
        return self::epg($nome, $email, "Ação do Usuário", $corpo_gestor);
    }

    /**
     * Notifica Cadastrado de Usuário
     * @param string $nome
     * @param string $email
     * @param string $senha
     * @return boolean
     */
    public static function uc($nome, $email, $senha) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Seja bem-vindo ao Insign Digital!";
        $mensagem[] = "";
        $mensagem[] = "Abaixo segue sua credencial de acesso, por segurança anote em um local seguro.";
        $mensagem[] = "";
        $mensagem[] = "\tSeu usuário é: {$email}";
        $mensagem[] = "\tSua senha é: {$senha}";
        $mensagem[] = "\tEndereço de acesso ao painel: http://painel.insigndigital.com.br/";
        $sucesso = self::epc($email, "Seja bem-vindo", $mensagem);
        if ($sucesso) {
            $corpo_gestor[] = "Nome: {$nome}";
            $corpo_gestor[] = "Usuário: {$email}";
            self::epg($nome, $email, "Novo Usuário", $corpo_gestor);
        }
        return $sucesso;
    }

    /**
     * Notifica Atualização de Usuário
     * @param string $nome
     * @param string $email
     * @param string $senha
     * @param string $motivo
     * @return boolean
     */
    public static function ua($nome, $email, $senha, $motivo) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Seus dados foram atualizados com sucesso!";
        $mensagem[] = "Por segurança, anote em um local seguro.";
        $mensagem[] = "";
        $mensagem[] = "\tSeu usuário é: {$email}";
        if ($senha) {
            $mensagem[] = ($senha ? "\tSua senha é: {$senha}" : "\tSua senha não foi alterada!");
        }
        $mensagem[] = "\tEndereço de acesso: http://painel.insigndigital.com.br/";
        $mensagem[] = "";
        if ($motivo) {
            $mensagem[] = "\tAtenção!";
            $mensagem[] = "\tSua conta encontra-se bloqueada.";
            $mensagem[] = "\tMotivo: {$motivo}";
        }
        return self::epc($email, "Suas informações foram atualizadas", $mensagem);
    }

    /**
     * Notifica Administração de Usuário
     * @param string $nome
     * @param string $email
     * @return boolean
     */
    public static function au($nome, $email) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Sua conta está sendo administrada pelo seu gestor!";
        $mensagem[] = "Calma, isto significa que ela está sendo acessada neste momento por ele para efetuar correções ou prestar ajuda solicitada!";
        $mensagem[] = "";
        $mensagem[] = "\tSeu usuário: {$email}";
        return self::epc($email, "Sua conta está sendo administrada", $mensagem);
    }

    /**
     * Notifica Remoção de Usuário
     * @param string $nome
     * @param string $email
     * @param string $motivo
     * @return boolean
     */
    public static function ur($nome, $email, $motivo) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Sua conta foi removida dos nossos serviços!";
        $mensagem[] = "";
        $mensagem[] = "\tSeu usuário era: {$email}";
        if ($motivo) {
            $mensagem[] = "\tMotivo: {$motivo}";
        }
        $sucesso = self::epc($email, "Precisamos de sua atenção", $mensagem);
        if ($sucesso) {
            $corpo_gestor[] = "Nome: {$nome}";
            $corpo_gestor[] = "Usuário: {$email}";
            $corpo_gestor[] = "";
            $corpo_gestor[] = "Motivo: {$motivo}";
            self::epg($nome, $email, "Usuário Removido", $corpo_gestor);
        }
        return $sucesso;
    }

    /**
     * Notifica Redefição de Senha do Usuário
     * @param string $nome
     * @param string $email
     * @param string $senha
     * @return boolean
     */
    public static function usr($nome, $email, $senha) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Como solicitado, estamos enviando sua nova senha de acesso!";
        $mensagem[] = "Por segurança, anote em um local seguro.";
        $mensagem[] = "";
        $mensagem[] = "\tSua nova senha é: {$senha}";
        $mensagem[] = "\tEndereço de acesso: http://painel.insigndigital.com.br/";
        $mensagem[] = "";
        $mensagem[] = "Esta é uma senha gerada aleatóriamente, por favor, altere!";
        return self::epc($email, "Sua senha foi redefinida", $mensagem);
    }

    /**
     * Notifica Assinatura de Plano do Usuário
     * @param string $nome
     * @param string $email
     * @param string $plano
     * @param string $valor
     * @param string $bonus
     * @return boolean
     */
    public static function uap($nome, $email, $plano, $valor, $bonus) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Sua assinatura foi efetuada com sucesso, obrigado por escolher nossos serviços!";
        $mensagem[] = "";
        $mensagem[] = "\tSeu plano: {$plano}";
        $mensagem[] = "\tValor: R$ {$valor}";
        if ($bonus) {
            $mensagem[] = "\tVocê foi contemplado com 15 dias de bônus!";
        }
        $sucesso = self::epc($email, "Sua assinatura", $mensagem);
        if ($sucesso) {
            $corpo_gestor[] = "Nome: {$nome}";
            $corpo_gestor[] = "Usuário: {$email}";
            $corpo_gestor[] = "";
            $corpo_gestor[] = "Plano: {$plano}";
            $corpo_gestor[] = "Valor: R$ {$valor}";
            self::epg($nome, $email, "Novo Assinatura", $corpo_gestor);
        }
        return $sucesso;
    }

    /**
     * Notificação de Bloqueio de Usuário
     * @param string $nome
     * @param string $email
     * @param string $motivo
     * @return boolean
     */
    public static function ub($nome, $email, $motivo) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Sua conta foi bloqueada!";
        $mensagem[] = "";
        $mensagem[] = "\tMotivo: {$motivo}";
        $sucesso = self::epc($email, "Precisamos de sua atenção!", $mensagem);
        if ($sucesso) {
            $corpo_gestor[] = "Nome: {$nome}";
            $corpo_gestor[] = "Usuário: {$email}";
            self::epg($nome, $email, "Usuário Bloqueado", $corpo_gestor);
        }
        return $sucesso;
    }

    /**
     * Notificação de Desbloqueio de Usuário
     * @param string $nome
     * @param string $email
     * @return boolean
     */
    public static function ud($nome, $email) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Sua conta foi desbloqueada!";
        $sucesso = self::epc($email, "Precisamos de sua atenção", $mensagem);
        if ($sucesso) {
            $corpo_gestor[] = "Nome: {$nome}";
            $corpo_gestor[] = "Usuário: {$email}";
            self::epg($nome, $email, "Usuário Desbloqueado", $corpo_gestor);
        }
        return $sucesso;
    }

    /**
     * CRP: Notificação de Crédito de Usuário
     * @param string $nome
     * @param string $email
     * @param string $descricao
     * @param string $valor
     * @return boolean
     */
    public static function crpuc($nome, $email, $descricao, $valor) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Seu pagamento foi registrado e creditado em sua conta!";
        $mensagem[] = "Acesse o painel do cliente para mais informações.";
        $mensagem[] = "";
        $mensagem[] = "Descrição: {$descricao}";
        $mensagem[] = "Valor: R$ {$valor}";
        return self::epc($email, "Crédito em Conta", $mensagem);
    }
    
    /**
     * CRP: Notificação de Crédito de Cliente de Usuário
     * @param string $nome
     * @param string $email
     * @param string $descricao
     * @param string $valor
     * @return boolean
     */
    public static function crpcc($nome, $email, $descricao, $valor) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Sua comissão referente ao pagamento do seu cliente foi registrado e creditado em sua conta!";
        $mensagem[] = "Acesse o painel do cliente para mais informações.";
        $mensagem[] = "";
        $mensagem[] = "Descrição: {$descricao}";
        $mensagem[] = "Valor: R$ {$valor}";
        return self::epc($email, "Comissão por Crédito do Cliente", $mensagem);
    }

    /**
     * Notificação de Novo Atendimento de Suporte do Usuário
     * @param string $nome
     * @param string $email
     * @param string $descricao
     * @param string $valor
     * @return boolean
     */
    public static function sc($nome, $email, $titulo) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Seu pedido atendimento foi registrado com sucesso!";
        $mensagem[] = "Acesse o painel do cliente para mais informações.";
        $mensagem[] = "";
        $mensagem[] = "\tTítulo: {$titulo}";
        $sucesso = self::epc($email, "Pedido de Atendimento", $mensagem);
        if ($sucesso) {
            $corpo_gestor[] = "Nome: {$nome}";
            $corpo_gestor[] = "Usuário: {$email}";
            $corpo_gestor[] = "";
            $corpo_gestor[] = "Título: {$titulo}";
            self::epg($nome, $email, "Nova Chamado de Atendimento", $corpo_gestor);
        }
        return $sucesso;
    }

    /**
     * Notificação de Nova Resposta de Atendimento do Usuário
     * @param string $nome
     * @param string $email
     * @param string $titulo
     * @return boolean
     */
    public static function scr($nome, $email, $titulo) {
        $corpo_gestor[] = "Nome: {$nome}";
        $corpo_gestor[] = "Usuário: {$email}";
        $corpo_gestor[] = "";
        $corpo_gestor[] = "Título: {$titulo}";
        return self::epg($nome, $email, "Atendimento Respondido", $corpo_gestor);
    }

    /**
     * Notificação de Encerramento de Atendimento do Usuário
     * @param string $nome
     * @param string $email
     * @param string $titulo
     * @return boolean
     */
    public static function sr($nome, $email, $titulo) {
        $corpo_gestor[] = "Nome: {$nome}";
        $corpo_gestor[] = "Usuário: {$email}";
        $corpo_gestor[] = "";
        $corpo_gestor[] = "Título: {$titulo}";
        return self::epg($nome, $email, "Atendimento Encerrado", $corpo_gestor);
    }

    /**
     * Notificação de Nova Resposta de Atendimento do Gestor
     * @param string $nome
     * @param string $email
     * @param string $titulo
     * @return boolean
     */
    public static function scrg($nome, $email, $titulo) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Seu atendimento foi respondido pelo gestor!";
        $mensagem[] = "Acesse o painel do cliente para mais informações.";
        $mensagem[] = "";
        $mensagem[] = "\tTítulo: {$titulo}";
        return self::epc($email, "Seu atendimento foi respondido", $mensagem);
    }

    /**
     * Notificação de Encerramento de Atendimento do Gestor
     * @param string $nome
     * @param string $email
     * @param string $titulo
     * @return boolean
     */
    public static function srg($nome, $email, $titulo) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Seu atendimento foi solucionado pelo gestor!";
        $mensagem[] = "";
        $mensagem[] = "\tTítulo: {$titulo}";
        return self::epc($email, "Seu atendimento foi solucionado", $mensagem);
    }
    
//    public static function scc($nome, $email, $hostname, $ip, $senha) {
//        $mensagem[] = "Olá {$nome},";
//        $mensagem[] = "";
//        $mensagem[] = "Seu Servidor Cloud já está pronto e funcionando!";
//        $mensagem[] = "";
//        $mensagem[] = "Abaixo segue sua credencial de acesso, por segurança anote em um local seguro.";
//        $mensagem[] = "";
//        $mensagem[] = "\tEnderço IP: {$ip} ({$hostname})";
//        $mensagem[] = "\tSeu usuário é: root";
//        $mensagem[] = "\tSua senha é: {$senha}";
//        $sucesso = self::epc($email, "Seu Servidor Cloud está pronto!", $mensagem);
//        if ($sucesso) {
//            $corpo_gestor[] = "Servidor: {$hostname}";
//            $corpo_gestor[] = "IP: {$ip}";
//            $corpo_gestor[] = "Senha: {$senha}";
//            self::epg($nome, $email, "Novo Servidor Cloud", $corpo_gestor);
//        }
//        return $sucesso;
//    }

}