<?php

class dao_crp_tipo_pagamento {

    private static function dao() {
        return array(
            array(
                "api" => "pagseguro",
                "nome" => "PagSeguro",
                "imagem" => "pagseguro-pagamento.png"
            )
        );
    }

    public static function pegar($id) {
        $dao = self::dao();
        if (!empty($dao[$id])) {
            return self::hook($dao[$id]);
        }
        return false;
    }

    public static function listar() {
        foreach (self::dao() as $key => $row) {
            $row["id"] = $key;
            $array[$key] = self::hook($row);
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

}