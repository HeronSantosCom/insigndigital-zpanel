<?php

class layout_emails extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->extract(dao_modulo::layout_uso("email", logon::meu_id()), "uso");
        $this->emails = dao_email::listar(logon::meu_id());
        $this->dominios = dao_dominio::listar(logon::meu_id());
    }

    private function abrir($id) {
        $dao = (dao_email::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o e-mail, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_email::atualizar($id, $this->senha, $this->bloqueado);
            } else {
                $action = dao_email::cadastrar($this->endereco, $this->senha, $this->dominio_id, logon::meu_id());
                if ($action) {
                    snr::uac(logon::meu_nome(), logon::meu_email(), "Novo E-mail: {$this->endereco}@{$this->dominio_id}");
                }
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("E-mail salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o e-mail, tente novamente!");
        if ($id) {
            $action = dao_email::remover($id);
            if ($action) {
                snr::uac(logon::meu_nome(), logon::meu_email(), "E-mail Removido: {$this->endereco}");
                $this->msgbox("E-mail removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}