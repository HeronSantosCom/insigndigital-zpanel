<?php

if (!class_exists("zpanel_servidor", false)) {
    include path::applications("zpanel/servidor.php");
}

class dao_servidor {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("servidor");
        $db->column("*");
        $db->match("id", $id);
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $tipo_servidor_id = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("servidor");
        $db->column("*");
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        if ($tipo_servidor_id) {
            $db->match("tipo_servidor_id", $tipo_servidor_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

    public static function uso($usuario_id) {
        $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
        if ($usuario_zpanel_id) {
            $titulo = array(
                "domains" => array("nome" => "Domínios", "modulo" => "dominio"),
                "subdomains" => array("nome" => "Subdomínios", "modulo" => "subdominio"),
                "mailboxes" => array("nome" => "E-mail", "modulo" => "email"),
                "forwarders" => array("nome" => "Redirecionamentos de E-mail", "modulo" => "redirecionamento_email"),
                "distlists" => array("nome" => "Grupos de Distribuição de E-mail", "modulo" => "lista_email"),
                "ftpaccounts" => array("nome" => "FTP", "modulo" => "ftp"),
                "mysql" => array("nome" => "MySQL", "modulo" => "mysql"),
                "diskspace" => array("nome" => "Espaço", "size" => true),
                "bandwidth" => array("nome" => "Transferência", "size" => true)
            );
            $uso = zpanel_servidor::uso($usuario_zpanel_id);
            if ($uso) {
                if (!empty($uso["stats"]["domains"]) && !empty($uso["stats"]["parkeddomains"])) {
                    $uso["stats"]["domains"] += $uso["stats"]["parkeddomains"];
                }
                $valores = false;
                foreach ($titulo as $key => $row) {
                    $row["usado"] = ($uso["stats"][$key] ? $uso["stats"][$key] : 0);
                    $row["total"] = ($uso["quota"][$key] ? $uso["quota"][$key] : 0);
                    $row["disponivel"] = $row["total"] - $row["usado"];
                    $valores[(!empty($row["modulo"]) ? $row["modulo"] : $key)] = $row;
                }
                return $valores;
            }
        }
        return false;
    }

    public static function atividade() {
        $status = zpanel_servidor::atividade();
        if ($status) {
            $valores = false;
            $titulo = array(
                "web" => array("nome" => "HTTP"),
                "ftp" => array("nome" => "FTP"),
                "mysql" => array("nome" => "MySQL"),
                "imap" => array("nome" => "IMAP"),
                "smtp" => array("nome" => "SMTP"),
                "pop3" => array("nome" => "POP3")
            );
            foreach ($titulo as $key => $row) {
                $row["status"] = false;
                if (!empty($status[$key])) {
                    if ($status[$key] == "1") {
                        $row["status"] = true;
                    }
                }
                $valores[] = $row;
            }
            return $valores;
        }
    }

    public static function usuario_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["usuario_id"]) ? $dao["usuario_id"] : false);
    }

}