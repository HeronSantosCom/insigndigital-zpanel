<?php

class layout_listas_email_membros extends main {

    public function __construct() {
        if (isset($_GET["parent_id"])) {
            $this->extract($_GET);
            $lista_email = dao_lista_email::pegar($this->parent_id, logon::meu_id());
            if ($lista_email) {
                $this->extract($lista_email, "lista_email");

                if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
                    $this->formulario = true;
                    if (!empty($_GET["id"])) {
                        if (!$this->abrir($_GET["id"])) {
                            define("app_layout_error", true);
                            return false;
                        }
                    }
                    if (isset($_GET["remover"])) {
                        $this->remover = true;
                    }
                    if (isset($_POST["id"])) {
                        $this->salvar($_POST["id"]);
                    }
                }

                $this->listas_email_membros = dao_lista_email_membro::listar(logon::meu_id(), $this->lista_email_id);
                $this->emails = dao_email::listar(logon::meu_id());
                return true;
            }
        }
        define("app_layout_error", true);
    }

    private function abrir($id) {
        $dao = (dao_lista_email_membro::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o membro do grupo de distribuição de e-mail, verifique os dados e tente novamente!");
            $action = dao_lista_email_membro::cadastrar($this->endereco, $this->lista_email_id, logon::meu_id());
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Membro do grupo de distribuição de e-mail salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o membro do grupo de distribuição de e-mail, tente novamente!");
        if ($id) {
            $action = dao_lista_email_membro::remover($id);
            if ($action) {
                $this->msgbox("Membro do grupo de distribuição de e-mail removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}