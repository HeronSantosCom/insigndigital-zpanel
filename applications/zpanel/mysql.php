<?php

class zpanel_mysql extends api_zpanel {

    static public function pegar($id) {
        $arr = self::PostXMWS("mysql_databases", "GetCurrentDatabases", $id);
        if (isset($arr["list"])) {
            return $arr["list"];
        }
        return false;
    }

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("mysql_databases", "GetDatabases", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["mysqlid"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($nome, $usuario_id) {
        $arr = self::PostXMWS("mysql_databases", "CreateMySQL", "<uid>{$usuario_id}</uid><databasename>{$nome}</databasename>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("mysql_databases", "DeleteMySQL", "<databasenameid>{$id}</databasenameid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
