<?php

class dao_crp_tipo_valor {

    private static function dao() {
        return array(
            array("valor" => 7),
            array("valor" => 12),
            array("valor" => 23),
            array("valor" => 46),
            array("valor" => 91),
            array("valor" => 180),
            array("valor" => 359),
            array("valor" => 538),
            array("valor" => 717),
            array("valor" => 1075)
        );
    }

    public static function pegar($id) {
        $dao = self::dao();
        if (!empty($dao[$id])) {
            return self::hook($dao[$id]);
        }
        return false;
    }

    public static function listar() {
        foreach (self::dao() as $key => $row) {
            $row["id"] = $key;
            $array[$key] = self::hook($row);
        }
        return $array;
    }

    private static function hook($row) {
        $row["valor_br"] = number_format($row["valor"], 2, ',', '.');
        return $row;
    }

}