<?php

class layout_assinatura extends main {

    public function __construct() {
        if (isset($_GET["assinar"])) {
            $this->assinar = true;
        }
        if (!empty($_GET["produto"])) {
            $this->xhr_produto($_GET["produto"]);
        } else {
            if ($this->assinar) {
                $this->assinar($_GET["id"]);
                if (!empty($_POST["plano_id"])) {
                    $this->salvar();
                }
            } else {
                $this->listagem();
            }
        }
    }

    private function listagem() {
        $this->hospedagem = dao_produtos::listar_planos();
        if (!$this->hospedagem) {
            $this->off = true;
        }
    }

    private function produto($id) {
        $produto = dao_produtos::pegar_plano($id);
        $this->extract($produto);
    }

    private function xhr_produto($id) {
        $this->produto($id);
        print knife::html("layout/main/assinatura/produto.html");
    }

    private function assinar($id) {
        $this->extract(self::session(), "us");
        $this->produto($id);
    }

    private function salvar() {
        $this->extract($_POST);
        $this->msgbox("Ocorreu um erro ao assinar o plano, tente novamente!");
        if ($this->associar_plano()) {
            self::reboot(logon::meu_id(), false, false, (self::cookie() ? '1' : '0'));
            $this->session_msgbox("Seu plano foi contratado com sucesso!");
            knife::redirect("/index.html");
        }
    }

    private function associar_plano() {
        $produto = dao_produtos::pegar_plano($this->plano_id);
        if ($produto) {
            $action = dao_usuario::associar_plano(logon::meu_id(), $this->plano_id);
            if ($action) {
                $bonus = dao_usuario::checa_bonus(logon::meu_email());
                if ($bonus) {
                    // define o ultimo dia do mes
                    $dias = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
                    //define o valor a creditar
                    $valor = ($produto["valor"] / $dias) * 15;
                    //credita saldo
                    $action = dao_crp_transacao::cadastrar(getmypid(), dao_crp_transacao::gera_codigo(logon::meu_id()), "Bônus pela Assinatura do Plano {$produto["nome"]}", $valor, date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), 5, logon::meu_id());
                    if ($action) {
                        dao_usuario::atualizar_saldo(logon::meu_id(), $valor);
                    }
                }
                snr::uap(logon::meu_nome(), logon::meu_email(), logon::meu_telefone(), $produto["nome"], $produto["valor_br"], $bonus);
                return true;
            }
        }
        return false;
    }

}