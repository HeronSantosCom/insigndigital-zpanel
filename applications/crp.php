<?php

if (!class_exists("pagseguro", false)) {
    include path::plugins("pagseguro.php");
}

/**
 * Centro de Recebimento e Pagamento
 */
class crp extends app {

    private $module = false;

    public function __construct($module = false) {
        $this->module = $module;
        switch ($this->module) {
            case "pagseguro":
                $this->pagseguro();
                break;
        }
    }

    /**
     * Módulo de recebimento de notificações do PagSeguro
     */
    private function pagseguro() {
        if (!empty($_POST["notificationType"]) && $_POST["notificationType"] == "transaction") {
            $retorno = pagseguro::notificacao($_POST["notificationCode"]);
            if ($retorno) {
                $crp_transacao = dao_crp_transacao::pegar(false, $retorno->reference);
                if ($crp_transacao) {
                    $pgs_tipo_cancelamento = (!empty($retorno->cancellationSource) ? dao_pgs_tipo_cancelamento::pegar($retorno->cancellationSource) : false);
                    $action = dao_pgs_transacao::cadastrar($retorno->code, $retorno->installmentCount, ($pgs_tipo_cancelamento ? $pgs_tipo_cancelamento["id"] : false), $retorno->status, $retorno->type, $retorno->paymentMethod->code, $crp_transacao["id"]);
                    if ($action) {
                        // efetua operacao
                        switch ($retorno->status) {
                            case "3": // credito confirmado
                                // registra credito do usuario
                                $this->registra_credito($crp_transacao);
                                break;
                            case "4": // credito efetivado
                                // credita comissão do revendedor
                                $this->registra_comissao($crp_transacao);
                                break;
                            case "6": // credito estornado
                                // devolve credito do cliente
                                $this->devolve_credito($crp_transacao);
                                break;
                            case "7": // cancelado
                                $this->cancela_transacao($crp_transacao);
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Efetua o Crédito do Cliente
     * 
     * @param type $crp_transacao
     */
    private function registra_credito($crp_transacao) {
        // abre cliente
        $usuario = logon::reboot($crp_transacao["usuario_id"], false, false, false);
        if ($usuario) {
            // atualiza saldo do cliente
            $action = dao_usuario::atualizar_saldo($usuario["id"], ($usuario["saldo"] + $crp_transacao["valor"]));
            if ($action) {
                // efetiva transação do cliente
                if (dao_crp_transacao::efetivar($crp_transacao["id"])) {
                    // notifica cliente
                    snr::crpuc($usuario["nome"], $usuario["email"], $usuario["telefone"], $crp_transacao["descricao"], $crp_transacao["valor_br2"]);
                    // desbloqueia cliente caso esteja bloqueado
                    $usuario = logon::reboot($usuario["id"], false, false, false);
                    if ($usuario) {
                        $cron = new cron();
                        $cron->desbloqueio_saldo($usuario);
                    }
                }
            }
        }
    }

    /**
     * Efetura o Crédito da Comissão do Revendedor
     * 
     * @param type $crp_transacao
     */
    private function registra_comissao($crp_transacao) {
        // abre cliente
        $usuario = logon::reboot($crp_transacao["usuario_id"], false, false, false);
        if ($usuario) {
            // abre revendedor
            $revenda = logon::reboot($usuario["usuario_id"], false, false, false);
            if ($revenda) {
                // calculo de porcentagem referente ao revendedor
                $crp_tipo_transacao_id = 2;
                $descricao = "Comissão 100% ({$crp_transacao["codigo"]})";
                $valor = $crp_transacao["valor"]; // adminitradores do sistema recebem valor integral em forma de créditos
                // caso contrario recebem 30% do valor creditado
                if ($usuario["usuario_id"] != $usuario["servidor_usuario_id"]) {
                    $crp_tipo_transacao_id = 4;
                    $descricao = "Comissão 30% ({$crp_transacao["codigo"]})";
                    $valor = ($crp_transacao["valor"] * 0.3);
                }
                // efetua o crédito do revendedor
                $action = dao_crp_transacao::cadastrar(getmypid(), dao_crp_transacao::gera_codigo($usuario["usuario_id"]), $descricao, $valor, date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), $crp_tipo_transacao_id, $usuario["usuario_id"]);
                if ($action) {
                    // atualiza crédito do revendedor
                    $action = dao_usuario::atualizar_saldo($revenda["id"], ($revenda["saldo"] + $valor));
                    if ($action) {
                        // notifica revendedor
                        snr::crpcc($revenda["nome"], $revenda["email"], $revenda["telefone"], $descricao, number_format($valor, 2, ',', '.'));
                    }
                }
            }
        }
    }

    /**
     * 
     * @param type $crp_transacao
     */
    private function devolve_credito($crp_transacao) {
        // abre cliente
        $usuario = logon::reboot($crp_transacao["usuario_id"], false, false, false);
        if ($usuario) {
            // atualiza saldo do cliente
            $action = dao_usuario::atualizar_saldo($usuario["id"], ($usuario["saldo"] - $crp_transacao["valor"]));
            if ($action) {
                // efetiva cancelamento  do cliente
                $this->cancela_transacao($crp_transacao["id"]);
                // desbloqueia cliente caso esteja bloqueado
                $usuario = logon::reboot($usuario["id"], false, false, false);
                if ($usuario) {
                    $cron = new cron();
                    $cron->bloqueio_saldo($usuario);
                }
            }
        }
    }

    /**
     * Efetura o Cancelamento da Transação
     * 
     * @param type $crp_transacao
     * @return type
     */
    private function cancela_transacao($crp_transacao) {
        return dao_crp_transacao::cancelar($crp_transacao["id"]);
    }

    /**
     * Registrador de Crédito Manual
     */
    public function credito_manual() {
        $funcao = permalink::get();
        if (!empty($funcao[2])) {
            $valor = number_format($funcao[0], 5, '.', '');
            $usuario = logon::reboot(false, $funcao[2], false, false);
            if ($usuario) {
                $dao_crp_transacao_id = dao_crp_transacao::cadastrar(getmypid(), dao_crp_transacao::gera_codigo(logon::meu_id()), $funcao[1], $valor, date("Y-m-d H:i:s"), false, 5, logon::meu_id());
                if ($dao_crp_transacao_id) {
                    knife::dump("Inserção de Crédito Manual");
                    knife::dump("Usuário: {$usuario["nome"]}");
                    knife::dump("Valor: R$ {$valor}");
                    knife::dump("Novo Saldo: R$ " . ($usuario["saldo"] + $valor));
                    $this->registra_credito(dao_crp_transacao::pegar($dao_crp_transacao_id));
                }
            }
        }
    }

    /**
     * Saldo
     */
    public function saldo() {
        $funcao = permalink::get();
        if (!empty($funcao[0])) {
            $usuario = logon::reboot(false, $funcao[0], false, false);
            if ($usuario) {
                knife::dump("Usuário: R$ {$usuario["nome"]}");
                knife::dump("Saldo: R$ {$usuario["saldo"]}");
            }
        }
    }

}