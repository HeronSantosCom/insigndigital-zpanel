<?php

if (!class_exists("zpanel_usuario", false)) {
    include path::applications("zpanel/usuario.php");
}

class dao_usuario {

    public static function pegar($id, $usuario = false, $senha = false, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("*");
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->match("email", $usuario);
            if ($senha) {
                $db->match("senha", md5($senha));
            }
        }
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            $servidor = dao_servidor::pegar($dao[0]["servidor_id"]);
            $plano = dao_plano::pegar($dao[0]["plano_id"]);
            return self::hook($dao[0], $plano, $servidor);
        }
        return false;
    }

    public static function listar($usuario_id, $all = false, $orderby = "nome") {
        $array = false;
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("*");
        if (!$all) {
            $db->match("usuario_id", $usuario_id);
            $db->match("id", $usuario_id, false, true);
        }
        $db->is("removido", false);
        if (!is_array($orderby)) {
            $db->order($orderby);
        } else {
            foreach ($orderby as $order) {
                $db->order($order);
            }
        }
        $dao = $db->go();
        if ($dao) {
            $dao_plano = dao_plano::listar($usuario_id);
            $dao_servidor = dao_servidor::listar($usuario_id);
            foreach ($dao as $row) {
                $plano = (!empty($dao_plano[$row["plano_id"]]) ? $dao_plano[$row["plano_id"]] : false);
                $servidor = (!empty($dao_servidor[$row["servidor_id"]]) ? $dao_servidor[$row["servidor_id"]] : false);
                $array[$row["id"]] = self::hook($row, $plano, $servidor);
            }
        }
        return $array;
    }

    private static function hook($row, $plano, $servidor) {
        $row["gravatar"] = knife::gravatar($row["email"], 360);
        $row["bloqueado_data"] = ($row["bloqueado"] ? date("d/m/Y \á\s H:i", strtotime($row["bloqueado"])) : false);
        $row["bloqueado"] = ($row["bloqueado"] ? '1' : '0');
        $row["bloqueado_saldo_data"] = ($row["bloqueado_saldo"] ? date("d/m/Y \á\s H:i", strtotime($row["bloqueado_saldo"])) : false);
        $row["bloqueado_saldo"] = ($row["bloqueado_saldo"] ? '1' : '0');
        $row["motivo"] = ($row["motivo"] ? $row["motivo"] : 'Nenhum motivo declarado!');
        if ($plano) {
            foreach ($plano as $key => $value) {
                $row["plano_{$key}"] = $value;
            }
        }
        if ($servidor) {
            foreach ($servidor as $key => $value) {
                $row["servidor_{$key}"] = $value;
            }
        }
        $row["saldo_br"] = number_format($row["saldo"], 2, ',', '.');
        return $row;
    }

    public static function checa_bonus($email) {
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("*");
        $db->match("email", $email);
        $db->is("removido", false, false, true);
        $dao = $db->go();
        if ($dao) {
            return false;
        }
        return true;
    }

    public static function saldo($id) {
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("saldo");
        $db->match("id", $id);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            $dao[0]["saldo_br"] = number_format($dao[0]["saldo"], 2, ',', '.');
            return $dao[0];
        }
        return true;
    }

    public static function associar_plano($id, $plano_id) {
        $usuario = self::pegar($id);
        if ($usuario && !$usuario["zpanel_id"]) {
//            $zpanel_usuario = substr(sha1(uniqid($usuario["email"] . mt_rand(), true)), 0, 6);
            $zpanel_usuario = knife::generate_uid($usuario["email"], 6);
            $zpanel_senha = knife::generate_random_password(6, 4);
            $plano_zpanel_id = dao_plano::zpanel_id($plano_id);
            if ($plano_zpanel_id) {
                $usuario_zpanel_id = self::zpanel_id(dao_servidor::usuario_id($usuario["servidor_id"]));
                $zpanel_id = zpanel_usuario::cadastrar($usuario["nome"], $usuario["email"], $usuario["telefone"], $zpanel_usuario, $zpanel_senha, $plano_zpanel_id, $usuario_zpanel_id);
                if ($zpanel_id) {
                    $db = new mysqlsave();
                    $db->table("usuario");
                    $db->column("zpanel_usuario", $zpanel_usuario);
                    $db->column("zpanel_senha", $zpanel_senha);
                    $db->column("grupo_id", 2);
                    $db->column("plano_id", $plano_id);
                    $db->column("zpanel_id", $zpanel_id);
                    $db->column("atualizado", date("Y-m-d H:i:s"));
                    $db->match("id", $id);
                    return $db->go();
                }
            }
        }
        return false;
    }

    public static function cadastrar($nome, $email, $senha, $telefone, $notificacao_sms, $notificacao_email, $plano_id, $grupo_id, $servidor_id, $usuario_id) {
        if (!self::verificar($email) && knife::is_mail($email)) {
//            $zpanel_usuario = substr(sha1(uniqid($email . mt_rand(), true)), 0, 6);
            $zpanel_usuario = knife::generate_uid($email, 6);
            $zpanel_senha = knife::generate_random_password(6, 4);
            $plano_zpanel_id = dao_plano::zpanel_id($plano_id);
            if ($plano_zpanel_id) {
                $usuario_zpanel_id = self::zpanel_id(dao_servidor::usuario_id($servidor_id));
                $zpanel_id = zpanel_usuario::cadastrar($nome, $email, $telefone, $zpanel_usuario, $zpanel_senha, $plano_zpanel_id, $usuario_zpanel_id);
                if (!$zpanel_id) {
                    return false;
                }
            }
            $db = new mysqlsave();
            $db->table("usuario");
            $db->column("nome", $nome);
            $db->column("email", $email);
            $db->column("senha", md5($senha));
            $db->column("telefone", $telefone);
            $db->column("bloqueado", false);
            $db->column("notificacao_sms", $notificacao_sms);
            $db->column("notificacao_email", $notificacao_email);
            $db->column("zpanel_usuario", $zpanel_usuario);
            $db->column("zpanel_senha", $zpanel_senha);
            $db->column("grupo_id", $grupo_id);
            $db->column("plano_id", $plano_id);
            $db->column("usuario_id", $usuario_id);
            $db->column("servidor_id", $servidor_id);
            $db->column("zpanel_id", (!empty($zpanel_id) ? $zpanel_id : false));
            $db->column("cadastrado", date("Y-m-d H:i:s"));
            $db->column("atualizado", date("Y-m-d H:i:s"));
            if ($db->go()) {
                return $db->id();
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $email, $senha, $telefone, $motivo, $bloqueado, $bloqueado_saldo, $notificacao_sms, $notificacao_email, $plano_id, $grupo_id) {
        if (!self::verificar($email, $id) && knife::is_mail($email)) {
            $zpanel_id = self::zpanel_id($id);
            $zpanel_senha = knife::generate_random_password(6, 4);
            $plano_zpanel_id = dao_plano::zpanel_id($plano_id);
            if ($zpanel_id && $plano_zpanel_id) {
                if ($plano_zpanel_id) {
                    if (!zpanel_usuario::atualizar($zpanel_id, $nome, $email, $telefone, ($bloqueado ? '0' : ($bloqueado_saldo ? '0' : '1')), $zpanel_senha, $plano_zpanel_id)) {
                        return false;
                    }
                } else {
                    if (!zpanel_usuario::desabilitar($zpanel_id)) {
                        return false;
                    }
                }
            }
            $db = new mysqlsave();
            $db->table("usuario");
            $db->column("nome", $nome);
            $db->column("email", $email);
            if ($senha) {
                $db->column("senha", md5($senha));
            }
            $db->column("telefone", $telefone);
            $db->column("motivo", $motivo);
            $db->column("bloqueado", ($bloqueado ? date("Y-m-d H:i:s") : false));
            $db->column("bloqueado_saldo", ($bloqueado_saldo ? date("Y-m-d H:i:s") : false));
            $db->column("notificacao_sms", $notificacao_sms);
            $db->column("notificacao_email", $notificacao_email);
            $db->column("zpanel_senha", $zpanel_senha);
            $db->column("grupo_id", $grupo_id);
            $db->column("plano_id", $plano_id);
            $db->column("atualizado", date("Y-m-d H:i:s"));
            $db->match("id", $id);
            return $db->go();
        }
        return false;
    }

    public static function definir_assistencia($id) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("assistente", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        return $db->go();
    }

    public static function atualizar_saldo($id, $saldo) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("saldo", $saldo);
        $db->match("id", $id);
        return $db->go();
    }

    public static function redefinir_senha($email, $senha) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("senha", md5($senha));
        $db->match("email", $email);
        $db->is("removido", false);
        return $db->go();
    }

    public static function remover($id, $motivo, $usuario_id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            $usuario_zpanel_id = self::zpanel_id($usuario_id);
            if (!zpanel_usuario::remover($zpanel_id, $usuario_zpanel_id)) {
                return false;
            }
        }
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("motivo", $motivo);
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        if ($db->go()) {
            if ($usuario_id) {
                self::mover_relacionados($id, $usuario_id);
            }
            self::remover_relacionados($id);
            return true;
        }
        return false;
    }

    private static function mover_relacionados($id, $usuario_id) {
        // move todos os planos
        $db = new mysqlsave();
        $db->table("plano");
        $db->column("usuario_id", $usuario_id);
        $db->column("atualizado", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // move todos os usuarios
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("usuario_id", $usuario_id);
        $db->column("atualizado", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
    }

    private static function remover_relacionados($id) {
        // remove apelido de email
        $db = new mysqlsave();
        $db->table("apelido_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove dns
        $db = new mysqlsave();
        $db->table("dns");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove dominio
        $db = new mysqlsave();
        $db->table("dominio");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove e-mail
        $db = new mysqlsave();
        $db->table("email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove ftp
        $db = new mysqlsave();
        $db->table("ftp");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove lista de email
        $db = new mysqlsave();
        $db->table("lista_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove membros lista de email
        $db = new mysqlsave();
        $db->table("lista_email_membro");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove mysql
        $db = new mysqlsave();
        $db->table("mysql");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove usuarios de mysql
        $db = new mysqlsave();
        $db->table("mysql_usuario");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove associação de usuarios de mysql
        $db = new mysqlsave();
        $db->table("mysql_usuario_associacao");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove redirecionamento de email
        $db = new mysqlsave();
        $db->table("redirecionamento_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
        // remove subdominio
        $db = new mysqlsave();
        $db->table("subdominio");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("usuario_id", $id);
        $db->go();
    }

    public static function verificar($email, $id = false) {
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("id");
        $db->match("email", $email);
        if ($id) {
            $db->match("id", $id, false, true);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function servidor_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["servidor_id"]) ? $dao["servidor_id"] : false);
    }

    public static function zpanel_usuario($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_usuario"]) ? $dao["zpanel_usuario"] : false);
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}