<?php

if (!class_exists("zpanel_dns", false)) {
    include path::applications("zpanel/dns.php");
}

class dao_dns {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("dns");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $dominio_id, $subdominio_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("dns");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        if ($dominio_id) {
            $db->match("dominio_id", $dominio_id);
        } else {
            if ($subdominio_id) {
                $db->match("subdominio_id", $subdominio_id);
            } else {
                return false;
            }
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $key => $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

    public static function criar($dominio_id, $usuario_id) {
        $dominio_zpanel_id = dao_dominio::zpanel_id($dominio_id);
        $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
        if ($dominio_zpanel_id && $usuario_zpanel_id) {
            if (zpanel_dns::criar($dominio_zpanel_id, $usuario_zpanel_id)) {
                $registros = zpanel_dns::listar($dominio_zpanel_id);
                if ($registros) {
                    foreach ($registros as $zpanel) {
                        $db = new mysqlsave();
                        $db->table("dns");
                        $db->column("tipo", $zpanel["type"]);
                        $db->column("valor", $zpanel["host"]);
                        $db->column("ttl", $zpanel["ttl"]);
                        $db->column("alvo", $zpanel["target"]);
                        $db->column("prioridade", (!is_array($zpanel["priority"]) ? $zpanel["priority"] : false));
                        $db->column("peso", (!is_array($zpanel["weight"]) ? $zpanel["weight"] : false));
                        $db->column("porta", (!is_array($zpanel["port"]) ? $zpanel["port"] : false));
                        $db->column("dominio_id", $dominio_id);
                        $db->column("subdominio_id", false);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel["id"]);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        $db->go();
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static function cadastrar($tipo, $valor, $ttl, $alvo, $prioridade, $peso, $porta, $dominio_id, $subdominio_id, $usuario_id) {
        $dominio_zpanel_id = ($dominio_id ? dao_dominio::zpanel_id($dominio_id) : ($subdominio_id ? dao_subdominio::zpanel_id($subdominio_id) : false));
        $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
        if ($dominio_zpanel_id && $usuario_zpanel_id) {
            $zpanel_id = zpanel_dns::cadastrar($tipo, $valor, $ttl, $alvo, $prioridade, $peso, $porta, $dominio_zpanel_id, $usuario_zpanel_id);
            if ($zpanel_id) {
                $db = new mysqlsave();
                $db->table("dns");
                $db->column("tipo", $tipo);
                $db->column("valor", $valor);
                $db->column("ttl", $ttl);
                $db->column("alvo", $alvo);
                $db->column("prioridade", $prioridade);
                $db->column("peso", $peso);
                $db->column("porta", $porta);
                $db->column("dominio_id", $dominio_id);
                $db->column("subdominio_id", $subdominio_id);
                $db->column("usuario_id", $usuario_id);
                $db->column("zpanel_id", $zpanel_id);
                $db->column("cadastrado", date("Y-m-d H:i:s"));
                $db->column("atualizado", date("Y-m-d H:i:s"));
                if ($db->go()) {
                    return $db->id();
                }
            }
        }
        return false;
    }

    public static function atualizar($id, $tipo, $valor, $ttl, $alvo, $prioridade, $peso, $porta) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_dns::atualizar($zpanel_id, $tipo, $valor, $ttl, $alvo, $prioridade, $peso, $porta)) {
                $db = new mysqlsave();
                $db->table("dns");
                $db->column("tipo", $tipo);
                $db->column("valor", $valor);
                $db->column("ttl", $ttl);
                $db->column("alvo", $alvo);
                $db->column("prioridade", $prioridade);
                $db->column("peso", $peso);
                $db->column("porta", $porta);
                $db->column("atualizado", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_dns::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("dns");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function destruir($dominio_id) {
        $db = new mysqlsave();
        $db->table("dns");
        $db->column("removido", date("Y-m-d H:i:s"));
        if ($dominio_id) {
            $db->match("dominio_id", $dominio_id);
        } else {
            return false;
        }
        return $db->go();
    }

    public static function status($dominio_id) {
        $dominio_zpanel_id = dao_dominio::zpanel_id($dominio_id);
        return zpanel_dns::status($dominio_zpanel_id);
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}