<?php

if (!class_exists("zpanel_email", false)) {
    include path::applications("zpanel/email.php");
}

class dao_email {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("email");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $redirecionamento_email = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("email");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        if ($redirecionamento_email) {
            $db->in("id", "(SELECT email_id FROM redirecionamento_email WHERE removido IS NULL)", false, true);
        }
        $db->is("removido", false);
        $db->order("endereco");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["webmail"] = "webmail." . substr($row["endereco"], strpos($row["endereco"], "@") + 1);
        $row["bloqueado_data"] = ($row["bloqueado"] ? date("d/m/Y \á\s H:i", strtotime($row["bloqueado"])) : false);
        $row["bloqueado"] = ($row["bloqueado"] ? '1' : '0');
        return $row;
    }

    public static function cadastrar($endereco, $senha, $dominio_id, $usuario_id) {
        if (dao_modulo::zpanel_disponivel("email", $usuario_id)) {
            $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
            $dominio_hostname = dao_dominio::hostname($dominio_id);
            if ($usuario_zpanel_id && $dominio_hostname) {
                $endereco = "{$endereco}@{$dominio_hostname}";
                if (!self::verificar($endereco)) {
                    $zpanel_id = zpanel_email::cadastrar($endereco, $senha, $usuario_zpanel_id);
                    if ($zpanel_id) {
                        $db = new mysqlsave();
                        $db->table("email");
                        $db->column("endereco", $endereco);
                        $db->column("senha", $senha);
                        $db->column("dominio_id", $dominio_id);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            return $db->id();
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function atualizar($id, $senha, $bloqueado) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_email::atualizar($zpanel_id, $senha, ($bloqueado ? '0' : '1'))) {
                $db = new mysqlsave();
                $db->table("email");
                if ($senha) {
                    $db->column("senha", $senha);
                }
                $db->column("bloqueado", ($bloqueado ? date("Y-m-d H:i:s") : false));
                $db->column("atualizado", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_email::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("email");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    self::remover_relacionados($id);
                    return true;
                }
            }
        }
        return false;
    }

    private static function remover_relacionados($id) {
        // remove apelidos de email
        $db = new mysqlsave();
        $db->table("apelido_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("email_id", $id);
        $db->go();
        // remove redirecionamentos de emails
        $db = new mysqlsave();
        $db->table("redirecionamento_email");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("email_id", $id);
        $db->go();
    }

    public static function verificar($endereco) {
        $db = new mysqlsearch();
        $db->table("email");
        $db->column("id");
        $db->match("endereco", $endereco);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function endereco($id) {
        $dao = self::pegar($id);
        return (!empty($dao["endereco"]) ? $dao["endereco"] : false);
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}