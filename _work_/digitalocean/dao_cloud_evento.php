<?php

class dao_cloud_evento {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("cloud_evento");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return $dao[0];
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("cloud_evento");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("data", "DESC");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = $row;
            }
        }
        return $array;
    }

    public static function cadastrar($descricao, $cloud_servidor_id, $usuario_id, $digitalocean_id) {
        $db = new mysqlsave();
        $db->table("cloud_evento");
        $db->column("descricao", $descricao);
        $db->column("cloud_servidor_id", $cloud_servidor_id);
        $db->column("usuario_id", $usuario_id);
        $db->column("digitalocean_id", $digitalocean_id);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function digitalocean_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["digitalocean_id"]) ? $dao["digitalocean_id"] : false);
    }

}