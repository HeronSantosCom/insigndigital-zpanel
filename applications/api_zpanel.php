<?php

if (!class_exists("xmwsclient", false)) {
    include path::plugins("xmwsclient.php");
}

class api_zpanel {

//    const host = '';
//    const api_key = '';

    static private function ConnectXMWS($module, $function) {
        $hostname = logon::meu_servidor_hostname();
        $zpanel_api = logon::meu_servidor_zpanel_api();
        if ($hostname && $zpanel_api) {	
            $xmws = new xmwsclient();
            $xmws->InitRequest("http://{$hostname}", $module, $function, $zpanel_api);
            if ($xmws) {
                knife::dump("ZPanel ConnectXMWS({$hostname}): Ok!");
                return $xmws;
            }
        }
        knife::dump("ZPanel ConnectXMWS({$hostname}): Erro!");
        return false;
    }

    static protected function GetXMWS($module, $function) {
        $t_start = array_sum(explode(' ', microtime()));
        $xmws = self::ConnectXMWS($module, $function);
        if ($xmws) {
            $xmws->SetRequestData('');
            $arr = $xmws->XMLDataToArray($xmws->Request($xmws->BuildRequest()), 0);
            self::dump($module, $function, false, $arr, $t_start);
            if ($arr['xmws']['response'] == 1101) {
                if (isset($arr['xmws']['content'])) {
                    return $arr['xmws']['content'];
                }
            }
        }
        return false;
    }

    static protected function PostXMWS($module, $function, $data) {
        $t_start = array_sum(explode(' ', microtime()));
        $xmws = self::ConnectXMWS($module, $function);
        if ($xmws) {
            $xmws->SetRequestData($data);
            $arr = $xmws->XMLDataToArray($xmws->Request($xmws->BuildRequest()), 0);
            self::dump($module, $function, $data, $arr, $t_start);
            if ($arr['xmws']['response'] == 1101) {
                if (isset($arr['xmws']['content'])) {
                    return $arr['xmws']['content'];
                }
            }
        }
        return false;
    }

    static protected function dump($module, $function, $data, $arr, $t_start) {
        $t_end = ((ceil((array_sum(explode(' ', microtime())) - $t_start) * 10000)) / 10000);
        knife::dump("ZPanel Call: {$module}::{$function}({$data})", "ZPanel Content Rows: " . (!empty($arr['xmws']['content']) ? count($arr['xmws']['content']) : false), "ZPanel Time: {$t_end}s", str_pad("", 80, "="));
    }

}
