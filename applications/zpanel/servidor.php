<?php

class zpanel_servidor extends api_zpanel {

    static public function configuracao() {
        $arr = self::GetXMWS("zpanelconfig", "GetAllSystemOptions");
        if (isset($arr["list"])) {
            return $arr["list"];
        }
        return false;
    }

    static public function uso($usuario_id) {
        $arr = self::PostXMWS("usage_viewer", "GetServerUsageStatsByUser", '<userid>' . $usuario_id . '</userid>');
        if (isset($arr)) {
            return $arr;
        }
        return false;
    }

    static public function atividade() {
        $arr = self::GetXMWS("services", "GetServiceStatus");
        if (isset($arr["list"])) {
            return $arr["list"];
        }
        return false;
    }

}
