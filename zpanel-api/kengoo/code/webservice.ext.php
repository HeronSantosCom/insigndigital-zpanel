<?php

/**
 * @package zpanelx
 * @subpackage modules
 * @author Heron Santos (contato@heronsantos.com)
 * @copyright Insign Digital (http://www.insigndigital.com.br/)
 * @link http://www.insigndigital.com.br/
 * @license GPL (http://www.gnu.org/licenses/gpl.html)
 */
class webservice extends ws_xmws {

	private function parametros() {
        $wsdata = $this->RawXMWSToArray($this->wsdata);
		if (!empty($wsdata['content'])) {
			return json_decode($wsdata['content']);
		}
		return false;
	}
	
	private function comunicar($status, $response, $data, $id) {
        $dbo = new runtime_dataobject();
        $dbo->addItemValue('response', '');
        $dbo->addItemValue('content', json_encode($data));
        return $dbo->getDataObject();
	}

    public function dominio_listar_todos() {
		try {
			$arr = self::parametros();
			
			$retorno = module_controller::dominio_listar_todos();
			
			return self::comunicar(true, false, $retorno, false);
		} catch (Exception $e) {
			return self::comunicar(false, $e->getMessage(), false, false);
		}
    }

}
