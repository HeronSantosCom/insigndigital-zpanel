<?php

class cron {

    private $time = false;

    public function __construct() {
        $this->time = time();
    }

    private function executar($name) {
        $pid = "/tmp/id_{$name}" . date("Y-m-d", $this->time) . ".pid";
        if (file_exists($pid)) {
            return false;
        }
        return file_put_contents($pid, getmypid());
    }

    private function sair($name) {
        $pid = "/tmp/id_{$name}" . date("Y-m-d", $this->time) . ".pid";
        if (file_exists($pid)) {
            return unlink($pid);
        }
        return false;
    }

    /**
     * Debitar Saldo do Plano de Hospedagem a cada 12 horas
     */
    public function debito_hospedagem($usuario) {
        if (!empty($usuario["plano_valor"]) && $usuario["bloqueado"] != "1" && $usuario["bloqueado_saldo"] != "1") {
            if (!dao_crp_transacao::listar($usuario["id"], false, 6, date("Y-m-d", $this->time))) {
                $valor = ($usuario["plano_valor"] / cal_days_in_month(CAL_GREGORIAN, date("m", $this->time), date("Y", $this->time)));
                if (dao_crp_transacao::cadastrar(getmypid(), dao_crp_transacao::gera_codigo($usuario["id"]), "Assinatura de Hospedagem {$usuario["plano_nome"]}", (-1 * $valor), date("Y-m-d H:i:s", $this->time), date("Y-m-d H:i:s", $this->time), 6, $usuario["id"])) {
                    dao_usuario::atualizar_saldo($usuario["id"], ($usuario["saldo"] - $valor));
                }
            }
        }
    }

    /**
     * Debitar Saldo do Servidores Cloud a cada 12 horas
     */
//    public function debito_cloud($usuario) {
//        // 7
//    }

    /**
     * Bloqueio da Conta caso o saldo seja inferior ou igual a Zero a cada 3 horas
     */
    public function bloqueio_saldo($usuario) {
        if (!empty($usuario["plano_valor"])) {
            if ($usuario["bloqueado_saldo"] != "1" && $usuario["saldo"] <= 0) {
                $action = dao_usuario::atualizar($usuario["id"], $usuario["nome"], $usuario["email"], false, $usuario["telefone"], $usuario["motivo"], $usuario["bloqueado"], 1, $usuario["notificacao_sms"], $usuario["notificacao_email"], $usuario["plano_id"], $usuario["grupo_id"]);
                if ($action) {
                    snr::ub($usuario["nome"], $usuario["email"], $usuario["telefone"], "Crédito insuficiente para manter seu plano em atividade.");
                }
            }
        }
    }

    /**
     * Desbloqueio da Conta caso esteja bloqueado e o saldo superior ao plano contratado a cada 3 horas
     */
    public function desbloqueio_saldo($usuario) {
        if (!empty($usuario["plano_valor"])) {
            $valor = ($usuario["plano_valor"] / cal_days_in_month(CAL_GREGORIAN, date("m", $this->time), date("Y", $this->time))) * 3;
            if ($usuario["bloqueado_saldo"] == "1" && $usuario["saldo"] >= $valor) {
                $action = dao_usuario::atualizar($usuario["id"], $usuario["nome"], $usuario["email"], false, $usuario["telefone"], $usuario["motivo"], $usuario["bloqueado"], 0, $usuario["notificacao_sms"], $usuario["notificacao_email"], $usuario["plano_id"], $usuario["grupo_id"]);
                if ($action) {
                    snr::ud($usuario["nome"], $usuario["email"], $usuario["telefone"]);
                }
            }
        }
    }

    /**
     * Gerar PDF com faturamento mensal a cada 30 dias
     */
    public function faturamento_mensal($usuario) {
        // fazer
    }

    /**
     * Remover transações não efetivadas e não canceladas a mais de 1 mes a cada 30 dias
     */
    public function limpeza_transacoes($usuario) {
        // fazer
    }

    public function processar() {
        $funcao = permalink::get(0);
        if ($funcao) {
            if ($this->executar($funcao)) {
                $servidores = dao_servidor::listar(false, 1);
                if ($servidores) {
                    $servidores_size = sizeof($servidores);
                    $servidores_keys = array_keys($servidores);
                    for ($y = 0; $y < $servidores_size; $y++) {
                        $key = $servidores_keys[$y];
                        $servidor = $servidores[$key];
                        // ---- inicio servidor
                        if (logon::reboot($servidor["usuario_id"], false, false, false)) {
                            $usuarios = dao_usuario::listar($servidor["usuario_id"]);
                            if ($usuarios) {
                                $usuarios_size = sizeof($usuarios);
                                $usuarios_keys = array_keys($usuarios);
                                for ($y = 0; $y < $usuarios_size; $y++) {
                                    $key = $usuarios_keys[$y];
                                    $usuario = $usuarios[$key];
                                    // ---- inicio usuario
                                    $this->$funcao($usuario);
                                    // ---- fim usuario
                                    unset($key, $usuario);
                                }
                                unset($usuarios_size, $usuarios_keys, $usuarios);
                            }
                        }
                        // ---- fim servidor
                        unset($key, $servidor);
                    }
                    unset($servidores_size, $servidores_keys, $servidores);
                }
            }
            $this->sair($funcao);
        }
    }

    /**
     * Receber emails do Cloud@ a cada minuto
     */
//    public function spool_imap() {
//        if ($this->executar(__FUNCTION__)) {
//            new spool_imap();
//            $this->sair(__FUNCTION__);
//        }
//    }

    /**
     * Enviar no máximo 60 emails a cada 1 segundo / minuto
     */
    public function spool_smtp() {
        if ($this->executar(__FUNCTION__)) {
            // fazer
            $this->sair(__FUNCTION__);
        }
    }

    /**
     * Enviar no máximo 30 sms a cada 2 segundos / minuto
     */
    public function spool_sms() {
        if ($this->executar(__FUNCTION__)) {
            // fazer
            $this->sair(__FUNCTION__);
        }
    }

    /**
     * Verifica Status das Operações no Pag Seguro
     */
    public function pagseguro() {
        if ($this->executar(__FUNCTION__)) {
            // fazer
            $this->sair(__FUNCTION__);
        }
    }

    /**
     * Carrega a lista de backup dos Servidores Cloud
     */
//    public function cloud_imagem() {
//        if ($this->executar(__FUNCTION__)) {
//            // fazer
//            $this->sair(__FUNCTION__);
//        }
//    }

    /**
     * Identifica Novos Servidores Cloud
     */
//    public function cloud_servidor() {
//        if ($this->executar(__FUNCTION__)) {
//            $dao_spool_imap = dao_spool_imap::listar(array(date("Y-m-d", $this->time) . " 00:00:00", date("Y-m-d H:i:s", $this->time)), "0000-00-00 00:00:00", "support@mail.digitalocean.com");
//            if ($dao_spool_imap) {
//                foreach ($dao_spool_imap as $spool_imap) {
//                    //Your new Droplet is created!  - DigitalOcean
//                    if (preg_match("/\((.*)\)/", $spool_imap["assunto"], $matches)) {
//                        if ($matches[1]) {
//                            $hostname = trim(strtolower($matches[1]));
//                            $cloud_servidor = dao_cloud_servidor::pegar(false, $hostname);
//                            if ($cloud_servidor) {
//                                $mensagem = explode("\n", $spool_imap["mensagem"]);
//                                foreach ($mensagem as $linha) {
//                                    if (preg_match("/ip(.*):(.*)/i", $linha, $matches)) {
//                                        if (!empty($matches[2])) {
//                                            $ip = trim($matches[2]);
//                                        }
//                                    } else {
//                                        if (preg_match("/password:(.*)/i", $linha, $matches)) {
//                                            if (!empty($matches[1])) {
//                                                $senha = trim($matches[1]);
//                                            }
//                                        }
//                                    }
//                                }
//                                if (!empty($ip) && !empty($senha)) {
//                                    $action = dao_cloud_servidor::marcar_configurado($cloud_servidor["id"], $ip, $senha);
//                                    if ($action) {
//                                        $usuario = dao_usuario::pegar($cloud_servidor["usuario_id"]);
//                                        if ($usuario) {
//                                            snr::scc($usuario["nome"], $usuario["email"], $usuario["telefone"], $hostname, $ip, $senha);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    dao_spool_imap::marcar_lido($spool_imap["id"]);
//                }
//            }
//            $this->sair(__FUNCTION__);
//        }
//    }

//    public function envio_prepago($usuario) {
//        if (!empty($usuario["plano_valor"])) {
//            $mensagem[] = "Olá {$usuario["nome"]},";
//            $mensagem[] = "";
//            $mensagem[] = "Agora sua conta será pré-paga, isto é, para usufruir dos serviços da Insign Digital será necessário ter crédito disponível na sua conta.";
//            $mensagem[] = "Inicialmente com essa mudança, você recebeu cinco dias de créditos referentes ao seu plano contratado como vantagem para você poder fazer a recarga da sua conta.";
//            $mensagem[] = "Para saber mais sobre seu saldo, clique no menu 'Saldo' disposta na tela inicial ou no menu '{$usuario["nome"]} ({$usuario["zpanel_usuario"]})' e em seguida 'Saldo', lá você terá um detalhamento de movimentação financeira da sua conta.";
//            spool_smtp::epc($usuario["email"], "Sua conta agora é Pré-Pago!", $mensagem);
//        }
//    }
}
