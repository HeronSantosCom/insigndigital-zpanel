<?php

class zpanel_lista_email extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("distlists", "GetDistList", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($endereco, $usuario_id) {
        $endereco = explode("@", $endereco);
        if (!empty($endereco[1])) {
            $arr = self::PostXMWS("distlists", "CreateDistList", "<uid>{$usuario_id}</uid><inAddress>{$endereco[0]}</inAddress><inDomain>{$endereco[1]}</inDomain>");
            if (isset($arr['id']) and $arr['id'] != "false") {
                return $arr['id'];
            }
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("distlists", "DeleteDistList", "<distlistid>{$id}</distlistid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
