<?php

class logon extends app {

    public function __construct() {
        if (isset($_GET["sair"]) || isset($_GET["reset"])) {
            $this->logoff();
        }
        $this->login();
    }

    private function login() {
        if (empty($_SESSION["logon"])) {
            $this->extract(self::cookie());
            $this->extract($_POST);
            if ($this->usuario && $this->senha) {
                if (self::reboot(false, $this->usuario, $this->senha, $this->relembrar)) {
                    return true;
                }
                $this->msgbox("O nome de usuário ou a senha inserida está(ão) incorreto(s).");
            }
            return false;
        }
        return $_SESSION["logon"];
    }

    protected function cadastrar() {
        $this->extract($_POST);
        if ($this->usuario && $this->nome) {
            $this->msgbox("É necessário aceitar os termos para continuar.");
            if ($this->aceita_termos) {
                $senha = knife::generate_random_password(8, 4);
                $this->msgbox("Ocorreu um erro ao criar sua conta, tente novamente ou entre em contato através dos nossos canais de atendimento.");
                if (dao_usuario::cadastrar($this->nome, strtolower($this->usuario), $senha, false, 1, 1, false, 3, 4, 1)) {
                    if (snr::uc($this->nome, $this->usuario, false, $senha)) {
                        $this->session_msgbox("Sua conta foi criada com sucesso e sua credencial foi enviada para seu e-mail.");
                    } else {
                        knife::dump("Logon > Cadastrar: Não foi possível notificar o usuário da criação da conta. ( Nome: '{$this->nome}' | Usuário: '{$this->usuario}' | Senha : '{$senha}' ).");
                    }
                    knife::redirect("/entrar.html");
                    return true;
                }
            }
        }
        return false;
    }

    protected function redefinir_senha() {
        $this->extract($_POST);
        if ($this->usuario) {
            $senha = knife::generate_random_password(8, 4);
            $this->msgbox("Ocorreu um erro ao redefinir sua senha, tente novamente ou entre em contato através dos nossos canais de atendimento.");
            if (dao_usuario::redefinir_senha(strtolower($this->usuario), $senha)) {
                $usuario = dao_usuario::pegar(false, $this->usuario, $senha);
                if ($usuario) {
                    $this->session_msgbox("Sua senha foi redefinida com sucesso.");
                    if (snr::usr($usuario["nome"], $usuario["email"], $usuario["telefone"], $senha)) {
                        $this->session_msgbox("Sua senha foi redefinida com sucesso e sua nova credencial foi enviada para seu e-mail.");
                    } else {
                        knife::dump("Logon > Redefinir Senha: Não foi possível notificar o usuário da criação da conta. ( Nome: '{$usuario["nome"]}' | Usuário: '{$usuario["email"]}' | Senha : '{$senha}' ).");
                    }
                    knife::redirect("/entrar.html");
                    return true;
                }
            }
        }
        return false;
    }

    protected static function logoff() {
        $last = false;
        if (!empty($_SESSION["saved_logon"])) {
            $saved_logon = $_SESSION["saved_logon"];
            if (count($saved_logon) > 0) {
                $last = array_pop($saved_logon);
                $_SESSION["saved_logon"] = $saved_logon;
                return logon::reboot($last, false, false, false);
            }
        }
        $_COOKIE = false;
        $_SESSION = false;
        setcookie("logon", false, time() + 60 * 60 * 24 * 100, "/");
        session_unset();
        session_destroy();
        return true;
    }

    protected static function cookie($dao = false) {
        if ($dao) {
            return setcookie("logon", base64_encode(serialize($dao)), time() + 60 * 60 * 24 * 100, "/");
        }
        return (!empty($_COOKIE["logon"]) ? unserialize(base64_decode($_COOKIE["logon"])) : false);
    }

    protected static function session() {
        return (!empty($_SESSION["logon"]) ? $_SESSION["logon"] : false);
    }

    public static function reboot($id, $usuario, $senha, $relembrar) {
        $_SESSION["logon"] = dao_usuario::pegar($id, $usuario, $senha);
        if ($_SESSION["logon"]) {
            if ($relembrar) {
                self::cookie(array("usuario" => $usuario, "senha" => $senha, "relembrar" => $relembrar));
            }
            return $_SESSION["logon"];
        }
        return false;
    }

    public static function __callStatic($name, $arguments) {
        return (!empty($_SESSION["logon"][str_replace("meu_", "", $name)]) ? $_SESSION["logon"][str_replace("meu_", "", $name)] : (!empty($arguments[0]) ? $arguments[0] : false));
    }

}