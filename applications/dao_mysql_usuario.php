<?php

if (!class_exists("zpanel_mysql_usuario", false)) {
    include path::applications("zpanel/mysql_usuario.php");
}

class dao_mysql_usuario {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("mysql_usuario");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("mysql_usuario");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("usuario");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["acesso"] = ($row["acesso"] == "%" ? "A partir de qualquer IP" : "Somente a partir de IP {$row["acesso"]}");
        return $row;
    }

    public static function cadastrar($usuario, $senha, $acesso, $mysql_id, $usuario_id) {
        $usuario_zpanel_usuario = dao_usuario::zpanel_usuario($usuario_id);
        $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
        $mysql_zpanel_id = dao_mysql::zpanel_id($mysql_id);
        if ($usuario_zpanel_id && $mysql_zpanel_id && $usuario_zpanel_usuario) {
            $usuario = "{$usuario_zpanel_usuario}_{$usuario}";
            $senha = (!$senha ? knife::generate_random_password(9, 4) : $senha);
            if (!self::verificar($usuario)) {
                $zpanel_id = zpanel_mysql_usuario::cadastrar($usuario, $senha, $acesso, $mysql_zpanel_id, $usuario_zpanel_id);
                if ($zpanel_id) {
                    $db = new mysqlsave();
                    $db->table("mysql_usuario");
                    $db->column("usuario", $usuario);
                    $db->column("senha", $senha);
                    $db->column("acesso", $acesso);
                    $db->column("usuario_id", $usuario_id);
                    $db->column("zpanel_id", $zpanel_id[0]);
                    $db->column("cadastrado", date("Y-m-d H:i:s"));
                    $db->column("atualizado", date("Y-m-d H:i:s"));
                    if ($db->go()) {
                        $mysql_usuario_id = $db->id();
                        $db = new mysqlsave();
                        $db->table("mysql_usuario_associacao");
                        $db->column("mysql_usuario_id", $mysql_usuario_id);
                        $db->column("mysql_id", $mysql_id);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id[1]);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            return $mysql_usuario_id;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function atualizar($id, $senha) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_mysql_usuario::atualizar($zpanel_id, $senha)) {
                $db = new mysqlsave();
                $db->table("mysql_usuario");
                if ($senha) {
                    $db->column("senha", $senha);
                }
                $db->column("atualizado", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_mysql_usuario::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("mysql_usuario");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                if ($db->go()) {
                    self::remover_relacionados($id);
                    return true;
                }
            }
        }
        return false;
    }

    private static function remover_relacionados($id) {
        // remove associacao com usuarios do banco de dados
        $db = new mysqlsave();
        $db->table("mysql_usuario_associacao");
        $db->column("removido", date("Y-m-d H:i:s"));
        $db->match("mysql_usuario_id", $id);
        $db->go();
    }

    public static function verificar($usuario) {
        $db = new mysqlsearch();
        $db->table("mysql_usuario");
        $db->column("id");
        $db->match("usuario", $usuario);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}