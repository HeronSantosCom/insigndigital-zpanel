<?php
/**
 * Main ZPM script for The Hosting Tool Admin Module for ZPanel 10+
 * Version : 100
 * Author :  TGates
 * Email :  tgates@mach-hosting.com
 */
class module_controller {

    static function getModuleName() {
        $module_name = ui_module::GetModuleName();
        return $module_name;
    }

    static function getLaunchTHTClient() {
        $message = ui_language::translate("Open in New Window");
        return $message;
    }

    static function getModuleDesc() {
        $message = ui_language::translate("THT Client is a small module for clients to easily access The Hosting Tool client web interface.");
        return $message;
    }

    static function getModuleIcon() {
        global $controller;
        $module_icon = "modules/" . $controller->GetControllerRequest('URL', 'module') . "/assets/icon.png";
        return $module_icon;
    }

    static function getthtLoader() {
		
		// START CONFIG URL
		$thtUrl = 'billing.mach-hosting.com';
		// END CONFIG URL
		
        $thtloader = '<iframe id="myFrame" scrolling="auto" height="640" width="100%" frameborder="0" src="http://'.$thtUrl.'/client/"></iframe>';
        return $thtloader;
    }
}
?>