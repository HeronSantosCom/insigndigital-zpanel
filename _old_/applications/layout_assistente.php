<?php

class layout_assistente extends main {

    private $crumbs = array("dominio", "ftp", "mysql", "email", "resumo", "concluido");

    public function __construct() {
        $assistente = logon::meu_assistente();
        if ($assistente) {
            return knife::redirect("/index.html");
        }
        $this->crumb = $this->crumb();
        $this->{$this->crumb}();
        $this->post($this->crumb);
        $this->proximo();
//        if (!empty($_SESSION["assistente"]))
//            knife::dump($_SESSION["assistente"]);
        $this->interno = knife::html("layout/main/assistente/{$this->crumb}.html");
    }

    private function inicio() {
        if (!empty($_POST["dominio"])) {
            if (substr($_POST["dominio"], 0, 4) == "www.") {
                $_POST["dominio"] = substr($_POST["dominio"], 4);
            }
            $_SESSION["assistente"]["dominio_endereco"] = $_POST["dominio"];
        }
    }

    private function dominio() {
        $this->www = "1";
    }

    private function ftp() {
        $this->autohome = "2";
        $this->tipo_acesso_id = "3";
        $this->zpanel_usuario = logon::meu_zpanel_usuario();
        $this->diretorios = dao_ftp::diretorios(logon::meu_id());
        $this->tipo_acesso = dao_tipo_acesso::listar();
        if (empty($_POST["senha"])) {
            $_POST["senha"] = (!empty($_SESSION["assistente"]["ftp"]["senha"]) ? $_SESSION["assistente"]["ftp"]["senha"] : false);
        }
    }

    private function mysql() {
        $this->zpanel_usuario = logon::meu_zpanel_usuario();
        $this->acesso_tipo = "1";
        if (empty($_POST["senha"])) {
            $_POST["senha"] = (!empty($_SESSION["assistente"]["ftp"]["senha"]) ? $_SESSION["assistente"]["ftp"]["senha"] : false);
        }
    }

    private function resumo() {
        $this->zpanel_usuario = logon::meu_zpanel_usuario();
        foreach ($this->crumbs as $crumb) {
            if (!empty($_SESSION["assistente"][$crumb])) {
                $this->extract($_SESSION["assistente"][$crumb], $crumb);
            }
        }
    }

    private function concluido() {
        $this->session_msgbox("Ocorreu um erro ao salvar suas informações definidas no Assistente, verifique os dados ou tente novamente mais tarde!");
        $dominio_id = false;
        if (!empty($_SESSION["assistente"]["dominio"])) {
            // cadastra domínio
            $this->extract($_SESSION["assistente"]["dominio"], "dominio");
            $dominio_id = dao_dominio::cadastrar($this->dominio_dominio, false, $this->dominio_www, ($this->dominio_redirect ? $this->dominio_redirecionar : false), $this->dominio_estacionado, logon::meu_id());
            if ($dominio_id) {
                $proximo_passo = true;

                //cadastra ftp
                if (!empty($_SESSION["assistente"]["ftp"])) {
                    $this->extract($_SESSION["assistente"]["ftp"], "ftp");
                    if ($this->ftp_usuario) {
                        $ftp_id = dao_ftp::cadastrar($this->ftp_usuario, $this->ftp_senha, ($this->ftp_autohome == "2" ? '/' : ($this->ftp_autohome == "3" ? $this->ftp_diretorio : false)), $this->ftp_tipo_acesso_id, logon::meu_id());
                        if (!$ftp_id) {
                            $proximo_passo = false;
                        }
                    }
                }

                // cadastra mysql
                if ($proximo_passo && !empty($_SESSION["assistente"]["mysql"])) {
                    $this->extract($_SESSION["assistente"]["mysql"], "mysql");
                    if ($this->mysql_nome) {
                        $mysql_id = dao_mysql::cadastrar($this->mysql_nome, logon::meu_id());
                        if ($mysql_id) {
                            // cadastra usuario mysql
                            if ($this->mysql_usuario) {
                                $mysql_usuario_id = dao_mysql_usuario::cadastrar($this->mysql_usuario, $this->mysql_senha, ($this->mysql_acesso_tipo == "1" ? "%" : $this->mysql_acesso), $mysql_id, logon::meu_id());
                                if (!$mysql_usuario_id) {
                                    $proximo_passo = false;
                                }
                            }
                        } else {
                            $proximo_passo = false;
                        }
                    }
                }

                // cadastra email
                if ($proximo_passo && !empty($_SESSION["assistente"]["email"])) {
                    $this->extract($_SESSION["assistente"]["email"], "email");
                    if ($this->email_endereco) {
                        $email_id = dao_email::cadastrar($this->email_endereco, $this->email_senha, $dominio_id, logon::meu_id());
                        if ($email_id) {
                            // cadastra retirecionamento
                            if ($this->email_redirect) {
                                $redirecionamento_email_id = dao_redirecionamento_email::cadastrar($this->email_redirecionamento_endereco, $this->email_redirecionamento_original, $email_id, logon::meu_id());
                                if (!$redirecionamento_email_id) {
                                    $proximo_passo = false;
                                }
                            }
                        } else {
                            $proximo_passo = false;
                        }
                    }
                }


                if (!$proximo_passo) {
                    if (!empty($email_id)) {
                        dao_email::remover($email_id);
                    }
                    if (!empty($mysql_usuario_id)) {
                        dao_mysql_usuario::remover($mysql_usuario_id);
                    }
                    if (!empty($mysql_id)) {
                        dao_mysql::remover($mysql_id);
                    }
                    if (!empty($ftp_id)) {
                        dao_ftp::remover($ftp_id);
                    }
                    if (!empty($dominio_id)) {
                        dao_dominio::remover($dominio_id);
                    }
                } else {
                    unset($_SESSION["assistente"]);
                    dao_usuario::definir_assistencia(logon::meu_id());
                    $this->session_msgbox("Salvar suas informações definidas no Assistente foram salvas com sucesso, em alguns instantes seu site estará disponível!");
                    logon::reboot(logon::meu_id(), false, false, (self::cookie() ? '1' : '0'));
                    return knife::redirect("/index.html");
                }
            }
        }
        return knife::redirect("/index.html?m=assistente");
    }

    private function crumb() {
        foreach ($this->crumbs as $crumb) {
            if (isset($_GET[$crumb])) {
                if (!empty($_SESSION["assistente"]["dominio_endereco"])) {
                    $this->dominio_endereco = $_SESSION["assistente"]["dominio_endereco"];
                    return $crumb;
                }
                $this->msgbox("É necessário iniciar o assistente através do primeiro passo!");
                break;
            }
        }
        return "inicio";
    }

    private function proximo() {
        if (!empty($_POST["proximo_passo"])) {
            return knife::redirect("/index.html?m=assistente&{$_POST["proximo_passo"]}");
        }
        return false;
    }

    private function post($crumb) {
        $crumb = ($crumb == "inicio" ? "dominio" : $crumb);
        if (!empty($_POST["proximo_passo"])) {
            $_SESSION["assistente"][$crumb] = $_POST;
        }
        if (!empty($_SESSION["assistente"][$crumb])) {
            $this->extract($_SESSION["assistente"][$crumb]);
        }
        return false;
    }

    public function __call($name, $arguments) {
        return false;
    }

}