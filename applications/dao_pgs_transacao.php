<?php

class dao_pgs_transacao {

    public static function listar($crp_transacao_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("pgs_transacao");
        $db->column("*");
        $db->match("crp_transacao_id", $crp_transacao_id);
        $db->order("cadastrado", "ASC");
        $dao = $db->go();
        if ($dao) {
            $dao_pgs_tipo_cancelamento = dao_pgs_tipo_cancelamento::listar();
            $dao_pgs_tipo_status = dao_pgs_tipo_status::listar();
            $dao_pgs_tipo_transacao = dao_pgs_tipo_transacao::listar();
            $dao_pgs_tipo_meio_pagamento = dao_pgs_tipo_meio_pagamento::listar();
            foreach ($dao as $row) {
                $pgs_tipo_cancelamento = (!empty($dao_pgs_tipo_cancelamento[$row["pgs_tipo_cancelamento_id"]]) ? $dao_pgs_tipo_cancelamento[$row["pgs_tipo_cancelamento_id"]] : false);
                $pgs_tipo_status = (!empty($dao_pgs_tipo_status[$row["pgs_tipo_status_id"]]) ? $dao_pgs_tipo_status[$row["pgs_tipo_status_id"]] : false);
                $pgs_tipo_transacao = (!empty($dao_pgs_tipo_transacao[$row["pgs_tipo_transacao_id"]]) ? $dao_pgs_tipo_transacao[$row["pgs_tipo_transacao_id"]] : false);
                $pgs_tipo_meio_pagamento = (!empty($dao_pgs_tipo_meio_pagamento[$row["pgs_tipo_meio_pagamento_id"]]) ? $dao_pgs_tipo_meio_pagamento[$row["pgs_tipo_meio_pagamento_id"]] : false);
                $array[$row["id"]] = self::hook($row, $pgs_tipo_cancelamento, $pgs_tipo_status, $pgs_tipo_transacao, $pgs_tipo_meio_pagamento);
            }
        }
        return $array;
    }

    private static function hook($row, $pgs_tipo_cancelamento, $pgs_tipo_status, $pgs_tipo_transacao, $pgs_tipo_meio_pagamento) {
        $row["cadastrado_br"] = date("d/m/Y H:i", strtotime($row["cadastrado"]));
        if ($pgs_tipo_cancelamento) {
            foreach ($pgs_tipo_cancelamento as $key => $value) {
                $row["pgs_tipo_cancelamento_{$key}"] = $value;
            }
        }
        if ($pgs_tipo_status) {
            foreach ($pgs_tipo_status as $key => $value) {
                $row["pgs_tipo_status_{$key}"] = $value;
            }
        }
        if ($pgs_tipo_transacao) {
            foreach ($pgs_tipo_transacao as $key => $value) {
                $row["pgs_tipo_transacao_{$key}"] = $value;
            }
        }
        if ($pgs_tipo_meio_pagamento) {
            foreach ($pgs_tipo_meio_pagamento as $key => $value) {
                $row["pgs_tipo_meio_pagamento_{$key}"] = $value;
            }
        }
        return $row;
    }

    public static function cadastrar($codigo, $parcelas, $pgs_tipo_cancelamento_id, $pgs_tipo_status_id, $pgs_tipo_transacao_id, $pgs_tipo_meio_pagamento_id, $crp_transacao_id) {
        $db = new mysqlsave();
        $db->table("pgs_transacao");
        $db->column("codigo", $codigo);
        $db->column("parcelas", $parcelas);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        $db->column("pgs_tipo_cancelamento_id", $pgs_tipo_cancelamento_id);
        $db->column("pgs_tipo_status_id", $pgs_tipo_status_id);
        $db->column("pgs_tipo_transacao_id", $pgs_tipo_transacao_id);
        $db->column("pgs_tipo_meio_pagamento_id", $pgs_tipo_meio_pagamento_id);
        $db->column("crp_transacao_id", $crp_transacao_id);
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

}