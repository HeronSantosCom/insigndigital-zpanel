<?php

class dao_chamado_interacao {

    public static function listar($chamado_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("chamado_interacao");
        $db->join("usuario", array("usuario_id", "=", "id"), "LEFT");
        $db->column("*");
        $db->column("nome", 2, "usuario_nome");
        $db->column("zpanel_usuario", 2, "usuario_zpanel_usuario");
        $db->match("chamado_id", $chamado_id);
        $db->order("cadastrado", "ASC");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["resposta"] = nl2br($row["resposta"]);
        $row["cadastrado_br"] = date("d/m/Y H:i", strtotime($row["cadastrado"]));
        if ($row["lido"]) {
            $row["lido_br"] = date("d/m/Y H:i", strtotime($row["lido"]));
        }
        return $row;
    }

    public static function cadastrar($resposta, $chamado_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("chamado_interacao");
        $db->column("resposta", $resposta);
        $db->column("chamado_id", $chamado_id);
        $db->column("usuario_id", $usuario_id);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        $db->column("atualizado", date("Y-m-d H:i:s"));
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function atualizar($chamado_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("chamado");
        $db->column("lido", date("Y-m-d H:i:s"));
        $db->column("atualizado", date("Y-m-d H:i:s"));
        $db->match("chamado_id", $chamado_id);
        $db->match("usuario_id", $usuario_id, false, true);
        return $db->go();
    }

}