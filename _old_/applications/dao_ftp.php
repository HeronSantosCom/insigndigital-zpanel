<?php

if (!class_exists("zpanel_ftp", false)) {
    include path::applications("zpanel/ftp.php");
}

class dao_ftp {

    public static function pegar($id, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("ftp");
        $db->column("*");
        $db->match("id", $id);
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            $tipo_acesso = dao_tipo_acesso::pegar($dao[0]["tipo_acesso_id"]);
            return self::hook($dao[0], $tipo_acesso);
        }
        return false;
    }

    public static function listar($usuario_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("ftp");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        $db->order("usuario");
        $dao = $db->go();
        if ($dao) {
            $dao_tipo_acesso = dao_tipo_acesso::listar();
            foreach ($dao as $row) {
                $tipo_acesso = (!empty($dao_tipo_acesso[$row["tipo_acesso_id"]]) ? $dao_tipo_acesso[$row["tipo_acesso_id"]] : false);
                $array[$row["id"]] = self::hook($row, $tipo_acesso);
            }
        }
        return $array;
    }

    private static function hook($row, $tipo_acesso) {
        $row["tipo_acesso_nome"] = (!empty($tipo_acesso["nome"]) ? $tipo_acesso["nome"] : false);
        return $row;
    }

    public static function cadastrar($usuario, $senha, $diretorio, $tipo_acesso_id, $usuario_id) {
        if (dao_modulo::zpanel_disponivel("ftp", $usuario_id)) {
            $usuario_zpanel_usuario = dao_usuario::zpanel_usuario($usuario_id);
            $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
            $tipo_acesso_zpanel_id = dao_tipo_acesso::zpanel_id($tipo_acesso_id);
            if ($usuario_zpanel_id && $usuario_zpanel_usuario) {
                $usuario = "{$usuario_zpanel_usuario}_{$usuario}";
                if (!self::verificar($usuario)) {
                    $zpanel_id = zpanel_ftp::cadastrar($usuario, $senha, $diretorio, $tipo_acesso_zpanel_id, $usuario_zpanel_id);
                    if ($zpanel_id) {
                        $diretorio = ($diretorio ? $diretorio : ("/" . str_replace(".", "_", $usuario)));
                        $db = new mysqlsave();
                        $db->table("ftp");
                        $db->column("usuario", $usuario);
                        $db->column("senha", $senha);
                        $db->column("diretorio", $diretorio);
                        $db->column("tipo_acesso_id", $tipo_acesso_id);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            return $db->id();
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function atualizar($id, $senha) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_ftp::atualizar($zpanel_id, $senha)) {
                $db = new mysqlsave();
                $db->table("ftp");
                if ($senha) {
                    $db->column("senha", $senha);
                }
                $db->column("atualizado", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function remover($id) {
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            if (zpanel_ftp::remover($zpanel_id)) {
                $db = new mysqlsave();
                $db->table("ftp");
                $db->column("removido", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function diretorios($usuario_id) {
        $zpanel_id = dao_usuario::zpanel_id($usuario_id);
        if ($zpanel_id) {
            return zpanel_ftp::diretorios($zpanel_id);
        }
        return false;
    }

    public static function verificar($usuario) {
        $db = new mysqlsearch();
        $db->table("ftp");
        $db->column("id");
        $db->match("usuario", $usuario);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}