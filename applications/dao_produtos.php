<?php

if (!class_exists("zpanel_plano", false)) {
    include path::applications("zpanel/plano.php");
}

class dao_produtos {

    public static function pegar_plano($id) {
        $dao = dao_plano::pegar($id);
        if ($dao) {
            $dao = self::hook($dao, false);
        }
        return $dao;
    }

    public static function listar_planos($limite = false, $usuario_id = 1) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("plano");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->is("removido", false);
        if ($limite) {
            $db->limit($limite);
        }
        $db->order("valor");
        $dao = $db->go();
        if ($dao) {
            $_ENV["destaque"] = array(false, false);
            $zpanel_dao = zpanel_plano::listar(dao_usuario::zpanel_id(1));
            foreach ($dao as $row) {
                $zpanel = (!empty($zpanel_dao[$row["zpanel_id"]]) ? $zpanel_dao[$row["zpanel_id"]] : false);
                $array[$row["id"]] = self::hook($row, $zpanel);
            }
            if ($_ENV["destaque"][1] > 0) {
                $array[$_ENV["destaque"][1]]["destaque"] = "orange";
            }
        }
        return $array;
    }

    private static function hook($row, $zpanel) {
        $row["destaque"] = false;
        $row["valor_br"] = number_format($row["valor"], 2, ',', '.');
        $row["valor_arredondado"] = ceil($row["valor"]);

        $row["total_quota_disco"] = ($row["total_quota_disco"] / 1000);
        $row["total_quota_transferencia"] = ($row["total_quota_transferencia"] / 1000);

        $row["total_usuarios"] = (!empty($zpanel["clients"]) ? $zpanel["clients"] : 0);
        if (isset($_ENV["destaque"])) {
            if ($row["total_usuarios"] > $_ENV["destaque"][0]) {
                $_ENV["destaque"] = array($row["total_usuarios"], $row["id"]);
            }
        }
        return $row;
    }

}