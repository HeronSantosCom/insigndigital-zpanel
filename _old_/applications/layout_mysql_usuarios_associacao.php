<?php

class layout_mysql_usuarios_associacao extends main {

    public function __construct() {
        if (isset($_GET["parent_id"])) {
            $this->extract($_GET);
            $mysql_usuario = dao_mysql_usuario::pegar($this->parent_id, logon::meu_id());
            if ($mysql_usuario) {
                $this->extract($mysql_usuario, "mysql_usuario");

                if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
                    $this->formulario = true;
                    if (!empty($_GET["id"])) {
                        if (!$this->abrir($_GET["id"])) {
                            define("app_layout_error", true);
                            return false;
                        }
                    }
                    if (isset($_GET["remover"])) {
                        $this->remover = true;
                    }
                    if (isset($_POST["id"])) {
                        $this->salvar($_POST["id"]);
                    }
                }

                $this->mysql_usuarios_associacao = dao_mysql_usuario_associacao::listar(logon::meu_id(), $this->mysql_usuario_id);
                $this->mysql = dao_mysql::listar(logon::meu_id(), $this->mysql_usuario_id);
                return true;
            }
        }
        define("app_layout_error", true);
    }

    private function abrir($id) {
        $dao = (dao_mysql_usuario_associacao::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar a associação do usuário com o banco de dados, verifique os dados e tente novamente!");
            $action = dao_mysql_usuario_associacao::cadastrar($this->mysql_id, $this->mysql_usuario_id, logon::meu_id());
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Associação do usuário com o banco de dados salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover a associação do usuário com o banco de dados, tente novamente!");
        if ($id) {
            $action = dao_mysql_usuario_associacao::remover($id, $this->mysql_usuario_id);
            if ($action) {
                $this->msgbox("Associação do usuário com o banco de dados removido com sucesso!");
                unset($this->formulario);
            }
        }
    }

}