<?php

class dao_chamado {

    public static function pegar($id, $usuario_id, $responsavel_usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("chamado");
        $db->join("usuario", array("usuario_id", "=", "id"), "LEFT");
        $db->column("*");
        $db->column("nome", 2, "usuario_nome");
        $db->column("email", 2, "usuario_email");
        $db->column("zpanel_usuario", 2, "usuario_zpanel_usuario");
        $db->match("id", $id);
        if ($responsavel_usuario_id) {
            $db->match("responsavel_usuario_id", $responsavel_usuario_id);
        } else {
            $db->match("usuario_id", $usuario_id);
        }
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $responsavel_usuario_id = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("chamado");
        $db->join("usuario", array("usuario_id", "=", "id"), "LEFT");
        $db->column("*");
        $db->column("nome", 2, "usuario_nome");
        $db->column("email", 2, "usuario_email");
        $db->column("zpanel_usuario", 2, "usuario_zpanel_usuario");
        if ($responsavel_usuario_id) {
            $db->match("responsavel_usuario_id", $responsavel_usuario_id);
        } else {
            $db->match("usuario_id", $usuario_id);
        }
        $db->order("status", "ASC");
        $db->order("cadastrado", "DESC");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        switch ($row["status"]) {
            case "4":
                $row["status_titulo"] = "Encerrado.";
                $row["status_cor"] = "label-inverse";
                $row["status_icone"] = "icon-ban-circle";
                break;
            case "3":
                $row["status_titulo"] = "Solucionada.";
                $row["status_cor"] = "label-success";
                $row["status_icone"] = "icon-check";
                break;
            case "2":
                $row["status_titulo"] = "Aguardando resposta do cliente.";
                $row["status_cor"] = "label-warning";
                $row["status_icone"] = "icon-edit";
                break;
            case "1":
            default:
                $row["status_titulo"] = "Aguardando resposta do suporte.";
                $row["status_cor"] = "label-important";
                $row["status_icone"] = "icon-share";
                break;
        }
        return $row;
    }

    public static function cadastrar($titulo, $questao, $usuario_id, $responsavel_usuario_id) {
        $db = new mysqlsave();
        $db->table("chamado");
        $db->column("titulo", $titulo);
        $db->column("questao", $questao);
        $db->column("usuario_id", $usuario_id);
        $db->column("responsavel_usuario_id", $responsavel_usuario_id);
        $db->column("cadastrado", date("Y-m-d H:i:s"));
        $db->column("atualizado", date("Y-m-d H:i:s"));
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function atualizar($id, $status) {
        $db = new mysqlsave();
        $db->table("chamado");
        $db->column("status", $status);
        $db->column("atualizado", date("Y-m-d H:i:s"));
        $db->match("id", $id);
        return $db->go();
    }

}