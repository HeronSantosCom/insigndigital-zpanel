<?php

class _cobranca {

    public function __construct() {
        define("TIMENOW", time());
//        define("TIMENOW", strtotime("2013-05-03 03:50:00"));
        // define a pid
        $this->pid = getmypid();
        // define calendario debito
        $this->deb_date = TIMENOW;
        $this->deb_m = date("m", $this->deb_date);
        $this->deb_a = date("Y", $this->deb_date);
        $this->deb_last_d = cal_days_in_month(CAL_GREGORIAN, $this->deb_m, $this->deb_a);
        // define calendario debito
        $this->cred_date = strtotime("-1 day", TIMENOW);
        $this->cred_m = date("m", $this->cred_date);
        $this->cred_a = date("Y", $this->cred_date);
        $this->cred_last_d = cal_days_in_month(CAL_GREGORIAN, $this->cred_m, $this->cred_a);
        // define calendario para faturamento
        $this->fat_date = strtotime("-1 month", TIMENOW);
        $this->fat_first_d = '01';
        $this->fat_m = date("m", $this->fat_date);
        $this->fat_a = date("Y", $this->fat_date);
        $this->fat_last_d = cal_days_in_month(CAL_GREGORIAN, $this->fat_m, $this->fat_a);
        $this->fat_first_date = strtotime("{$this->fat_a}-{$this->fat_m}-{$this->fat_first_d} 00:00:00");
        $this->fat_last_date = strtotime("{$this->fat_a}-{$this->fat_m}-{$this->fat_last_d} 23:59:59");
        // lista todos os usuários ativos
        $usuarios = $this->pega_usuarios();
        if ($usuarios) {
            $size = sizeof($usuarios);
            $keys = array_keys($usuarios);
            $servidor = false;
            $servidor_id = false;
            $plano = false;
            $plano_id = false;
            for ($y = 0; $y < $size; $y++) {
                $key = $keys[$y];
                $usuario = $usuarios[$key];
                // conecta ao usuário que não é adminitrador do servidor e seja assinante
                // descomentar para efetuar testes
//                if ($usuario["id"] == "6") {
//                    $usuario["usuario_id"] = 16;
                if ($usuario["id"] != $usuario["usuario_id"] && $usuario["plano_id"]) {
                    $servidor = ($servidor_id != $usuario["servidor_id"] ? dao_servidor::pegar($usuario["servidor_id"]) : $servidor);
                    $servidor_id = $usuario["servidor_id"];
                    $usuario["servidor"] = $servidor;
                    $plano = ($plano_id != $usuario["plano_id"] ? dao_plano::pegar($usuario["plano_id"]) : $plano);
                    $plano_id = $usuario["plano_id"];
                    $usuario["plano"] = $plano;
                    $this->conectar_usuario($usuario);
                }
//                }
                $this->atualiza_saldo($usuario);
                unset($key, $usuario);
            }
            unset($size, $keys, $usuarios);
        }
    }

    private function logger($id, $titulo, $descricao, $status) {
        print "{$id} // {$titulo} // {$descricao} // " . ($status ? "OK" : "FALHOU") . "\n";
    }

    private function pega_usuarios() {
        if (logon::reboot(1, false, false, false)) {
            return dao_usuario::listar(false, true, array("servidor_id", "plano_id", "nome"));
        }
        return false;
    }

    private function conectar_usuario($usuario) {
        // conecta ao administrador da conta do cliente
        $session = logon::reboot($usuario["usuario_id"], false, false, false);
        if ($session) {
//            // debitar saldo do dia
//            $this->corrigir_faturas($usuario);
            // debitar saldo do dia
            $this->debitar_saldo_dia($usuario);
            // creditar saldo do dia
            $this->creditar_saldo_dia($usuario);
            // faturar saldo ciclo mensal
            $this->faturar($usuario);
            // bloquear
            $this->bloquear($usuario);
            // desbloquear
            $this->desbloquear($usuario);
        }
    }

    private function debitar_saldo_dia($usuario) {
        // verifica se já foi feito o débito de hoje e se não foi cadastra
        $hoje = crp_transacao::debito_hoje($usuario["id"], date("Y-m-d", $this->deb_date), 200);
        if (!$hoje) {
            $debido_descricao = "Assinatura de Hospedagem {$usuario["plano"]["nome"]}";
            $debido_valor = ($usuario["plano"]["valor"] / $this->deb_last_d);
            $action = crp_transacao::cadastrar($this->pid, $debido_descricao, $debido_valor, date("Y-m-d H:i:s", $this->deb_date), 200, false, $usuario["id"]);
            $this->logger($usuario["id"], "DEBITO DIA", "{$debido_descricao}: {$debido_valor}", $action);
            return $action;
        }
        return false;
    }

    private function creditar_saldo_dia($usuario) {
        // verifica se a faturas pagas que ainda não foram creditadas.
        $cobrancas = dao_cobranca::listar($usuario["id"], false, false, false, "0000-00-00 00:00:00", 3);
        if ($cobrancas) {
            foreach ($cobrancas as $cobranca) {
                $credito_descricao = "Pagamento Confirmado ({$cobranca["referencia"]})";
                $credito_valor = (-1 * $cobranca["valor"]);
                $action = crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", $this->cred_date), 100, false, $usuario["id"]);
                $this->logger($usuario["id"], "CRÉDITO PAGAMENTO", "{$credito_descricao}: {$credito_valor}", $action);
                // atualiza cobranca para não haver créditos duplicados
                if ($action) {
                    $action = dao_cobranca::atualizar($cobranca["id"], $cobranca["referencia"], date("Y-m-d H:i:s", $this->cred_date), $cobranca["crp_tipo_pagamento_id"], $cobranca["crp_tipo_pagamento_status_id"]);
                    if ($action) {
                        // credita porcentagem referente ao revendedor
                        $credito_revenda_saldo_id = 102;
                        $credito_revenda_descricao = "Comissão 30% ({$cobranca["referencia"]})";
                        $credito_revenda_valor = (-1 * $cobranca["valor"]); // adminitradores do sistema recebem valor integral em forma de créditos
                        if ($usuario["usuario_id"] != $usuario["servidor"]["usuario_id"]) { // caso contrario recebem 30% do valor creditado
                            $credito_revenda_saldo_id = 101;
                            $credito_revenda_descricao = "Comissão 100% ({$cobranca["referencia"]})";
                            $credito_revenda_valor = (-1 * ($cobranca["valor"] * 0.3));
                        }
                        $action = crp_transacao::cadastrar($this->pid, $credito_revenda_descricao, $credito_revenda_valor, date("Y-m-d H:i:s", $this->cred_date), $credito_revenda_saldo_id, false, $usuario["usuario_id"]);
                        $this->logger($usuario["usuario_id"], "CRÉDITO PAGAMENTO CLIENTE", "{$usuario["id"]} - {$credito_revenda_descricao}: {$credito_revenda_valor}", $action);
                        // notifica cliente e revendedor de pagamento efetuado
                        if ($action) {
                            $revenda = dao_usuario::pegar($usuario["usuario_id"]);
                            snr::crpuc($usuario["nome"], $usuario["email"], $usuario["telefone"], $credito_descricao, $credito_valor);
                            snr::crpcc($revenda["nome"], $revenda["email"], $revenda["telefone"], $credito_revenda_descricao, $credito_revenda_valor);
                        }
                    }
                }
            }
        }
    }

    private function faturar($usuario) {
        $periodo = array(date("Y-m-d", $this->fat_first_date), date("Y-m-d", $this->fat_last_date));
        // verifica se não há cobrancas no periodo mensal
        $cobrancas = dao_cobranca::fatura_mes($usuario["id"], $periodo);
        if (!$cobrancas) {
            // carrega o saldo do periodo
            $saldos = crp_transacao::listar($usuario["id"], false, 200, $periodo);
            if ($saldos) {
                // cadastra cobranca
                $valor = 0.00000;
                $codigo = substr(sha1(uniqid(date("r", $this->fat_last_date) . $usuario["id"] . mt_rand(), true)), 0, 8);
                $cobranca_id = dao_cobranca::cadastrar($codigo, false, date("Y-m-d H:i:s", TIMENOW), date("Y-m-d H:i:s", $this->fat_last_date), date("Y-m-d H:i:s", strtotime("+7 days", TIMENOW)), false, 1, $usuario["id"]);
                if ($cobranca_id) {
                    $saldo = crp_transacao::saldo($usuario["id"]);
                    foreach ($saldos as $saldo_id => $saldo) {
                        $valor += $saldo["valor"];
                        crp_transacao::atualizar($saldo_id, $cobranca_id);
                    }
                    // registrar fatura em saldo
                    $credito_descricao = "Fatura Registrada ({$codigo})";
                    $credito_valor = $valor;
                    $action = crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", TIMENOW), 400, false, $usuario["id"]);
                    if ($action) {
                        // notificar existencia de cobranca
                        $periodo = date("d/m/Y", $this->fat_first_date) . " até " . date("d/m/Y", $this->fat_last_date);
                        snr::cobranca_lancamento($usuario["nome"], $usuario["email"], $usuario["telefone"], $codigo, number_format($valor, 2, ',', '.'), $periodo, date("d/m/Y", strtotime("+7 days", TIMENOW)));
                        if ($valor <= 0) {
                            // anula fatura com valor igual ou inferior a zero
                            $action = dao_cobranca::atualizar($cobranca_id, false, date("Y-m-d H:i:s", TIMENOW), 6, 5);
                            if ($action) {
                                $credito_descricao = "Reajuste de Fatura ({$codigo})";
                                $credito_valor = $valor;
                                $action = crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", TIMENOW), 302, false, $usuario["id"]);
                            }
                        } else {
                            // efetua pagamento automaticamente do saldo
                            if ($saldo >= $valor) {
                                $credito_descricao = "Débito Automático do Saldo ({$codigo})";
                                $credito_valor = $valor;
                                $action = crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", TIMENOW), 203, false, $usuario["id"]);
                                if ($action) {
                                    $credito_descricao = "Pagamento Confirmado ({$codigo})";
                                    $credito_valor = (-1 * $valor);
                                    $action = crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", TIMENOW), 100, false, $usuario["id"]);
                                    if ($action) {
                                        $action = dao_cobranca::atualizar($cobranca_id, false, date("Y-m-d H:i:s", TIMENOW), 6, 3);
                                    }
                                }
                            }
                        }
                    }
                }
                $this->logger($usuario["id"], "FATURAMENTO CICLO MENSAL", "Nova cobrança {$this->fat_m}/{$this->fat_a}: {$valor}", $cobranca_id);
            }
        }
        return false;
    }

    private function bloquear($usuario) {
        if ($usuario["motivo"] != "Existe(m) fatura(s) em atraso.") {
            $cobrancas = dao_cobranca::listar($usuario["id"], false, false, false, false, 1);
            if ($cobrancas) {
                foreach ($cobrancas as $cobranca) {
                    if (time() > strtotime($cobranca["vencimento"])) {
                        $action = dao_usuario::atualizar($usuario["id"], $usuario["nome"], $usuario["email"], false, $usuario["telefone"], 1, "Existe(m) fatura(s) em atraso.", $usuario["notificacao_sms"], $usuario["notificacao_email"], $usuario["plano_id"], $usuario["grupo_id"]);
                        if ($action) {
                            snr::ub($usuario["nome"], $usuario["email"], $usuario["telefone"], "Existe(m) fatura(s) em atraso.");
                        }
                        $this->logger($usuario["id"], "BLOQUEIO POR FATURA EM ATRASO", "{$usuario["nome"]}, cobrança vencida em {$cobranca["vencimento"]}", $action);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private function desbloquear($usuario) {
        if ($usuario["motivo"] == "Existe(m) fatura(s) em atraso.") {
            $cobrancas = dao_cobranca::listar($usuario["id"], false, false, false, false, 1);
            if ($cobrancas) {
                foreach ($cobrancas as $cobranca) {
                    if (time() > strtotime($cobranca["vencimento"])) {
                        return false;
                    }
                }
            }
            $action = dao_usuario::atualizar($usuario["id"], $usuario["nome"], $usuario["email"], false, $usuario["telefone"], 0, false, $usuario["notificacao_sms"], $usuario["notificacao_email"], $usuario["plano_id"], $usuario["grupo_id"]);
            if ($action) {
                snr::ud($usuario["nome"], $usuario["email"], $usuario["telefone"]);
            }
            $this->logger($usuario["id"], "DESBLOQUEIO POR FATURAMENTO EM DIA", "{$usuario["nome"]}", $action);
        }
        return true;
    }

    private function atualiza_saldo($usuario) {
        $saldo = crp_transacao::saldo($usuario["id"]);
        if ($saldo) {
            dao_usuario::atualizar_saldo($usuario["id"], $saldo);
        }
    }

    private function corrigir_faturas($usuario) {
        $cobrancas = dao_cobranca::listar($usuario["id"]);
        if ($cobrancas) {
            foreach ($cobrancas as $cobranca) {
                $data_lancamento_cobranca = strtotime($cobranca["lancamento"]);
                $fat_date = strtotime("-1 month", $data_lancamento_cobranca);
                $fat_first_d = '01';
                $fat_m = date("m", $fat_date);
                $fat_a = date("Y", $fat_date);
                $fat_last_d = cal_days_in_month(CAL_GREGORIAN, $fat_m, $fat_a);
                $fat_first_date = strtotime("{$fat_a}-{$fat_m}-{$fat_first_d} 00:00:00");
                $fat_last_date = strtotime("{$fat_a}-{$fat_m}-{$fat_last_d} 23:59:59");
                $valor = 0.00000;
                // carrega o saldo do periodo
                $saldos = crp_transacao::listar($usuario["id"], false, 200, array(date("Y-m-d", $fat_first_date), date("Y-m-d", $fat_last_date)));
                if ($saldos) {
                    foreach ($saldos as $saldo_id => $saldo) {
                        $valor += $saldo["valor"];
                    }
                }

                $credito_descricao = "Fatura Registrada ({$cobranca["codigo"]})";
                $credito_valor = $valor;
                crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", $data_lancamento_cobranca), 400, false, $usuario["id"]);
                if ($valor <= 2.99999) {
                    $credito_descricao = "Reajuste de Fatura ({$cobranca["codigo"]})";
                    $credito_valor = (-1 * $valor);
                    crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", $data_lancamento_cobranca), 302, false, $usuario["id"]);
                } else {
                    if ($cobranca["creditado"] != "0000-00-00 00:00:00") {
                        $data_creditado_cobranca = strtotime($cobranca["creditado"]);
                        if ($cobranca["crp_tipo_pagamento_status_id"] == 5) {
                            $credito_descricao = "Reajuste por Crédito Concedido ({$cobranca["codigo"]})";
                            $credito_valor = (-1 * $valor);
                            crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", $data_creditado_cobranca), 300, false, $usuario["id"]);
                        } else {
                            $credito_descricao = "Pagamento Confirmado ({$cobranca["referencia"]})";
                            $credito_valor = (-1 * $cobranca["valor"]);
                            crp_transacao::cadastrar($this->pid, $credito_descricao, $credito_valor, date("Y-m-d H:i:s", $data_creditado_cobranca), 100, false, $usuario["id"]);
                            // credita porcentagem referente ao revendedor
                            $credito_revenda_crp_transacao_id = 102;
                            $credito_revenda_descricao = "Comissão 30% ({$crp_cobranca["referencia"]})";
                            $credito_revenda_valor = (-1 * $crp_cobranca["valor"]); // adminitradores do sistema recebem valor integral em forma de créditos
                            if ($usuario["usuario_id"] != $usuario["servidor"]["usuario_id"]) { // caso contrario recebem 30% do valor creditado
                                $credito_revenda_crp_transacao_id = 101;
                                $credito_revenda_descricao = "Comissão 100% ({$crp_cobranca["referencia"]})";
                                $credito_revenda_valor = (-1 * ($crp_cobranca["valor"] * 0.3));
                            }


                            crp_transacao::cadastrar($this->pid, $credito_revenda_descricao, $credito_revenda_valor, date("Y-m-d H:i:s", $data_creditado_crp_cobranca), $credito_revenda_crp_transacao_id, false, $usuario["usuario_id"]);
                        }
                    }
                }
            }
        }
    }

}
