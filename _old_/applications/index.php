<?php

class index extends app {

    public function __construct() {
        $permalink = permalink::get(0);
        $layout = ($permalink ? $permalink : "index");
        switch ($layout) {
            case 'cadastro':
                knife::redirect("http://painel.insigndigital.com.br/cadastrar.html");
                break;
            case 'painel':
                knife::redirect("http://painel.insigndigital.com.br/painel.html");
                break;
            case 'estado':
                knife::redirect("http://stats.pingdom.com/r843l4yra20v");
                break;
            default:
                $this->planos(($layout == "index" ? 4 : 8));
                $this->contato();
                $this->active = $layout;
                $this->modulo("layout/main/{$layout}.html");
                break;
        }
    }

    protected function modulo($modulo) {
        $this->layout = knife::html($modulo);
    }

    protected function planos($limit = 8) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("plano");
        $db->column("*");
        $db->match("usuario_id", 1);
        $db->is("removido", false, "AND");
        $db->order("valor");
        $db->limit($limit);
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $row["destaque"] = false;
                $row["valor_br"] = number_format($row["valor"], 2, ',', '.');
                $row["valor_arredondado"] = ceil($row["valor"]);
                $row["total_quota_disco"] = ($row["total_quota_disco"] / 1000);
                $row["total_quota_transferencia"] = ($row["total_quota_transferencia"] / 1000);
                $array[$row["id"]] = $row;
            }
            $this->planos = $array;
        }
    }

    public function __call($name, $arguments) {
        return false;
    }

    public function contato() {
        $this->recaptcha = recaptcha::form();
        if (isset($_POST["email"])) {
            $this->mensagem_erro = true;
            $this->extract($_POST);
            $this->nome = trim(htmlspecialchars($this->nome));
            $this->email = trim(htmlspecialchars($this->email));
            $this->assunto = trim(htmlspecialchars($this->assunto));
            $this->mensagem = trim(str_replace(array("\t", "\r", "\n"), " ", $this->mensagem));
            if ($this->nome && $this->email && $this->assunto && $this->mensagem && recaptcha::validacao()) {
                if (self::atendimento($this->nome, $this->email, $this->assunto, $this->mensagem)) {
                    $this->mensagem_enviada = true;
                }
            }
        }
    }

    public static function atendimento($nome, $email, $assunto, $mensagem) {
        $corpo_gestor[] = date("r") . ",";
        $corpo_gestor[] = $assunto;
        $corpo_gestor[] = "";
        $corpo_gestor[] = $mensagem;
        $corpo_gestor[] = "";
        $corpo_gestor[] = "Atenciosamente,";
        $corpo_gestor[] = "{$nome} ({$email})";
        $corpo_gestor[] = "Atendimento: " . getmypid() . "({$_SERVER["REMOTE_ADDR"]})";
        if (self::envia_para_gestor($nome, $email, "Novo Atendimento (" . getmypid() . ")", $corpo_gestor)) {
            $corpo_cliente[] = "Olá {$nome},";
            $corpo_cliente[] = "";
            $corpo_cliente[] = "Informamos sua mensagem que foi registrada sob o número " . getmypid() . " e em breve retornaremos!";
            $corpo_cliente[] = "";
            $corpo_cliente[] = "========================";
            $corpo_cliente[] = date("r") . ",";
            $corpo_cliente[] = $assunto;
            $corpo_cliente[] = "";
            $corpo_cliente[] = $mensagem;
            $corpo_cliente[] = "";
            $corpo_cliente[] = "Atenciosamente,";
            $corpo_cliente[] = "{$nome} ({$email})";
            $corpo_cliente[] = "========================";
            return self::envia_para_cliente($email, "Atendimento Recebido (" . getmypid() . ")", $corpo_cliente);
        }
        return false;
    }

    private static function envia_para_cliente($email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = name;
        $mensagem[] = "http://" . domain . "/";
        $headers = "From: " . name . " <contato@insigndigital.com.br>\n";
        $headers .= "Reply-To: " . name . " <contato@insigndigital.com.br>\n";
        return knife::mail_utf8($email, '[' . name . '] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

    private static function envia_para_gestor($nome, $email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = name;
        $mensagem[] = "http://" . domain . "/";
        $headers = "From: " . $nome . " <" . $email . ">\n";
        $headers .= "Reply-To: " . $nome . " <" . $email . ">\n";
        return knife::mail_utf8("contato@insigndigital.com.br", '[' . name . '] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

}

?>
