<?php

class zpanel_ftp extends api_zpanel {

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("ftp_management", "GetFTPClients", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["id"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function diretorios($usuario_id) {
        $arr = self::PostXMWS("ftp_management", "GetFTPDirs", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["domains"]] = array("id" => $row["domains"], "diretorio" => $row["domains"]);
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($usuario, $senha, $diretorio, $tipo_acesso, $usuario_id) {
        $autohome = '1';
        if ($diretorio) {
            $autohome = '0';
            if (substr($diretorio, 0, 1) == "/") {
                $diretorio = substr($diretorio, 1);
            }
        }
        $arr = self::PostXMWS("ftp_management", "CreateFTPClient", "<uid>{$usuario_id}</uid><username>{$usuario}</username><password>{$senha}</password><destination>{$diretorio}</destination><access_type>{$tipo_acesso}</access_type><home>{$autohome}</home>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function atualizar($id, $senha) {
        $arr = self::PostXMWS("ftp_management", "UpdateFTPClient", "<ftpclientid>{$id}</ftpclientid><password>{$senha}</password>");
        if (isset($arr['updated']) and $arr['updated'] == "true") {
            return $arr['updated'];
        }
        return false;
    }

    static public function remover($id) {
        $arr = self::PostXMWS("ftp_management", "DeleteFTPClient", "<ftpclientid>{$id}</ftpclientid>");
        if (isset($arr['deleted']) and $arr['deleted'] == "true") {
            return $arr['deleted'];
        }
        return false;
    }

}
