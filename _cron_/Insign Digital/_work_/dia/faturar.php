<?php

header("Content-type: text/html; charset=utf8");

function Msg($linha, $mensagem) {
    $mensagem = trim($mensagem);
    if (strlen($mensagem) > 0) {
        print "[" . date("d/m/Y H:i:s") . "] [{$linha}] {$mensagem}\n";
    }
}

function Response($linha, $mensagem) {
    if (!is_array($mensagem)) {
        $mensagem = explode("\n", $mensagem);
    }
    foreach ($mensagem as $row) {
        Msg($linha, $row);
    }
}

Response(__LINE__, file_get_contents("http://ccd.insigndigital.com.br/index.html"));
