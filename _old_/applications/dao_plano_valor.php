<?php

class dao_plano_valor {

    public static function pegar($tipo) {
        $db = new mysqlsearch();
        $db->table("plano_valor");
        $db->column("*");
        $db->match("tipo", $tipo);
        $db->order("atualizado", "DESC");
        $db->limit(1);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("plano_valor");
        $db->column("*");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["tipo"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["valor_br"] = number_format($row["valor"], 5, ',', '.');
        return $row;
    }

}