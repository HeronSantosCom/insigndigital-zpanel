<?php

class zpanel_usuario extends api_zpanel {

    static public function pegar($id) {
        $arr = self::PostXMWS("manage_clients", "GetClient", $id);
        if (isset($arr["list"])) {
            return $arr["list"];
        }
        return false;
    }

    static public function listar($usuario_id) {
        $arr = self::PostXMWS("manage_clients", "GetAllClients", $usuario_id);
        if (isset($arr["list"])) {
            $retorno = false;
            foreach ((empty($arr["list"][0]) ? array($arr["list"]) : $arr["list"]) as $row) {
                $retorno[$row["userid"]] = $row;
            }
            return $retorno;
        }
        return false;
    }

    static public function cadastrar($nome, $email, $telefone, $usuario, $senha, $plano_id, $usuario_id) {
        $arr = self::PostXMWS("manage_clients", "CreateClient", "<resellerid>{$usuario_id}</resellerid><username>{$usuario}</username><packageid>{$plano_id}</packageid><groupid>2</groupid><fullname>{$nome}</fullname><email>{$email}</email><address></address><postcode></postcode><phone>{$telefone}</phone><password>{$senha}</password><sendemail>0</sendemail><emailsubject></emailsubject><emailbody></emailbody>");
        if (isset($arr['id']) and $arr['id'] != "false") {
            return $arr['id'];
        }
        return false;
    }

    static public function atualizar($id, $nome, $email, $telefone, $status, $senha, $plano_id) {
        $arr = self::PostXMWS("manage_clients", "UpdateClient", "<clientid>{$id}</clientid><package>{$plano_id}</package><enabled>{$status}</enabled><group>2</group><fullname>{$nome}</fullname><email>{$email}</email><address></address><post></post><phone>{$telefone}</phone><newpass>{$senha}</newpass>");
        if (isset($arr['updated']) and $arr['updated'] == "true") {
            return $arr['updated'];
        }
        return false;
    }

    static public function remover($id, $usuario_id) {
        $arr = self::PostXMWS("manage_clients", "DeleteClient", "<userid>{$id}</userid><clientid>{$usuario_id}</clientid>");
        if (isset($arr["deleted"]) and $arr['deleted'] == "true") {
            return $arr["deleted"];
        }
        return false;
    }

    static public function desabilitar($id) {
        $arr = self::PostXMWS("manage_clients", "DisableClient", "<uid>{$id}</uid>");
        if (isset($arr["disabled"]) and $arr['disabled'] == "true") {
            return $arr["disabled"];
        }
        return false;
    }

}
