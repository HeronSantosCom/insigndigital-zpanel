<?php

if (!class_exists("zpanel_subdominio", false)) {
    include path::applications("zpanel/subdominio.php");
}

class dao_subdominio {

    public static function pegar($id, $dominio_id = false, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("subdominio");
        $db->column("*");
        if ($dominio_id) {
            $db->match("dominio_id", $dominio_id);
        } else {
            $db->match("id", $id);
        }
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id);
        }
        $db->is("removido", false);
        $dao = $db->go();
        if ($dao) {
            return self::hook($dao[0], false);
        }
        return false;
    }

    public static function listar($usuario_id, $dominio_id = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("subdominio");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        if ($dominio_id) {
            $db->match("dominio_id", $dominio_id);
        }
        $db->is("removido", false);
        $db->order("subdominio");
        $dao = $db->go();
        if ($dao) {
            $zpanel_dao = zpanel_subdominio::listar(dao_usuario::zpanel_id($usuario_id));
            foreach ($dao as $row) {
                $zpanel = (!empty($zpanel_dao[$row["zpanel_id"]]) ? $zpanel_dao[$row["zpanel_id"]] : false);
                $array[$row["id"]] = self::hook($row, $zpanel);
            }
        }
        return $array;
    }

    private static function hook($row, $zpanel) {
        $row["online"] = (!empty($zpanel["subactive"]) ? ($zpanel["subactive"] ? true : false) : false);
        $row["redirect"] = ($row["redirecionar"] ? true : false);
        $row["mirror"] = ($row["diretorio"] != ("/" . str_replace(".", "_", $row["subdominio"])));
        return $row;
    }

    public static function cadastrar($subdominio, $diretorio, $redirecionar, $dominio_id, $usuario_id) {
        if (dao_modulo::zpanel_disponivel("subdominio", $usuario_id)) {
            $dominio = dao_dominio::hostname($dominio_id);
            $dominio_id_zpanel_id = dao_dominio::zpanel_id($dominio_id);
            $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
            if ($dominio && $dominio_id_zpanel_id && $usuario_zpanel_id) {
                $subdominio_completo = "{$subdominio}.{$dominio}";
                if (!self::verificar($subdominio_completo)) {
                    $zpanel_id = zpanel_subdominio::cadastrar($subdominio_completo, $diretorio, $redirecionar, $usuario_zpanel_id);
                    if ($zpanel_id) {
                        $diretorio = ($diretorio ? $diretorio : ("/" . str_replace(".", "_", $subdominio_completo)));
                        $db = new mysqlsave();
                        $db->table("subdominio");
                        $db->column("subdominio", $subdominio_completo);
                        $db->column("diretorio", $diretorio);
                        $db->column("redirecionar", $redirecionar);
                        $db->column("dominio_id", $dominio_id);
                        $db->column("usuario_id", $usuario_id);
                        $db->column("zpanel_id", $zpanel_id);
                        $db->column("cadastrado", date("Y-m-d H:i:s"));
                        $db->column("atualizado", date("Y-m-d H:i:s"));
                        if ($db->go()) {
                            $id = $db->id();
                            if (dao_dns::cadastrar("CNAME", $subdominio, "3600", "@", false, false, false, $dominio_id, $id, $usuario_id)) {
                                return $id;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function atualizar($id, $diretorio, $redirecionar, $usuario_id) {
        $subdominio = self::hostname($id);
        $zpanel_id = self::zpanel_id($id);
        $usuario_zpanel_id = dao_usuario::zpanel_id($usuario_id);
        if ($subdominio && $zpanel_id && $usuario_zpanel_id) {
            if (zpanel_subdominio::atualizar($zpanel_id, $subdominio, $diretorio, $redirecionar, $usuario_id)) {
                $diretorio = ($diretorio ? $diretorio : ("/" . str_replace(".", "_", $subdominio)));
                $db = new mysqlsave();
                $db->table("subdominio");
                $db->column("diretorio", $diretorio);
                $db->column("redirecionar", $redirecionar);
                $db->column("atualizado", date("Y-m-d H:i:s"));
                $db->match("id", $id);
                return $db->go();
            }
        }
        return false;
    }

    public static function remover($id, $dominio_id = false) {
        if ($dominio_id) {
            $dao = self::pegar(false, $dominio_id);
            if ($dao) {
                $id = $dao["id"];
            }
        }
        $zpanel_id = self::zpanel_id($id);
        if ($zpanel_id) {
            $dns_removido = true;
            if (!$dominio_id) {
                $dns_id = self::dns_id($id);
                $dns_removido = false;
                if ($dns_id) {
                    if (dao_dns::remover($dns_id)) {
                        $dns_removido = true;
                    }
                }
            }
            if ($dns_removido) {
                if (zpanel_subdominio::remover($zpanel_id)) {
                    $db = new mysqlsave();
                    $db->table("subdominio");
                    $db->column("removido", date("Y-m-d H:i:s"));
                    $db->match("id", $id);
                    if ($db->go()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static function verificar($subdominio_completo) {
        $db = new mysqlsearch();
        $db->table("subdominio");
        $db->column("id");
        $db->match("subdominio", $subdominio_completo);
        $db->is("removido", false);
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

    public static function dns_id($id) {
        $usuario_id = self::usuario_id($id);
        $dao = dao_dns::listar($usuario_id, false, $id, false);
        if ($dao) {
            $dao = array_shift($dao);
        }
        return (!empty($dao["id"]) ? $dao["id"] : false);
    }

    public static function usuario_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["usuario_id"]) ? $dao["usuario_id"] : false);
    }

    public static function hostname($id) {
        $dao = self::pegar($id);
        return (!empty($dao["subdominio"]) ? $dao["subdominio"] : false);
    }

    public static function zpanel_id($id) {
        $dao = self::pegar($id);
        return (!empty($dao["zpanel_id"]) ? $dao["zpanel_id"] : false);
    }

}