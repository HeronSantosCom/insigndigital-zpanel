<?php

class layout_chamados extends main {

    public function __construct() {
        $this->sem_cadastro = true;
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
            $this->interacoes = dao_chamado_interacao::listar($this->id);
        }
        $this->chamados = dao_chamado::listar(false, logon::meu_id());
    }

    private function abrir($id) {
        $dao = (dao_chamado::pegar($id, false, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            dao_chamado_interacao::atualizar($this->id, logon::meu_id());
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            if ($id) {
                $this->msgbox("Não foi possível salvar a resposta do atendimento, verifique os dados e tente novamente!");
                $action = dao_chamado_interacao::cadastrar($this->resposta, $id, logon::meu_id());
                if ($action) {
                    $usuario = dao_usuario::pegar($this->usuario_id);
                    if ($usuario) {
                        snr::scrg($usuario["nome"], $usuario["email"], $usuario["telefone"], $this->titulo);
                    }
                    $this->msgbox("Resposta salva com sucesso!");
                    dao_chamado::atualizar($id, 2);
                    $this->resposta = false;
                }
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível encerrar o atendimento, tente novamente!");
        if ($id && $this->status < 3) {
            $action = dao_chamado::atualizar($id, 3);
            if ($action) {
                $usuario = dao_usuario::pegar($this->usuario_id);
                if ($usuario) {
                    snr::srg($usuario["nome"], $usuario["email"], $usuario["telefone"], $this->titulo);
                }
                $this->msgbox("Atendimento encerrado com sucesso!");
                unset($this->formulario);
            }
        }
    }

}