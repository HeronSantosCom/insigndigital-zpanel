<?php

class layout_atendimentos extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
            $this->interacoes = dao_chamado_interacao::listar($this->id);
        }
        $this->atendimentos = dao_chamado::listar(logon::meu_id());
    }

    private function abrir($id) {
        $dao = (dao_chamado::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            dao_chamado_interacao::atualizar($this->id, logon::meu_id());
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            if ($id) {
                $this->msgbox("Não foi possível salvar a resposta do atendimento, verifique os dados e tente novamente!");
                $action = dao_chamado_interacao::cadastrar($this->resposta, $id, logon::meu_id());
                if ($action) {
                    snr::scr(logon::meu_nome(), logon::meu_email(), logon::meu_telefone(), $this->titulo);
                    $this->msgbox("Resposta salva com sucesso!");
                    dao_chamado::atualizar($id, 1);
                    $this->resposta = false;
                }
            } else {
                $this->msgbox("Não foi possível salvar o pedido atendimento, verifique os dados e tente novamente!");
                $action = dao_chamado::cadastrar($this->titulo, $this->questao, logon::meu_id(), logon::meu_usuario_id());
                if ($action) {
                    snr::sc(logon::meu_nome(), logon::meu_email(), logon::meu_telefone(), $this->titulo);
                    unset($this->formulario);
                    $this->msgbox("Pedido atendimento foi registrado com sucesso!");
                }
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível encerrado o atendimento, tente novamente!");
        if ($id && $this->status < 3) {
            $action = dao_chamado::atualizar($id, 4);
            if ($action) {
                snr::sr(logon::meu_nome(), logon::meu_email(), logon::meu_telefone(), $this->titulo);
                $this->msgbox("Atendimento encerrado com sucesso!");
                unset($this->formulario);
            }
        }
    }

}