<?php

class layout_clientes extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"]) || isset($_GET["administrar"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["administrar"])) {
                if ($this->plano_id) {
                    $this->administrar = true;
                    if (isset($_POST["id"])) {
                        $this->administrar($_POST["id"]);
                    }
                } else {
                    define("app_layout_error", true);
                    return false;
                }
            } else {
                if (isset($_GET["remover"])) {
                    $this->remover = true;
                }
                if (isset($_POST["id"])) {
                    $this->salvar($_POST["id"]);
                }
            }
        }
        $this->planos = dao_plano::listar(logon::meu_id());
        $this->usuarios = dao_usuario::listar(logon::meu_id());
    }

    private function abrir($id) {
        $dao = (dao_usuario::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function administrar($id) {
        $_SESSION["saved_logon"][] = logon::meu_id();
        if (logon::reboot($id, false, false, false)) {
            snr::au(logon::meu_nome(), logon::meu_email(), logon::meu_telefone());
            knife::redirect("/index.html");
        }
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar a cliente, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_usuario::atualizar($id, $this->nome, strtolower($this->email), $this->senha, $this->telefone, $this->motivo, $this->bloqueado, $this->bloqueado_saldo, 1, 1, $this->plano_id, 2);
                if ($action) {
                    snr::ua($this->nome, $this->email, $this->telefone, $this->senha, ($this->bloqueado ? $this->motivo : false));
                }
            } else {
                $action = dao_usuario::cadastrar($this->nome, strtolower($this->email), $this->senha, $this->telefone, 1, 1, $this->plano_id, 2, logon::meu_servidor_id(), logon::meu_id());
                if ($action) {
                    snr::uc($this->nome, $this->email, $this->telefone, $this->senha);
                }
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Cliente salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o cliente, tente novamente!");
        if ($id) {
            $action = dao_usuario::remover($id, $this->motivo, false);
            if ($action) {
                snr::ur($this->nome, $this->email, $this->telefone, $this->motivo);
                unset($this->formulario);
                $this->msgbox("Cliente removido com sucesso!");
            }
        }
    }

}