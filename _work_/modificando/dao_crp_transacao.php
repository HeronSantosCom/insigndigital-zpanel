<?php

class _crp_transacao {

    public static function listar($usuario_id, $crp_cobranca_id = false, $crp_tipo_transacao_id = false, $lancamento = false) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("crp_transacao");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        if ($crp_cobranca_id) {
            $db->match("crp_cobranca_id", $crp_cobranca_id);
        }
        if ($crp_tipo_transacao_id) {
            $db->match("crp_tipo_transacao_id", $crp_tipo_transacao_id);
        }
        if ($lancamento) {
            if (is_array($lancamento)) {
                $db->between("DATE(lancamento)", $lancamento);
            } else {
                $db->match("DATE(lancamento)", $lancamento);
            }
        }
        $dao = $db->go();
        if ($dao) {
            $dao_crp_cobranca = ($crp_cobranca_id ? false : dao_crp_cobranca::listar($usuario_id));
            foreach ($dao as $row) {
                $crp_cobranca = (!empty($dao_crp_cobranca[$row["crp_cobranca_id"]]) ? $dao_crp_cobranca[$row["crp_cobranca_id"]] : false);
                $array[$row["id"]] = self::hook($row, $crp_cobranca);
            }
        }
        return $array;
    }

    public static function debito_hoje($usuario_id, $lancamento, $crp_tipo_transacao_id) {
        $db = new mysqlsearch();
        $db->table("crp_transacao");
        $db->column("COUNT(id)", false, "total");
        $db->match("usuario_id", $usuario_id);
        $db->match("DATE(lancamento)", $lancamento);
        $db->match("crp_tipo_transacao_id", $crp_tipo_transacao_id);
        $dao = $db->go();
        if (!empty($dao[0]["total"])) {
            return $dao[0]["total"];
        }
        return 0;
    }

    public static function saldo($usuario_id, $lancamento = false) {
        $db = new mysqlsearch();
        $db->table("crp_transacao");
        $db->column("SUM(valor)", false, "total");
        $db->match("usuario_id", $usuario_id);
        $db->match("crp_tipo_transacao_id", 200, false, true);
        if ($lancamento) {
            if (is_array($lancamento)) {
                $db->between("DATE(lancamento)", $lancamento);
            } else {
                $db->match("DATE(lancamento)", $lancamento);
            }
        }
        $dao = $db->go();
        if (!empty($dao[0]["total"])) {
            return $dao[0]["total"];
        }
        return 0;
    }

    public static function historico($usuario_id) {
        $db = new mysqlsearch();
        $db->table("crp_transacao");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->match("crp_tipo_transacao_id", 200, false, true);
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row, false);
            }
        }
        return $array;
    }

    private static function hook($row, $crp_cobranca) {
        $row["lancamento_br"] = date("d/m/Y", strtotime($row["lancamento"]));
        $row["valor_br"] = number_format($row["valor"], 5, ',', '.');
        $row["valor2_br"] = number_format($row["valor"], 2, ',', '.');
        if ($crp_cobranca) {
            foreach ($crp_cobranca as $key => $value) {
                $row["crp_cobranca_{$key}"] = $value;
            }
        }
        return $row;
    }

    public static function cadastrar($pid, $descricao, $valor, $lancamento, $crp_tipo_transacao_id, $crp_cobranca_id, $usuario_id) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("pid", $pid);
        $db->column("descricao", $descricao);
        $db->column("valor", $valor);
        $db->column("lancamento", $lancamento);
        $db->column("crp_tipo_transacao_id", $crp_tipo_transacao_id);
        $db->column("crp_cobranca_id", $crp_cobranca_id);
        $db->column("usuario_id", $usuario_id);
        if ($db->go()) {
            return $db->id();
        }
        return false;
    }

    public static function atualizar($id, $crp_cobranca_id) {
        $db = new mysqlsave();
        $db->table("crp_transacao");
        $db->column("crp_cobranca_id", $crp_cobranca_id);
        $db->match("id", $id);
        return $db->go();
    }

}