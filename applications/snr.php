<?php

/**
 * Serviço de Notificação e Relacionamento
 */
class snr {

    /**
     * Notifica Cadastrado de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $senha
     * @return boolean
     */
    public static function uc($nome, $email, $telefone, $senha) {
        if (spool_smtp::uc($nome, $email, $senha)) {
            return spool_sms::uc($telefone);
        }
        return false;
    }

    /**
     * Notifica Atualização de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $senha
     * @param string $motivo
     * @return boolean
     */
    public static function ua($nome, $email, $telefone, $senha, $motivo) {
        if (spool_smtp::ua($nome, $email, $senha, $motivo)) {
            return spool_sms::ua($telefone);
        }
        return false;
    }

    /**
     * Notifica Remoção de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $motivo
     * @return boolean
     */
    public static function ur($nome, $email, $telefone, $motivo) {
        if (spool_smtp::ur($nome, $email, $motivo)) {
            return spool_sms::ur($telefone);
        }
        return false;
    }

    /**
     * Notifica Redefição de Senha do Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $senha
     * @return boolean
     */
    public static function usr($nome, $email, $telefone, $senha) {
        if (spool_smtp::usr($nome, $email, $senha)) {
            return spool_sms::usr($telefone);
        }
        return false;
    }

    /**
     * Notifica Assinatura de Plano do Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $plano
     * @param string $valor
     * @param string $bonus
     * @return boolean
     */
    public static function uap($nome, $email, $telefone, $plano, $valor, $bonus) {
        if (spool_smtp::uap($nome, $email, $plano, $valor, $bonus)) {
            return spool_sms::uap($telefone);
        }
        return false;
    }

    /**
     * Notificação de Bloqueio de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $motivo
     * @return boolean
     */
    public static function ub($nome, $email, $telefone, $motivo) {
        if (spool_smtp::ub($nome, $email, $motivo)) {
            return spool_sms::ub($telefone);
        }
        return false;
    }

    /**
     * Notificação de Desbloqueio de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @return boolean
     */
    public static function ud($nome, $email, $telefone) {
        if (spool_smtp::ud($nome, $email)) {
            return spool_sms::ud($telefone);
        }
        return false;
    }

    /**
     * Notifica Administração de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @return boolean
     */
    public static function au($nome, $email, $telefone) {
        if (spool_smtp::au($nome, $email)) {
            return spool_sms::au($telefone);
        }
        return false;
    }

    /**
     * CRP: Notificação de Crédito de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $descricao
     * @param string $valor
     * @return boolean
     */
    public static function crpuc($nome, $email, $telefone, $descricao, $valor) {
        if (spool_smtp::crpuc($nome, $email, $descricao, $valor)) {
            return spool_sms::crpuc($telefone);
        }
        return false;
    }

    /**
     * CRP: Notificação de Crédito de Cliente de Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $descricao
     * @param string $valor
     * @return boolean
     */
    public static function crpcc($nome, $email, $telefone, $descricao, $valor) {
        if (spool_smtp::crpcc($nome, $email, $descricao, $valor)) {
            return spool_sms::crpcc($telefone);
        }
        return false;
    }

    /**
     * Notificação de Novo Atendimento de Suporte do Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $titulo
     * @return boolean
     */
    public static function sc($nome, $email, $telefone, $titulo) {
        if (spool_smtp::sc($nome, $email, $titulo)) {
            return spool_sms::sc($telefone);
        }
        return false;
    }

    /**
     * Notificação de Nova Resposta de Atendimento do Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $titulo
     * @return boolean
     */
    public static function scr($nome, $email, $telefone, $titulo) {
        if (spool_smtp::scr($nome, $email, $titulo)) {
            return spool_sms::scr($telefone);
        }
        return false;
    }

    /**
     * Notificação de Encerramento de Atendimento do Usuário
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $titulo
     * @return boolean
     */
    public static function sr($nome, $email, $telefone, $titulo) {
        if (spool_smtp::sr($nome, $email, $titulo)) {
            return spool_sms::sr($telefone);
        }
        return false;
    }

    /**
     * Notificação de Nova Resposta de Atendimento do Gestor
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $titulo
     * @return boolean
     */
    public static function scrg($nome, $email, $telefone, $titulo) {
        if (spool_smtp::scrg($nome, $email, $titulo)) {
            return spool_sms::scrg($telefone);
        }
        return false;
    }

    /**
     * Notificação de Encerramento de Atendimento do Gestor
     * @param string $nome
     * @param string $email
     * @param string $telefone
     * @param string $titulo
     * @return boolean
     */
    public static function srg($nome, $email, $telefone, $titulo) {
        if (spool_smtp::srg($nome, $email, $titulo)) {
            return spool_sms::srg($telefone);
        }
        return false;
    }

    /**
     * Notificação de Cadastro de Servidor Cloud
     * @param type $nome
     * @param type $email
     * @param type $telefone
     * @param type $hostname
     * @param type $ip
     * @param type $senha
     * @return boolean
     */
//    public static function scc($nome, $email, $telefone, $hostname, $ip, $senha) {
//        if (spool_smtp::scc($nome, $email, $hostname, $ip, $senha)) {
//            return spool_sms::scc($telefone);
//        }
//        return false;
//    }

    /**
     * Ação do Usuário
     * @param string $nome
     * @param string $email
     * @param string $acao
     * @return boolean
     */
    public static function uac($nome, $email, $acao) {
        return spool_smtp::uac($nome, $email, $acao);
    }

}